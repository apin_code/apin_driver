//
//  AppDelegate.m
//  Apin_Driver
//
//  Created by Apin on 15/8/1.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "ApinDriverMainViewController.h"
#import "PinChe_DriverOrderDetails.h"
#import "LoginViewController.h"
#import "MapViewController.h"//我这下来  这东西没有了///提交一下
#import "RegistTwoViewController.h"
//#define int MyViewController*

//#import <PgySDK/PgyManager.h>


#define PGY_APPKEY @"a8620a3deec815586e433de5e49f0b00"
#define BMKey @"lpO499LNNCYpw6wpDVf2rWif"
#define BMKeyDIST @"sZqQ7zxYuVTOZpK6FELQwsPs"
@interface AppDelegate ()
{
    UILabel * _messageLabel;
    BOOL animated;
    UIFont * _messageTextFont;
    
    int _mapDeleNum;
}
@end

@implementation AppDelegate
#pragma mark 蒲公英检查更新
//- (void)updateVersion
//{
//    PgyManager *manager = [PgyManager sharedPgyManager];
//    [manager startManagerWithAppId:PGY_APPKEY];
//    [manager setEnableFeedback:NO];
//    [manager checkUpdate];
//}
#pragma mark 自定义提示信息Label
+(void)creatMessageLabelWithMessage:(NSString *)message{
    AppDelegate * delegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
    [delegate startMessageLabel:message];
}
#pragma mark ios8之后注册推送
- (void)registerPushForIOS8{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= _IPHONE80_
    
    //Types
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    
    //Actions
    UIMutableUserNotificationAction *acceptAction = [[UIMutableUserNotificationAction alloc] init];
    
    acceptAction.identifier = @"ACCEPT_IDENTIFIER";
    acceptAction.title = @"Accept";
    
    acceptAction.activationMode = UIUserNotificationActivationModeForeground;
    acceptAction.destructive = NO;
    acceptAction.authenticationRequired = NO;
    
    //Categories
    UIMutableUserNotificationCategory *inviteCategory = [[UIMutableUserNotificationCategory alloc] init];
    
    inviteCategory.identifier = @"INVITE_CATEGORY";
    
    [inviteCategory setActions:@[acceptAction] forContext:UIUserNotificationActionContextDefault];
    
    [inviteCategory setActions:@[acceptAction] forContext:UIUserNotificationActionContextMinimal];
    
    NSSet *categories = [NSSet setWithObjects:inviteCategory, nil];
    
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:categories];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
#endif
}
#pragma mark ios8之前注册推送
- (void)registerPush{
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
}

#pragma mark appdelegate自身方法
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    _mapDeleNum=0;
//    NSLog(@"%@",launchOptions);
//    [self updateVersion];
    if (launchOptions!=nil) {
        ApinDriverMainViewController * mainCtr=(ApinDriverMainViewController *)self.window.rootViewController;
        if (mainCtr.navCtr==nil) {
            mainCtr.navCtr=[[UINavigationController alloc]init];
        }
        
        
//        UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"收到推送消息" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
//        [alertView show];
        NSUserDefaults * userDefaults=[NSUserDefaults standardUserDefaults];
        [userDefaults removeObjectForKey:@"userId"];
        [userDefaults removeObjectForKey:@"password"];
        [userDefaults removeObjectForKey:@"phone"];
        
        if (mainCtr.childViewControllers.count==0) {
            
            [mainCtr addChildViewController:mainCtr];
            [mainCtr.view addSubview:mainCtr.navCtr.view];
        }
        mainCtr.navCtr.viewControllers=@[[[LoginViewController alloc]init]];
        mainCtr.navCtr.view.frame=CGRectMake(KBgSmallViewX, KBgSmallViewY, KBgSmallViewW, KBgSmallViewH);
        UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:launchOptions[@"UIApplicationLaunchOptionsRemoteNotificationKey"][@"message"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertView show];
    }
    NSString *identifier = [[NSBundle mainBundle] bundleIdentifier];
    NSLog(@"identifier==%@",identifier);
    
    if ([XGPush isUnRegisterStatus]) {
        
    }
     _mapManager = [[BMKMapManager alloc]init];
    [XGPush unRegisterDevice:^{
        NSLog(@"注销成功");
    } errorCallback:^{
        NSLog(@"注销失败");
    }];
    BOOL ret;
    if ([identifier isEqualToString:@"com.apindriver.cn"]) {
        ret = [_mapManager start:BMKey  generalDelegate:nil];
        [XGPush startApp:ACCESSID appKey:ACCESSKEY];
    }else{
        [XGPush startApp:ACCESSIDDISCT appKey:ACCESSKEYDISCT];
        ret = [_mapManager start:BMKeyDIST  generalDelegate:nil];
    }
    
    
    
    
    
    //注销之后需要再次注册前的准备
    void (^successCallback)(void) = ^(void){
        //如果变成需要注册状态
        if(![XGPush isUnRegisterStatus])
        {
            //iOS8注册push方法
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= _IPHONE80_
            
            float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
            if(sysVer < 8){
                [self registerPush];
            }
            else{
                [self registerPushForIOS8];
            }
#else
            //iOS8之前注册push方法
            //注册Push服务，注册后才能收到推送
            [self registerPush];
#endif
        }
    };
    
    [XGPush initForReregister:successCallback];
    
    //推送反馈回调版本示例
    void (^successBlock)(void) = ^(void){
        //成功之后的处理
        NSLog(@"[XGPush]handleLaunching's successBlock");
    };
    
    void (^errorBlock)(void) = ^(void){
        //失败之后的处理
        NSLog(@"[XGPush]handleLaunching's errorBlock");
    };
    
    //角标清0
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    [XGPush handleLaunching:launchOptions successCallback:successBlock errorCallback:errorBlock];
    
    
   
    //     如果要关注网络及授权验证事件，请设定     generalDelegate参数
    
    
    if (!ret) {
        NSLog(@"manager start failed!");
    }else{
        NSLog(@"manager start YES!");
    }

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    self.window.rootViewController=[[ApinDriverMainViewController alloc]init];
    
    _bigLoadURLView =[[UIView alloc]initWithFrame:self.window.bounds];
    _bigLoadURLView.backgroundColor = [UIColor clearColor];
    _bigLoadURLView.alpha =1;
    _bigLoadURLView.hidden=YES;
    [_bigLoadURLView addSubview:_loadURLView];
    [self.window addSubview:_bigLoadURLView];
    
    _middleURLView = [[UIView alloc]initWithFrame:self.window.bounds];
    _middleURLView.backgroundColor =[UIColor blackColor];
    _middleURLView.alpha = 0.3;
    [_bigLoadURLView addSubview:_middleURLView];
    
    _loadURLView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
    _loadURLView.backgroundColor=[UIColor colorWithRed:1 green:1 blue:1 alpha:0.7];
    _loadURLView.backgroundColor=[UIColor clearColor];
//    _loadURLView.alpha=0.5;
    _loadURLView.layer.cornerRadius=10;
    _loadURLView.layer.masksToBounds=YES;
    _loadURLView.hidden=YES;
    _loadURLView.userInteractionEnabled=NO;
    _loadURLView.center=self.window.center;
    [self.window addSubview:_loadURLView];
    
    UIActivityIndicatorView * activityView=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityView.color=[UIColor whiteColor];
    [activityView startAnimating];
//    [activityView startAnimating];
    [_loadURLView addSubview:activityView];
    activityView.center=CGPointMake(_loadURLView.frame.size.width/2, _loadURLView.frame.size.height/2);
    
    UIImageView * loadImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
    loadImageView.center=CGPointMake(_loadURLView.frame.size.width/2, _loadURLView.frame.size.height/2);
    
    
    // 定位
    if ([CLLocationManager locationServicesEnabled]) {
        
        _locationManager = [[CLLocationManager alloc] init];
        
        _locationManager.delegate = self;
        
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest; //控制定位精度,越高耗电量越大。
        
        _locationManager.distanceFilter = 100; //控制定位服务更新频率。单位是“米”
        
        [_locationManager startUpdatingLocation];
        
        //在ios 8.0下要授权
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
            
            [_locationManager requestWhenInUseAuthorization];  //调用了这句,就会弹出允许框了.
        
    }
    return YES;
}

#pragma mark 设置跟试图控制器
-(void)setRootViewCtr{
    self.window.rootViewController=[[ApinDriverMainViewController alloc]init];
}
#pragma  mark - CLLocationManagerDelegate
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    CLLocation *location = [locations lastObject];
    
    CLLocationCoordinate2D cl2d = location.coordinate;
    
    self.locaLonStr = [NSString stringWithFormat:@"%f",cl2d.longitude];
    self.localLatStr = [NSString stringWithFormat:@"%f",cl2d.latitude];
    NSLog(@"cl2d.longitude=%@ -=- = -=-= cl2d.latitude=%@",self.locaLonStr,self.localLatStr);
    [self getStreetMessageWithLong:cl2d.longitude andLat:cl2d.latitude];
    
    _mapDeleNum++;
    if (_mapDeleNum>20) {
        _mapDeleNum=0;
//        [manager startUpdatingLocation];
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        NSString *userId=nil;
        if ([defaults objectForKey:@"userId"]) {
            userId=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"userId"]];
        }
        if (userId==nil) {
            return;
        }
        
        AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
        if (manager==nil) {
            return;
        }
        
        //type  clientLocation,//乘客位置 driverLocation,//司机位置
        //
        //id    longitude   --经度  latitude    --纬度
        
        NSDictionary *dic=@{@"type":@"driverLocation",
                            @"id":userId,
                            @"longitude":[NSString stringWithFormat:@"%f",cl2d.longitude],
                            @"latitude":[NSString stringWithFormat:@"%f",cl2d.latitude]};
        [manager POST:UpdataAddressURL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
        
    }
    
    
    
}
-(void)getStreetMessageWithLong:(CLLocationDegrees)lon andLat:(CLLocationDegrees)lat{
    CLLocation *loca =[[CLLocation alloc] initWithLatitude:lat longitude:lon];
    CLGeocoder *revGeo = [[CLGeocoder alloc]init];
   [revGeo reverseGeocodeLocation:loca completionHandler:^(NSArray *placemarks, NSError *error) {
       
        if (!error && [placemarks count]>0) {
            NSDictionary *dict = [[placemarks objectAtIndex:0] addressDictionary];
            NSLog(@"定位的地址 = = %@",[dict objectForKey:@"Street"]);
            self.locaStreetStr=[dict objectForKey:@"Street"];
            NSLog(@"定位的long , lat = %@,%@",self.locaLonStr,self.localLatStr);
        }else{
           
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"无法定位" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//            [alert show];
        }
   }];
    
    
}


#pragma mark  接收到推送消息的方法
//接收到推送消息
-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    
    NSLog(@"接收推送消息 notification==%@",notification);
    [self startMessageLabel:@"notification"];
    
    //notification是发送推送时传入的字典信息
    [XGPush localNotificationAtFrontEnd:notification userInfoKey:@"clockID" userInfoValue:@"myid"];
    
    //删除推送列表中的这一条
    [XGPush delLocalNotification:notification];
    //[XGPush delLocalNotification:@"clockID" userInfoValue:@"myid"];
    
    //清空推送列表
    //[XGPush clearLocalNotifications];
    
    _messageLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 30)];
    _messageLabel.backgroundColor=[UIColor magentaColor];
    _messageLabel.textColor=[UIColor whiteColor];
    _messageLabel.center=CGPointMake(self.window.center.x, self.window.center.y+100);
    _messageLabel.textAlignment=NSTextAlignmentCenter;
    [self.window addSubview:_messageLabel];
    _messageLabel.alpha=0;
    _messageLabel.numberOfLines=0;
    _messageTextFont=[UIFont fontWithName:nil size:15];
    _messageLabel.font=_messageTextFont;
    _messageLabel.layer.cornerRadius=5;
    _messageLabel.layer.masksToBounds=YES;
}
#pragma mark 注册UserNotification成功的回调
//
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    NSLog(@"苹果注册成功");
    
    
    
    //用户已经允许接收以下类型的推送
//    UIUserNotificationType allowedTypes = [notificationSettings types];
    
}

#pragma mark
-(void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler{
    
    if([identifier isEqualToString:@"ACCEPT_IDENTIFIER"]){
        NSLog(@"ACCEPT_IDENTIFIER is clicked");
    }
    
    NSLog(@"completionHandler userInfo%@",userInfo);
    
    completionHandler();
}

#pragma mark  推送接收到 deviceToken 方法
//
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    //NSString * deviceTokenStr = [XGPush registerDevice:deviceToken];
//    [XGPush unRegisterDevice];
    void (^successBlock)(void) = ^(void){
        //成功之后的处理
        NSLog(@"[XGPush]register 注册设备成功");
    };
    
    void (^errorBlock)(void) = ^(void){
        //失败之后的处理
        NSLog(@"[XGPush]register 注册设备失败");
    };
    
//    //注册设备
    [[XGSetting getInstance] setChannel:@"Apin_Driver"];
    [[XGSetting getInstance] setGameServer:@"Apin"];
    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
    
    if ([defaults objectForKey:@"phone"]) {
        [XGPush setAccount:[defaults objectForKey:@"phone"]];
        NSLog(@"[XGPush setAccount:%@]",[defaults objectForKey:@"phone"]);
    }
    
//    [XGPush setAccount:@"17767106659"];
    NSString * deviceTokenStr = [XGPush registerDevice:deviceToken successCallback:successBlock errorCallback:errorBlock];
    
    _tokenStr=deviceTokenStr;
    //如果不需要回调
    //[XGPush registerDevice:deviceToken];
    
    //打印获取的deviceToken的字符串
    NSLog(@"deviceTokenStr is %@",deviceTokenStr);
    
//    [XGPush unRegisterDevice];
}


#pragma mark  推送失败
// 推送失败
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error

{
    
    NSLog(@"获取token失败 error:{%@}",error);
    
    //    NSLog(@"Register Remote Notifications error:{%@}",error.localizedDescription);
    
}
#pragma mark  提示消息动画 lebel
-(void)startMessageLabel:(NSString *)message{
    
    
    if (animated) {
        return;
    }
    
    CGSize size=[message sizeWithFont:_messageTextFont constrainedToSize:CGSizeMake(200, 60)];
    _messageLabel.frame=CGRectMake(0, 0, size.width, size.height);
    animated=YES;
    _messageLabel.text=message;
    
    _messageLabel.center=CGPointMake(self.window.center.x, self.window.center.y+140);
    
    _messageLabel.alpha=1;
    
    [UIView animateWithDuration:1 animations:^{
        
        _messageLabel.center=CGPointMake(self.window.center.x, self.window.center.y+110);
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:2 animations:^{
            //            _messageLabel.userInteractionEnabled=NO;
            _messageLabel.alpha=0;
        } completion:^(BOOL finished) {
            
            animated=NO;
        }];
        
    }];
    
}
#pragma mark 运行时 接收到推送消息
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    
    
//    UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"收到的推送消息" message:[NSString stringWithFormat:@"%@",userInfo]  delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
////    alertView.tag=1;
//    [alertView show];
    
    
//    [XGPush unRegisterDevice];
    //推送反馈(app运行时)
    [XGPush handleReceiveNotification:userInfo];
//    [XGPush delLocalNotification:userInfo];
//    NSLog(@"userInfo==%@",userInfo);
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    NSLog(@"运行时 推送成功 userInfo==%@",userInfo);
    NSString * resultStr=userInfo[@"action"];
    if ([resultStr isEqualToString:@"userLogout"]) {
        //登出  别处登录  登录(司机 乘客)
        UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:userInfo[@"aps"][@"alert"]  delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        alertView.tag=1;
        [alertView show];
        
    }else if ([resultStr isEqualToString:@"carpoolSuccess"]) {
        //拼车成功(告诉乘客已被接单)(乘客)
        //司机  不用进入聊天室
        
        
    }else if ([resultStr isEqualToString:@"carpoolAcceptFailed"]) {
        //抢单失败(司机)
        UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:userInfo[@"aps"][@"alert"]  delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        alertView.tag=2;
        [alertView show];
        
        
        
    }else if ([resultStr isEqualToString:@"carpoolAcceptSuccess"]) {
        //抢单成功(司机)
        UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:userInfo[@"aps"][@"alert"]  delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        alertView.tag=3;
        [alertView show];
        
        
    }else if ([resultStr isEqualToString:@"carpoolStartJourney"]) {
        //开启行程(客户)
    }else if ([resultStr isEqualToString:@"carpoolEndJourney"]) {
        //结束行程(客户)
    }else if ([resultStr isEqualToString:@"carpoolNewJourney"]) {
        //新的拼车行程(司机)
        UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:userInfo[@"aps"][@"alert"]  delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        alertView.tag=4;
        [alertView show];
        
    }else if ([resultStr isEqualToString:@"carpoolAllExit"]){
        //所有乘客取消拼车
        UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:userInfo[@"aps"][@"alert"]  delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        alertView.tag=5;
        [alertView show];
        
    }else if ([resultStr isEqualToString:@"joinFilghtTour"]){
        //加入拼游拼车
//        UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:userInfo[@"aps"][@"alert"]  delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
//        alertView.tag=6;
//        [alertView show];
    }else if ([resultStr isEqualToString:@"cancelFilghtTour"]){
        //取消 拼游拼车
//        UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:userInfo[@"aps"][@"alert"]  delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
//        alertView.tag=7;
//        [alertView show];
    }else if ([resultStr isEqualToString:@"carpoolClientJoin"]){
        //乘客加入拼车
        
        
        
//        UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:userInfo[@"aps"][@"alert"]  delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
//        alertView.tag=6;
//        [alertView show];
    }else if ([resultStr isEqualToString:@"carpoolClientExit"]){
        //有乘客取消拼车
//        UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:userInfo[@"aps"][@"alert"]  delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
//        alertView.tag=6;
//        [alertView show];
    }else if ([resultStr isEqualToString:@"carpoolClientClose"]){
        //发起人关闭拼车
//        UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:userInfo[@"aps"][@"alert"]  delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
//        alertView.tag=6;
//        [alertView show];
    }
}

#pragma mark alertViewDelegate方法
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    ApinDriverMainViewController * mainCtr=(ApinDriverMainViewController *)self.window.rootViewController;
    if (mainCtr.navCtr==nil) {
        mainCtr.navCtr=[[UINavigationController alloc]init];
        
    }
    
    switch (alertView.tag) {
        case 1:
        {
            [ApinXGPush stopPush];
            mainCtr.centerButton.hidden=YES;
            mainCtr.orderButton.hidden=YES;
            //登出  别处登录  登录(司机 乘客)
            
            NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
            AFHTTPRequestOperationManager * manager=(AFHTTPRequestOperationManager *)[AFHTTPRequestOperationManager manager];
            if (manager==nil) {
                return;
            }
            [manager POST:LOGINOUTURL parameters:@{@"phone":[defaults objectForKey:@"phone"]} success:^(AFHTTPRequestOperation *operation, id responseObject) {
                if ([[NSString stringWithFormat:@"%@",responseObject[@"error_no"] ] isEqualToString:@"00000000"]) {
//                    AppDelegate * delegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
//                    delegate.userID=nil;
//                    
//                    //        [defaults setObject:nil forKey:@"phone"];
//                    [defaults setObject:nil forKey:@"password"];
//                    [defaults setObject:nil forKey:@"userId"];
//                    [defaults setObject:nil forKey:@"headpic"];
//                    [ApinXGPush stopPush];
//                    
//                    mainCtr.navigationController.viewControllers=@[[[LoginViewController alloc] init]];
                    
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
            }];
            
            
            
//            NSUserDefaults * userDefaults=[NSUserDefaults standardUserDefaults];
//            [userDefaults removeObjectForKey:@"userId"];
//            [userDefaults removeObjectForKey:@"password"];
//            [userDefaults removeObjectForKey:@"phone"];
//            [userDefaults removeObjectForKey:@"headpic"];
            if (mainCtr.childViewControllers.count==0) {
                
                [mainCtr addChildViewController:mainCtr.navCtr];
                [mainCtr.view addSubview:mainCtr.navCtr.view];
            }
            mainCtr.loginOK=NO;
            mainCtr.navCtr.viewControllers=@[[[LoginViewController alloc]init]];
            mainCtr.navCtr.view.frame=CGRectMake(KBgSmallViewX, KBgSmallViewY, KBgSmallViewW, KBgSmallViewH);
        }
            break;
            
        case 2:
        {
            mainCtr.centerButton.hidden=YES;
            mainCtr.orderButton.hidden=YES;
            //抢单失败(司机)
            if (mainCtr.childViewControllers.count==0) {
                [mainCtr addChildViewController:mainCtr.navCtr];
                [mainCtr.view addSubview:mainCtr.navCtr.view];
            }
            PinChe_DriverOrderDetails * pincheCtr=[[PinChe_DriverOrderDetails alloc]init];
            mainCtr.navCtr.viewControllers=@[pincheCtr];
            mainCtr.navCtr.view.frame=mainCtr.view.bounds;
            
            
        }
            break;
            
        case 3:
        {
            mainCtr.centerButton.hidden=YES;
            mainCtr.orderButton.hidden=YES;
            //抢单成功(司机)
            if (mainCtr.childViewControllers.count==0) {
                [mainCtr addChildViewController:mainCtr.navCtr];
                [mainCtr.view addSubview:mainCtr.navCtr.view];
            }
            MapViewController * pincheCtr=[[MapViewController alloc]init];
            mainCtr.navCtr.viewControllers=@[pincheCtr];
            mainCtr.navCtr.view.frame=mainCtr.view.bounds;
//            [pincheCtr tokenSuccess];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"抢单成功刷新订单详情页面" object:nil];
        }
            break;
            
        case 4:
        {
            mainCtr.centerButton.hidden=YES;
            mainCtr.orderButton.hidden=YES;
            //新的拼车行程(司机)
            if (mainCtr.childViewControllers.count==0) {
                [mainCtr addChildViewController:mainCtr.navCtr];
                [mainCtr.view addSubview:mainCtr.navCtr.view];
            }
            PinChe_DriverOrderDetails * pincheCtr=[[PinChe_DriverOrderDetails alloc]init];
            mainCtr.navCtr.viewControllers=@[pincheCtr];
            mainCtr.navCtr.view.frame=mainCtr.view.bounds;
        }
            break;
            
        case 5:
        {
            mainCtr.centerButton.hidden=YES;
            mainCtr.orderButton.hidden=YES;
            //所有乘客退出(司机)
            if (mainCtr.childViewControllers.count==0) {
                [mainCtr addChildViewController:mainCtr.navCtr];
                [mainCtr.view addSubview:mainCtr.navCtr.view];
            }
            PinChe_DriverOrderDetails * pincheCtr=[[PinChe_DriverOrderDetails alloc]init];
            mainCtr.navCtr.viewControllers=@[pincheCtr];
            mainCtr.navCtr.view.frame=mainCtr.view.bounds;
        }
            break;
            
            
            
        default:
            break;
    }
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    
    [ApinXGPush stopPush];
    
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "WangBaoCai.Apin_Driver" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Apin_Driver" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Apin_Driver.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
