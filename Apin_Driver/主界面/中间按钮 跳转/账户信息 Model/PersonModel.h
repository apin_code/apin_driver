//
//  PersonModel.h
//  Apin_Driver
//
//  Created by Apin on 15/8/13.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersonModel : NSObject
@property(nonatomic,copy)NSString *puserId;//用户id
@property(nonatomic,copy)NSString *pisBindAccount;//
@property(nonatomic,copy)NSString *pauditStatus;//
@property(nonatomic,copy)NSString *pheadPic;//头像路径
@property(nonatomic,copy)NSString *pdriver;//司机
@property(nonatomic,copy)NSString *pphone;//电话号
@property(nonatomic,copy)NSString *pplate;
@property(nonatomic,copy)NSString *auditStatus;
@end
