//
//  MapViewController.m
//  Apin_Driver
//
//  Created by yqy on 15/8/12.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#warning 9.10 17:12 改!!!!!

#import "MapViewController.h"
#import "AnnotationModel.h"
#import "BMapKit.h"
#import "DriverCenterViewController.h"
#import "ApinDriverMainViewController.h"
#import "UIImageView+WebCache.h"

//#define MyRoute @"http://192.168.1.50:8083/apin-back-interface-driver/api/apin/user/currentRideRoute/get?userId=%@"

//371328199301073018
@interface MapViewController ()<BMKMapViewDelegate, BMKLocationServiceDelegate>
{
    BMKMapView *_mapView;
    BMKLocationService *_locService;
    //    BMKPinAnnotationView *newAnnotationView;
    NSArray *_dataArr;
    NSArray *_driverDataArr;
    NSString *_telephoneStr;
    NSInteger _num; // 下一单
    NSInteger _count; // 下一个人
    BMKPointAnnotation *BmkAnnotation;
}

@property(nonatomic, assign) CLLocationDegrees localLatitude;//经度
@property(nonatomic, assign) CLLocationDegrees localLongitude;//纬度
@property(nonatomic, retain) UILabel *titleLabel;//开启服务label
@property(nonatomic, retain) UIButton *orderBtn;//待出发
@property(nonatomic, retain) UIImageView *xiaImageView;//下边框
@property(nonatomic, retain) UIButton *startBtn;//开启行程按钮
@property(nonatomic, retain) UIImageView *peoPleNumberImageView;//接单人数
@property(nonatomic, retain) UIImageView *shangImageView;//上边框
@property(nonatomic, retain) UIImageView *zhongImageView;//中间边框
@property(nonatomic, retain) UILabel *peopleNumberLable;//人数
@property(nonatomic, retain) UIButton *nextBtn;//下一单
@property(nonatomic, retain) UIImageView *photoImageView;//乘客头像
@property(nonatomic, retain) UILabel *nameLable;//乘客姓名
@property(nonatomic, retain) UIImageView *dateImageView ;//时间ImageView
@property(nonatomic, retain) UIImageView *siteImageView;//地点ImageView
@property(nonatomic, retain) UIImageView *peoNumberImageView;//人数ImageView
@property(nonatomic, retain) UILabel *dateLable;//时间
@property(nonatomic, retain) UILabel *timeLable;//time
@property(nonatomic, retain) UILabel *siteLable;//地点
/**已参与拼车人数*/
@property(nonatomic, retain) UILabel *peoNumberLabel;
@property(nonatomic, retain) UIButton *phoneBtn;//电话
@property(nonatomic, strong) UISwipeGestureRecognizer *leftSwipeGestureRecognizer;//向左滑动手势
@property(nonatomic, strong) UISwipeGestureRecognizer *rightSwipeGestureRecognizer;//向右滑动手势


@end

@implementation MapViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        _dataArr = [NSArray array];
        _driverDataArr = [NSArray array];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self creatUI];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(10 * KMapW, 34 * KMapH, 14 * KMapW, 20 * KMapH);
    [backBtn setImage:[UIImage imageNamed:@"接单返回按钮"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(touchBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    [self loadData];
    if (_dataArr.count!=0) {
        [self changeAttribute:0];
        
    }
}



- (void)creatUI
{
    _peoPleNumberImageView = [[UIImageView alloc] initWithFrame:CGRectMake(17 * KMapW, 435 * KMapH, 23 * KMapW, 20 * KMapH)];
    _peoPleNumberImageView.backgroundColor = [UIColor clearColor];
    _peoPleNumberImageView.image = [UIImage imageNamed:@"接单人数"];
    [self.view addSubview:_peoPleNumberImageView];
    //    _peoPleNumberImageView.hidden = YES;
    
    //上边框
    _shangImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 420 * KMapH, 375 * KMapW, 50 * KMapH)];
    _shangImageView.image = [UIImage imageNamed:@"接单上边框"];
    [self.view addSubview:_shangImageView];
    //    _shangImageView.hidden = YES;
    
    _zhongImageView = [[UIImageView alloc] initWithFrame:CGRectMake(8 * KMapW, 472 * KMapH, 359 * KMapW, 130 * KMapH)];
    _zhongImageView.image = [UIImage imageNamed:@"接单中间边框"];
    [self.view addSubview:_zhongImageView];
    //    _zhongImageView.hidden = YES;
    
    _xiaImageView = [[UIImageView alloc] initWithFrame:CGRectMake(4 * KMapW, 600 * KMapH, 367 * KMapW, 55 * KMapH)];
    _xiaImageView.image = [UIImage imageNamed:@"底边"];
    [self.view addSubview:_xiaImageView];
    //    _xiaImageView.hidden = YES;
    
    //人数
    _peopleNumberLable = [[UILabel alloc] initWithFrame:CGRectMake(42 * KMapW, 435 * KMapH, 30 * KMapW, 20 * KMapH)];
//    _peopleNumberLable.text = @"1/7";
    _peopleNumberLable.textColor = [UIColor whiteColor];
    [self.view addSubview:_peopleNumberLable];
    //    _peopleNumberLable.hidden = YES;
    
    //下一单
    _nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _nextBtn.frame = CGRectMake(289 * KMapW, 437 * KMapH, 47 * KMapW, 16 * KMapH);
    _nextBtn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_nextBtn];
    [_nextBtn setBackgroundImage:[UIImage imageNamed:@"接单下一单"] forState:UIControlStateNormal];
    [_nextBtn setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
    [_nextBtn addTarget:self action:@selector(touchNext) forControlEvents:UIControlEventTouchUpInside];
    //    _nextBtn.hidden = YES;
    
    //头像
    _photoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(30 * KMapW, 495 * KMapH, 60 * KMapW, 60 * KMapH)];
//    _photoImageView.backgroundColor = [UIColor redColor];
//    _photoImageView.image = [UIImage imageNamed:@"接单头像"];
    [self.view addSubview:_photoImageView];
    _photoImageView.layer.cornerRadius = _photoImageView.frame.size.width / 2;
    _photoImageView.layer.masksToBounds=YES;
    UITapGestureRecognizer * tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(myTapAction)];
    _photoImageView.userInteractionEnabled=YES;
    [_photoImageView addGestureRecognizer:tap];
    //    _photoImageView.hidden = YES;
    
    _nameLable = [[UILabel alloc] initWithFrame:CGRectMake(30 * KMapW, 555 * KMapH, 60 * KMapW, 30 * KMapH)];
//    _nameLable.text = @"姚女士";
    //    _nameLable.backgroundColor = [UIColor greenColor];
    _nameLable.textColor = [UIColor whiteColor];
    _nameLable.font = [UIFont systemFontOfSize:15.0];
    _nameLable.textAlignment = NSTextAlignmentCenter;
    _nameLable.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_nameLable];
    //    _nameLable.hidden = YES;
    
    //时间ImageView
    _dateImageView = [[UIImageView alloc] initWithFrame:CGRectMake(130 * KMapW, 490 * KMapH, 25 * KMapW, 25 * KMapH)];
    _dateImageView.backgroundColor = [UIColor clearColor];
    _dateImageView.image = [UIImage imageNamed:@"接单时间"];
    [self.view addSubview:_dateImageView];
    //    _dateImageView.hidden = YES;
    
    //起点ImageView
    _siteImageView = [[UIImageView alloc] initWithFrame:CGRectMake(132 * KMapW, 520 * KMapH, 20 * KMapW, 25 * KMapH)];
    _siteImageView.backgroundColor = [UIColor clearColor];
    _siteImageView.image = [UIImage imageNamed:@"接单探头"];
    [self.view addSubview:_siteImageView];
    //    _siteImageView.hidden = YES;
    
    //终点ImageView
    _peoNumberImageView = [[UIImageView alloc] initWithFrame:CGRectMake(132 * KMapW, 552 * KMapH, 20 * KMapW, 25 * KMapH)];
    _peoNumberImageView.image = [UIImage imageNamed:@"接单探头"];
    _peoNumberImageView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_peoNumberImageView];
    //    _peoNumberImageView.hidden = YES;
    
    //时间
    _dateLable = [[UILabel alloc] initWithFrame:CGRectMake(157 * KMapW, 490 * KMapH, 140 * KMapW, 25 * KMapH)];
    _dateLable.backgroundColor = [UIColor clearColor];
//    _dateLable.text = @"7月12日";
    _dateLable.textColor = [UIColor whiteColor];
    _dateLable.font = [UIFont systemFontOfSize:15.0];
    [self.view addSubview:_dateLable];
    //    _dateLable.hidden = YES;
    
    //地点
    _siteLable = [[UILabel alloc] initWithFrame:CGRectMake(156 * KMapW, 522 * KMapH, 160 * KMapW, 25 * KMapH)];
//    _siteLable.text = @"杭州萧山机场";
    _siteLable.textColor = [UIColor whiteColor];
    _siteLable.font = [UIFont systemFontOfSize:15.0];
    _siteLable.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_siteLable];
    //    _siteLable.hidden = YES;
    
    //人数
    _peoNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(157 * KMapW, 553 * KMapH, 160 * KMapW, 25 * KMapH)];
//    _peoNumberLabel.text = @"3人";
    _peoNumberLabel.textColor = [UIColor whiteColor];
    _peoNumberLabel.font = [UIFont systemFontOfSize:15.0];
    _peoNumberLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_peoNumberLabel];
    //    _peoNumberLabel.hidden = YES;
    
    //电话
    _phoneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _phoneBtn.frame = CGRectMake(325 * KMapW-25, 535 * KMapH-25, 50, 50);
    //    [_phoneBtn setImage:[UIImage imageNamed:@"电话"] forState:UIControlStateNormal];
    [_phoneBtn setBackgroundImage:[UIImage imageNamed:@"电话"] forState:UIControlStateNormal];
    [_phoneBtn addTarget:self action:@selector(callPhone:) forControlEvents:UIControlEventTouchUpInside];
    _phoneBtn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_phoneBtn];
    //    _phoneBtn.hidden = YES;
    
    
    //设置定位的精确度 默认:kCLLocationAccuracyBest
    [BMKLocationService setLocationDesiredAccuracy:kCLLocationAccuracyHundredMeters];
    //指定最小距离更新（米）默认:kCLDistanceFilterNone
    [BMKLocationService setLocationDistanceFilter:50.f];
    
    //初始化BMKLocationService
    //    _locService = [[BMKLocationService alloc] init];
    _locService = [[BMKLocationService alloc] init];
    _locService.delegate = self;
    //启动LocationService
    [_locService startUserLocationService];
    
    UIView *view =[[UIView alloc]initWithFrame:CGRectMake(0, 452 * KMapH, 305 * KMapW, 160 * KMapH)];
    view.backgroundColor = [UIColor clearColor];
    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipes:)];
    leftSwipe.direction=UISwipeGestureRecognizerDirectionLeft;
    
    UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipes:)];
    rightSwipe.direction = UISwipeGestureRecognizerDirectionRight;
    [view addGestureRecognizer:leftSwipe];
    [view addGestureRecognizer:rightSwipe];
    [self.view addSubview:view];
}
-(void)myTapAction{
    [_locService startUserLocationService];
}

#pragma mark-控件填充数据
-(void)handleSwipes:(UISwipeGestureRecognizer *)sender{
    
    if ([_driverDataArr[_num][@"passenger"] count]==1) {
        return;
    }
    
    if (sender.direction == UISwipeGestureRecognizerDirectionLeft) {
        _count++;
        if (_count == [_driverDataArr[_num][@"passenger"] count]) {
            _count=0;
            NSLog(@"哈哈哈也是没有更多数据%ld",_count);
            
        }else{
            NSLog(@"怎么回事??%ld",_count);
            
        }
        NSLog(@"向右滑");
        
    }else if (sender.direction == UISwipeGestureRecognizerDirectionRight) {
        _count--;
        if (_count == -1) {
            _count = [_driverDataArr[_num][@"passenger"] count] - 1;
        }else{
            NSLog(@"哈哈和神经内科%ld",_count);
            
        }
        NSLog(@"向左滑");
    }
    
    NSLog(@"-----------没有更多数据%ld-------%ld",_count,_num);
    _nameLable.text = _driverDataArr[_num][@"passenger"][_count][@"passenger"];
    
    if ([_nameLable.text isEqualToString:@""]||[_nameLable.text isEqualToString:@"null"]) {
        _nameLable.text=@"匿名";
    }
    //这个label展示的是名字加人数
    _nameLable.text = [NSString stringWithFormat:@"%@(%@)",_nameLable.text,_driverDataArr[_num][@"passenger"][_count][@"includePassenger"]];
    //头像设置
    [_photoImageView setImageWithURL:[NSURL URLWithString:_driverDataArr[_num][@"passenger"][_count][@"filepath"]] placeholderImage:[UIImage imageNamed:@"头像"]];
    _dateLable.text =  _driverDataArr[_num][@"passenger"][_count][@"setOutTime"];
    
    //设置起点
    _siteLable.text =  _driverDataArr[_num][@"passenger"][_count][@"routeStart"];
    //设置终点
    _peoNumberLabel.text =  _driverDataArr[_num][@"passenger"][_count][@"routeEnd"];
    BmkAnnotation = [[BMKPointAnnotation alloc] init];
    CLLocationCoordinate2D coor;
    coor.latitude = [_driverDataArr[_num][@"passenger"][_count][@"latitude"] doubleValue];
    coor.longitude = [_driverDataArr[_num][@"passenger"][_count][@"longitude"] doubleValue];
    BmkAnnotation.coordinate = coor;
    BmkAnnotation.title = @"乘客的位置";
    [_mapView addAnnotation:BmkAnnotation];
    [_mapView setCenterCoordinate:BmkAnnotation.coordinate animated:YES];
    
    _telephoneStr =  _driverDataArr[_num][@"passenger"][_count][@"phone"];
}

-(void)changeAttribute:(NSInteger )i{
    _peopleNumberLable.text =[NSString stringWithFormat:@"%@/%@",_driverDataArr[0][@"passengerNum"],_driverDataArr[i][@"maxPassengerNum"]];
    NSLog(@"电话号码 == %@",_peoNumberLabel.text);
    //是的
    _nameLable.text = _driverDataArr[i][@"passenger"][0][@"passenger"];
    if ([_nameLable.text isEqualToString:@""]) {
        _nameLable.text=@"匿名";
    }
    //这个label展示的是名字加人数
    _nameLable.text = [NSString stringWithFormat:@"%@(%@)",_nameLable.text,_driverDataArr[i][@"passenger"][0][@"includePassenger"]];

    [_photoImageView setImageWithURL:[NSURL URLWithString:_driverDataArr[i][@"passenger"][0][@"filepath"]] placeholderImage:[UIImage imageNamed:@"iconfont-morentouxiang"]];
    _dateLable.text = _driverDataArr[i][@"passenger"][0][@"setOutTime"];
    
    //设置起点及终点
    _siteLable.text = _driverDataArr[i][@"passenger"][0][@"routeStart"];
    _peoNumberLabel.text = _driverDataArr[i][@"passenger"][0][@"routeEnd"];
    _telephoneStr = _driverDataArr[i][@"passenger"][0][@"phone"];
    BmkAnnotation = [[BMKPointAnnotation alloc] init];
    CLLocationCoordinate2D coor;
    coor.latitude = [_driverDataArr[i][@"passenger"][0][@"latitude"] doubleValue];
    coor.longitude = [_driverDataArr[i][@"passenger"][0][@"longitude"] doubleValue];
    BmkAnnotation.coordinate = coor;
    BmkAnnotation.title = @"乘客的位置";
    [_mapView addAnnotation:BmkAnnotation];
    [_mapView setCenterCoordinate:BmkAnnotation.coordinate animated:YES];
    
}

- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id<BMKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[BMKPointAnnotation class]]) {
        BMKPinAnnotationView *newAnnotationView = [[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"myAnnotation"];
        newAnnotationView.pinColor = BMKPinAnnotationColorPurple;
        
        newAnnotationView.image = [UIImage imageNamed:@"iconfont-dingwei"];
        //        newAnnotationView.transform = CGAffineTransformMakeRotation(0.3);
        newAnnotationView.animatesDrop = YES;
        return newAnnotationView;
    }
    return nil;
}

- (void)startTheBtn{
    //开启行程
    _startBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _startBtn.backgroundColor = [UIColor clearColor];
    _startBtn.frame = CGRectMake(187.5 * KMapW-57.5, 642 * KMapH-25, 115, 53);
    [_startBtn setBackgroundImage:[UIImage imageNamed:@"接单开启行程"] forState:UIControlStateNormal];
    [self.view addSubview:_startBtn];
    [_startBtn addTarget:self action:@selector(touchStart) forControlEvents:UIControlEventTouchUpInside];
}

- (void)endTheBtn{
    UIButton *endBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    endBtn.frame = CGRectMake(187.5 * KMapW-57.5, 642 * KMapH-25, 115, 53);
    [endBtn setImage:[UIImage imageNamed:@"结束行程按钮"] forState:UIControlStateNormal];
    [endBtn addTarget:self action:@selector(touchEnd) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:endBtn];}


-(void)loadData{
    // #warning 完善参数
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString *userId=[defaults objectForKey:@"userId"];
    NSLog(@"%@",userId);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //    NSString *url = [NSString stringWithFormat:MyRoute,userId];
    [manager POST:MyRoute parameters:@{@"userId":userId} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"我的行程 = = %@",responseObject);
        if ([responseObject[@"head"][@"msg"] isEqualToString:@"成功"] ) {
            _driverDataArr = responseObject[@"body"];
            NSLog(@"_driverDataArr == %@",_driverDataArr);
            NSLog(@"_driberdataArr.count = %ld",_driverDataArr.count);
            if (_driverDataArr.count!=0) {
                _dataArr =responseObject[@"body"][0][@"passenger"];
                NSLog(@"老子砍死你丫的%ld",_dataArr.count);
                if ([responseObject[@"body"][0][@"status"] isEqualToString:@"0"]) {
                    [self startTheBtn];
                }else{
                    [self endTheBtn];
                }
//                [self creatUI];
            }
            if (_dataArr.count==0) {
                // 提示无数据
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"提示" message:@"无数据" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
                return ;
            }
            [self changeAttribute:0];
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"我的行程 Error = %@",error);
    }];
}


- (void)viewWillAppear:(BOOL)animated
{
    [_mapView viewWillAppear];
}

//通过经纬度获取当前地址
- (void)sendHttpRequest
{
    NSString *str = [NSString stringWithFormat:@"http://api.map.baidu.com/geocoder/v2/?ak=g3MnvGtTYDNiRj9eUGW6CHMM&callback=renderReverse&location=%f,%f&output=json&pois=0",_localLatitude,_localLongitude];
    str = [str stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: [NSURL URLWithString: str] cachePolicy: NSURLRequestUseProtocolCachePolicy timeoutInterval: 60];
    request.HTTPMethod = @"POST";
    NSString *bodyStr = @"mcode=WangBaoCai.Apin-Driver";
    request.HTTPBody = [bodyStr dataUsingEncoding: NSUTF8StringEncoding];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *result = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    NSLog(@"%@",result);
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [_mapView viewWillDisappear];
}

- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation
{
    NSLog
    (@"--------======= %f %f",userLocation.location.coordinate.longitude,userLocation.location.coordinate.latitude);
    _localLatitude = userLocation.location.coordinate.latitude;//获取经度
    _localLongitude = userLocation.location.coordinate.longitude;
    //设置地图跳转到用户位置
    [self inita];
    [_mapView setCenterCoordinate:userLocation.location.coordinate animated:YES];
    
    //展示用户位置
    _mapView.showsUserLocation = YES;
    //更新用户当前的坐标。
    [_mapView updateLocationData:userLocation];
}

- (void)inita
{
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(130 * KMapW, 15 * KMapH, 120 * KMapW, 50 * KMapH)];
    _titleLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_titleLabel];
    _titleLabel.text = @"服务已开启";
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.font = [UIFont boldSystemFontOfSize:19.0];
    _timeLable.hidden = YES;
    
    
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    bgImageView.image = [UIImage imageNamed:@"Apin_backgroundImage"];
    [self.view addSubview:bgImageView];
    
    
    
    _mapView = [[BMKMapView alloc] initWithFrame:CGRectMake(0, 65 * KMapH, 375 * KMapW, 351 * KMapH)];
    _mapView.delegate = self;
    [self.view addSubview:_mapView];
    _mapView.zoomEnabled=YES;
    _mapView.scrollEnabled = YES;
    _mapView.zoomLevel =14;
    _mapView.showMapScaleBar = YES;
    
}

- (void)touchBack
{
    if (self.navigationController.viewControllers.count == 1) {
        AppDelegate *appdelegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
        appdelegate.backBlock(self);
    }else{   
        [self.navigationController popViewControllerAnimated:NO];
    }
}

//开启行程点击方法
- (void)touchStart
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *userId = [userDefaults objectForKey:@"userId"];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameter = @{@"userId":userId,@"routeId":_driverDataArr[0][@"routeId"]};
    [manager GET:StartRouteURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *jihhhh = [NSString stringWithFormat:@"%@",responseObject[@"head"][@"code"]];
        if ([jihhhh isEqualToString:@"00000000"]) {
            
            [_startBtn removeFromSuperview];
            UIButton *endBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            endBtn.frame = CGRectMake(187.5 * KMapW-57.5, 642 * KMapH-25, 115, 53);
            [endBtn setImage:[UIImage imageNamed:@"结束行程按钮"] forState:UIControlStateNormal];
            [endBtn addTarget:self action:@selector(touchEnd) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:endBtn];
            
        }else{
            NSLog(@"开启行程失败");
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"无网络连接");
    }];
    
}

//结束行程
- (void)touchEnd
{
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString *userId=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"userId"]];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *parameter = @{@"userId":userId,@"routeId":_driverDataArr[_num][@"routeId"]};
    [manager POST:EndTheRoute parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"head"][@"code"]intValue] == 00000000) {
            UIAlertView *alt = [[UIAlertView alloc] initWithTitle:@"结束行程成功" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            alt.tag = 371328;
            [alt show];
        }else{
            
            UIAlertView *altEnd = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:responseObject[@"head"][@"msg"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [altEnd show];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"无网络连接");
    }];
}

#pragma mark-UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 371328) {
        if (buttonIndex == 0) {
            if (self.navigationController.viewControllers.count==1) {
                NSLog(@"sdqqqq");
                
                AppDelegate * delegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
                delegate.backBlock(self);
            }else{
                [self.navigationController popViewControllerAnimated:NO];
            }
        }
    }
}

//下一单
- (void)touchNext
{
    if (_driverDataArr.count==1) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"没有更多行程" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
    }
    _num++;
    if (_num==_driverDataArr.count) {
        NSLog(@"没有更多单");
        _num=0;
    }else{
        NSLog(@"走了这里了");
    }
    [self changeAttribute:_num];
}

//调用系统电话功能
- (void)callPhone:(UIButton *)btn
{
    NSLog(@"call!!!!");
    NSString *allString = [NSString stringWithFormat:@"tel:%@",_telephoneStr];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:allString]];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
