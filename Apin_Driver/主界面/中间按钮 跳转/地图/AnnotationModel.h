//
//  AnnotationModel.h
//  Apin_Driver
//
//  Created by yqy on 15/8/12.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import <Foundation/Foundation.h>
//371328199301073018
@interface AnnotationModel : NSObject

@property(nonatomic,copy)NSString * latitude;
@property(nonatomic,copy)NSString * longitude;
@property(nonatomic,copy)NSString * title;
@property(nonatomic,copy)NSString * subtitle;
///

@end
