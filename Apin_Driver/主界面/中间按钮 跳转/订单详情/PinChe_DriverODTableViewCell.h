//
//  PinChe_DriverODTableViewCell.h
//  司机端_订单详情页面
//
//  Created by Apin on 15/8/5.
//  Copyright (c) 2015年 zlp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PinChe_DriverODTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *rabOrderBtn;

@property (weak, nonatomic) IBOutlet UILabel *startLabel;

@property (weak, nonatomic) IBOutlet UILabel *endLabel;

@property (weak, nonatomic) IBOutlet UIImageView *headImg;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;


@property (weak, nonatomic) IBOutlet UILabel *percentLabel;



















@end
