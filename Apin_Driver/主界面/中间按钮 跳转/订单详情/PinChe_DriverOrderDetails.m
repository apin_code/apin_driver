//
//  PinChe_DriverOrderDetails.m
//  司机端_订单详情页面
//
//  Created by Apin on 15/8/5.
//  Copyright (c) 2015年 zlp. All rights reserved.
//

#import "PinChe_DriverOrderDetails.h"
#import "PinChe_DriverODTableViewCell.h"
#import "DriverOrderDatilsModel.h"
#import "MapViewController.h"

#define TEXTSTR_ONE @"当前没有行程,刷新试试"
#define TEXTSTR_TWO @"没有最新消息哦，过会儿再看看";
#define TEXTSTR_THREE @"已停止接单，不能查看待抢路线"

@interface PinChe_DriverOrderDetails ()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,MJRefreshBaseViewDelegate>
{
    NSMutableArray *_dataArr;
    int  _num;
    NSString *_theUserId;
    UIAlertView * _alert;
    
}

@property (nonatomic,strong)MJRefreshHeaderView *headView;
@property  (nonatomic,strong)MJRefreshFooterView *footView;
@property (nonatomic,strong) UILabel *noNetLabel; // 无网络 显示的view

@end
//static BOOL startGet;

@implementation PinChe_DriverOrderDetails
-(MJRefreshHeaderView *)headView{
    
    if (_headView==nil) {
        _headView = [[MJRefreshHeaderView alloc]init];
        _headView.scrollView=_myTableView;
        _headView.delegate=self;
    }
    return _headView;
}
-(MJRefreshFooterView *)footView{
    if (_footView == nil) {
        _footView = [[MJRefreshFooterView alloc]init];
        _footView.scrollView = _myTableView;
        _footView.delegate = self;
    }
    return _footView;
}

-(UILabel *)noNetLabel{
    if (!_noNetLabel) {
        _noNetLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _noNetLabel.font = [UIFont systemFontOfSize:25];
        _noNetLabel.textColor = [UIColor whiteColor];
        _noNetLabel.backgroundColor = [UIColor clearColor];
        _noNetLabel.numberOfLines = 0;
    }
    return _noNetLabel;
}

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    
    if (self=[super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        _dataArr = [[NSMutableArray alloc]init];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor clearColor];
    [_myTableView registerNib:[UINib nibWithNibName:@"PinChe_DriverODTableViewCell" bundle:nil] forCellReuseIdentifier:@"myCell"];
    _myTableView.tableFooterView = [[UIView alloc]init];
    
    [self.headView endRefreshing];
    [self.footView endRefreshing];
    // 手动调 上拉刷新
    _badgeLabel.text =@"0";
    _num =1;
    
    // 接收推送 刷新页面
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(tokenSuccessAndRefresh) name:@"抢单成功刷新订单详情页面" object:nil];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self getNetWork];
    
}

-(void)tokenSuccessAndRefresh{
    [self refreshViewBeginRefreshing:_headView];
    [_alert dismissWithClickedButtonIndex:0 animated:YES];
}

-(void)getNetWork{
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        NSLog(@"%ld",(long)status);
        
        if (status<=0) {
            NSString *str =@"无法获取行程，请查看网络设置";
            CGSize size = [str boundingRectWithSize:CGSizeMake(200, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.noNetLabel.font} context:nil].size;
            self.noNetLabel.frame = CGRectMake(0, 0, size.width, size.height);
            self.noNetLabel.center =self.view.center;
            self.noNetLabel.text = str;
            [self.view addSubview:self.noNetLabel];
            
            NSLog(@"无连接");
            
        }else if (status>0){
            
            [self refreshViewBeginRefreshing:_headView];
        }
    }];
    
}


-(void)dealloc{
    [_headView free];
    [_footView free];
}
#pragma mark - MJRefreshDelegate
-(void)refreshViewBeginRefreshing:(MJRefreshBaseView *)refreshView{
    if (refreshView==_headView) {
        _num=1;
    }else{
        _num++;
    }
    AppDelegate *del = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userId =[defaults objectForKey:@"userId"];
    _theUserId=userId;
    
    //    NSLog(@"startGet == %d",startGet);
    //       if (!startGet) {
    
    AFHTTPRequestOperationManager *manager= [AFHTTPRequestOperationManager manager];
    if (manager==nil) {
        return;
    }
    NSString * pageNum =[NSString stringWithFormat:@"%d",_num];
#if TARGET_IPHONE_SIMULATOR
    //模拟器
    NSDictionary *parameter = @{@"userId":userId,
                                @"street":@"长河路",
                                @"longitude":@"120.20000",
                                @"latitude":@"30.3333",
                                @"pageNum":pageNum,
                                @"pageSize":@"10"};
    
    
#elif TARGET_OS_IPHONE
    
    //真机
    if (!del.locaLonStr || !del.localLatStr||!userId) {
        del.loadURLView.hidden=YES;
        NSLog(@"%@   %@    %@",del.localLatStr,del.locaLonStr,userId);
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"信号不好,无法定位啦" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if(del.locaStreetStr==nil){
        del.locaStreetStr=@"";
    }
    NSLog(@"我就是我 == %@%@%@%@%@",userId,del.locaStreetStr,del.locaLonStr,del.localLatStr,pageNum);
    
    NSDictionary *parameter = @{@"userId":userId,
                                @"street":del.locaStreetStr,
                                @"longitude":del.locaLonStr,
                                @"latitude":del.localLatStr,
                                @"pageNum":pageNum,
                                @"pageSize":@"10"};
    
#endif
    del.loadURLView.hidden=NO;
    [manager POST:GetCarLinesURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [UIView animateWithDuration:0.25 animations:^{
            del.loadURLView.hidden=YES;
        }];
        //               NSLog(@"responseObject=-=-=-=-=- %@   \n %@",responseObject,responseObject[@"head"][@"msg"]);
        if (![responseObject[@"head"][@"msg"] isEqualToString:@"成功"]) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"获取失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
            return ;
        }
        // 角标
        _badgeLabel.text = [NSString stringWithFormat:@"%@",responseObject[@"body"][@"ownOrderNum"]];
        NSString *serviceStates =responseObject[@"body"][@"serviceStatus"];
        if (serviceStates.integerValue ==1) {
            // _bottomBtn.titleLabel.text =@"停止抢单";
            [_bottomBtn setTitle:@"停止抢单" forState:UIControlStateNormal];
        }else{
            //_bottomBtn.titleLabel.text =@"开始抢单";
            [_bottomBtn setTitle:@"开始抢单" forState:UIControlStateNormal];
        }
        NSArray *array = responseObject[@"body"][@"routes"];
        //                NSLog(@"array == %@",array);
        if (array.count ==0 && _dataArr.count == 0) {
            NSString *str =TEXTSTR_ONE;
            CGSize size = [str boundingRectWithSize:CGSizeMake(200, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.noNetLabel.font} context:nil].size;
            self.noNetLabel.frame = CGRectMake(0, 0, size.width, size.height);
            self.noNetLabel.center =self.view.center;
            self.noNetLabel.text = str;
            [self.view addSubview:self.noNetLabel];
            _myTableView.scrollEnabled=YES;
            [self performSelector:@selector(endRefresh:) withObject:refreshView afterDelay:0.5];
            return;
        }
        
        if (_num==1) {
            [_dataArr removeAllObjects];
        }
        
        //                NSLog(@"阿卡家地方 == %@",array);
        if (array.count) {
            [self.noNetLabel removeFromSuperview];
        }
        
        for (int i=0; i<array.count; i++) {
            
            DriverOrderDatilsModel *model=[[DriverOrderDatilsModel alloc]init];
            model.filePathStr=array[i][@"filePath"];
            model.passengerIdStr=array[i][@"passengerId"];
            model.passengerNameStr=array[i][@"passenger"];
            model.setOutTimeStr=array[i][@"setOutTime"]; // 出发时间
            model.routeStartStr=array[i][@"routeStart"];
            model.routeEndStr=array[i][@"routeEnd"];
            model.passengerNumStr=array[i][@"passengerNum"];
            model.maxPassengerNumStr=array[i][@"maxPassengerNum"];
            model.routeIdStr = array[i][@"routeId"]; // 行程id
            [_dataArr addObject:model];
            //                    NSLog(@"model -=-=-==-=- %@",model);
        }
        
        [self performSelector:@selector(endRefresh:) withObject:refreshView afterDelay:0.5];
        [_myTableView reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self performSelector:@selector(endRefresh:) withObject:refreshView afterDelay:0.5];
        NSLog(@"ERROR:  %@",error);
        [UIView animateWithDuration:0.25 animations:^{
            del.loadURLView.hidden=YES;
        }];
        [self performSelector:@selector(endRefresh:) withObject:refreshView afterDelay:0.5];
        [_myTableView reloadData];
    }];
    
    /*else{
     del.bigLoadURLView.hidden=YES;
     NSString *str =TEXTSTR_TWO;
     CGSize size = [str boundingRectWithSize:CGSizeMake(200, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.noNetLabel.font} context:nil].size;
     self.noNetLabel.frame = CGRectMake(0, 0, size.width, size.height);
     self.noNetLabel.center =self.view.center;
     self.noNetLabel.text = str;
     _myTableView.userInteractionEnabled=NO;
     [self.view addSubview:self.noNetLabel];
     }
     */
    
}

-(void)endRefresh:(MJRefreshBaseView *)sender{
    //停止转动
    [sender endRefreshing];
    
}
// 设置 button 的文字
/*
 -(void)setBottomBtn:(UIButton *)bottomBtn{
 
 if (!startGet) {
 [bottomBtn setTitle:@"停止接单" forState:UIControlStateNormal];
 
 }else {
 [bottomBtn setTitle:@"开始抢单" forState:UIControlStateNormal];
 }
 _bottomBtn=bottomBtn;
 
 }
 */


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _dataArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PinChe_DriverODTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"myCell"];
    
    DriverOrderDatilsModel *model=_dataArr[indexPath.row];
    [cell.headImg setImageWithURL:[NSURL URLWithString:model.filePathStr]];
    cell.nameLabel.text=model.passengerNameStr;
    cell.timeLabel.text=model.setOutTimeStr;
    cell.startLabel.text=model.routeStartStr;
    cell.endLabel.text= model.routeEndStr;
    cell.percentLabel.text=[NSString stringWithFormat:@"%@/%@",model.passengerNumStr,model.maxPassengerNumStr];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegate.loadURLView.hidden=NO;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userId =[defaults objectForKey:@"userId"];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if (manager==nil) {
        return;
    }
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    DriverOrderDatilsModel *model = _dataArr[indexPath.row];
    // userId=%@&routeId=%@
    NSDictionary *parameter = @{@"userId":userId,@"routeId":model.routeIdStr};
    
    [manager GET:RideRouteURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [UIView animateWithDuration:0.25 animations:^{
            delegate.loadURLView.hidden=YES;
        }];
        //开始抢单成功
        NSLog(@"开始抢单成功= %@",responseObject);
        if ([responseObject[@"head"][@"msg"] isEqualToString:@"成功"]) {
            _alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"排上号了,稍等..." delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
            _alert.tag=2;
            [_alert show];
            [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(dissmissAlertView) userInfo:nil repeats:NO];
        }else{
            _alert.title=@"提示";
            _alert.message=responseObject[@"head"][@"msg"];
            _alert.tag=1;
            [_alert show];
            [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(dissmissAlertView) userInfo:nil repeats:NO];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [UIView animateWithDuration:0.25 animations:^{
            delegate.loadURLView.hidden=YES;
        }];
        if (error.code==3840||error.code==-1011) {
            [self creatAlertViewWithMassage:@"请求失败"];
        }else if (error.userInfo[@"NSDebugDescription"]&&[[NSString stringWithFormat:@"%@",error.userInfo[@"NSDebugDescription"]] length]<30) {
            [self creatAlertViewWithMassage:error.userInfo[@"NSDebugDescription"]];
        }else if(error.userInfo[@"NSLocalizedDescription"]&&[[NSString stringWithFormat:@"%@",error.userInfo[@"NSLocalizedDescription"]] length]>30){
            [self creatAlertViewWithMassage:error.userInfo[@"NSLocalizedDescription"]];
        }else{
            [self creatAlertViewWithMassage:@"请求数据失败,请稍后重试"];
        }
        [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(dissmissAlertView) userInfo:nil repeats:NO];
    }];
    
}
-(void)creatAlertViewWithMassage:(NSString *)massage{
    UIAlertView*alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:massage delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView show];
}
-(void)dissmissAlertView{
    [_alert dismissWithClickedButtonIndex:0 animated:YES];
    [self refreshViewBeginRefreshing:_headView];
    if (_dataArr.count == 0) {
        if (_alert.tag==2) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"因服务器原因,数据返回较慢,请稍后..." delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }
    
}

-(void)rabOrder:(UIButton *)btn{
    
    NSLog(@"%ld",btn.tag-100);
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return ([[UIScreen mainScreen]bounds].size.height/568)*76;
}

// 弹框
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag!=10) {
        return;
    }
    if (buttonIndex==0) {
        //        NSString *str =TEXTSTR_THREE;
        //        CGSize size = [str boundingRectWithSize:CGSizeMake(200, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.noNetLabel.font} context:nil].size;
        //        self.noNetLabel.frame = CGRectMake(0, 0, size.width, size.height);
        //        self.noNetLabel.center =self.view.center;
        //        self.noNetLabel.text = str;
        //        [self.view addSubview:self.noNetLabel];
        //        _myTableView.userInteractionEnabled=NO;
        //        [_bottomBtn setTitle:@"开始抢单" forState:UIControlStateNormal];
        //        [_dataArr removeAllObjects];
        //        [_myTableView reloadData];
        //        [_headView endRefreshing];
        //        _myTableView.scrollEnabled = NO;
        //  startGet=!startGet; //
        // 关闭 连接
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        if (manager==nil) {
            return;
        }
        NSString *url =[NSString stringWithFormat:StopService,_theUserId];
        [manager POST:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSString *str =responseObject[@"head"][@"msg"];
            NSLog(@"停止服务  = = str =%@",str);
            if ([str isEqualToString:@"成功"]) {
                [self getNetWork];
            }
            NSLog(@"%@",responseObject[@"head"][@"msg"]);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }
}


- (IBAction)backBtn:(UIButton *)sender {
    AppDelegate * delegata=(AppDelegate *)[UIApplication sharedApplication].delegate;
    if (delegata.backBlock) {
        delegata.backBlock(self);
    }
    
}


- (IBAction)rightCornerBtn:(id)sender {
    UIButton *btn =sender;
    [self endRefresh:_headView];
    [self endRefresh:_footView];
    if ([_badgeLabel.text isEqualToString:@"0"]) {
        btn.userInteractionEnabled=NO;
        return;
    }else{
        btn.userInteractionEnabled=YES;
    }
    MapViewController *mapVC = [[MapViewController alloc]init];
    [self.navigationController pushViewController:mapVC animated:NO];
    
}

- (IBAction)bottomBtn:(UIButton *)sender {
    /*   if (!startGet) {
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"确认停止抢单?" message:nil delegate:self cancelButtonTitle:@"是" otherButtonTitles:@"否", nil];
     alert.tag =10;
     [alert show];
     }else if(startGet){
     // 开始抢单 的连接
     //    [self endRefresh:_headView];
     
     [_bottomBtn setTitle:@"停止抢单" forState:UIControlStateNormal];
     startGet=!startGet;
     self.noNetLabel.text=@"";
     [self refreshViewBeginRefreshing:_headView];
     }
     */
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if (manager==nil) {
        return;
    }
    NSString *startUrl = [NSString stringWithFormat:StartService,_theUserId];
    if ([sender.currentTitle isEqualToString:@"开始抢单"]) {
        [manager POST:startUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSString *str =responseObject[@"head"][@"msg"];
            NSLog(@"停止服务  = = str =%@",str);
            if ([str isEqualToString:@"成功"]) {
                [self getNetWork];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"确认停止抢单?" message:nil delegate:self cancelButtonTitle:@"是" otherButtonTitles:@"否", nil];
        alert.tag =10;
        [alert show];
        
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
