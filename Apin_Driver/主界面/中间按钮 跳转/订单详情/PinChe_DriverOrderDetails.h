//
//  PinChe_DriverOrderDetails.h
//  司机端_订单详情页面
//
//  Created by Apin on 15/8/5.
//  Copyright (c) 2015年 zlp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PinChe_DriverOrderDetails : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *myTableView;

@property (weak, nonatomic) IBOutlet UIButton *bottomBtn;

@property (weak, nonatomic) IBOutlet UIButton *rightCornerBtn;


@property (weak, nonatomic) IBOutlet UILabel *badgeLabel;



//@property(nonatomic,copy)void(^backBlock)(void);

-(void)tokenSuccess;

@end
