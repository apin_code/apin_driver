//
//  ApinXGPush.h
//  Apin
//
//  Created by Apin on 15/8/17.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApinXGPush : NSObject

+(void)startPushWithAccount:(NSString *)account;
+(void)stopPush;
@end
