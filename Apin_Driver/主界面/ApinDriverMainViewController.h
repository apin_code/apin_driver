//
//  ApinDriverMainViewController.h
//  Apin_Driver
//
//  Created by Apin on 15/8/5.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApinDriverMainViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *centerButton;
@property (weak, nonatomic) IBOutlet UIButton *orderButton;
@property(nonatomic,assign)BOOL loginOK;
@property(nonatomic,strong)UINavigationController * navCtr;
@end
