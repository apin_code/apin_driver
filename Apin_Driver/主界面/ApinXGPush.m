//
//  ApinXGPush.m
//  Apin
//
//  Created by Apin on 15/8/17.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "ApinXGPush.h"
#import "XGPush.h"
#import "XGSetting.h"
@implementation ApinXGPush

+ (void)registerPushForIOS8{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= _IPHONE80_
    
    //Types
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    
    //Actions
    UIMutableUserNotificationAction *acceptAction = [[UIMutableUserNotificationAction alloc] init];
    
    acceptAction.identifier = @"ACCEPT_IDENTIFIER";
    acceptAction.title = @"Accept";
    
    acceptAction.activationMode = UIUserNotificationActivationModeForeground;
    acceptAction.destructive = NO;
    acceptAction.authenticationRequired = NO;
    
    //Categories
    UIMutableUserNotificationCategory *inviteCategory = [[UIMutableUserNotificationCategory alloc] init];
    
    inviteCategory.identifier = @"INVITE_CATEGORY";
    
    [inviteCategory setActions:@[acceptAction] forContext:UIUserNotificationActionContextDefault];
    
    [inviteCategory setActions:@[acceptAction] forContext:UIUserNotificationActionContextMinimal];
    
    NSSet *categories = [NSSet setWithObjects:inviteCategory, nil];
    
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:categories];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
#endif
}
+ (void)registerPush{
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
}


+(void)startPushWithAccount:(NSString *)account{
    
    
    AppDelegate * delegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
//    NSString *identifier = [[NSBundle mainBundle] bundleIdentifier];
//    NSLog(@"identifier==%@",identifier);
    //注销之后需要再次注册前的准备
    void (^successCallback)(void) = ^(void){
        //如果变成需要注册状态
        if(![XGPush isUnRegisterStatus])
        {
            //iOS8注册push方法
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= _IPHONE80_
            
            float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
            if(sysVer < 8){
                [self registerPush];
            }
            else{
                [self registerPushForIOS8];
            }
#else
            //iOS8之前注册push方法
            //注册Push服务，注册后才能收到推送
            [self registerPush];
#endif
        }
        
    };
    [XGPush unRegisterDevice:^{
        NSLog(@"注销设备成功");
        
        
        
        [XGPush initForReregister:successCallback];
        NSLog(@"delegate.tokenStr=%@",delegate.tokenStr);
        [XGPush setAccount:account];
//        [XGPush setAccount:@"17767106659"];
        [XGPush registerDeviceStr:delegate.tokenStr];
    } errorCallback:^{
        
    }];
    
}
+(void)stopPush{
//    return;
    [XGPush unRegisterDevice:^{
        NSLog(@"%@ 注销设备成功",[self class]);
    } errorCallback:^{
        NSLog(@"%@ 注销设备失败",[self class]);
     }];
}
@end
