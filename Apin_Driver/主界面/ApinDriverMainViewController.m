//
//  ApinDriverMainViewController.m
//  Apin_Driver
//
//  Created by Apin on 15/8/5.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "ApinDriverMainViewController.h"
#import "PinChe_DriverOrderDetails.h"
#import "MapViewController.h"
#import "ApinXGPush.h"
#import "LoginViewController.h"
//#import "MyOrderViewController.h"
#import "DriverCenterViewController.h"
//#import "DriverFatherViewController.h"
@interface ApinDriverMainViewController ()<UIAlertViewDelegate>
{
    NSString * _download_url;
    
}
@end

@implementation ApinDriverMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
//    NSLog(@"self.view.frame.size.height=%.2f",self.view.frame.size.height);
//    NSLog(@"self.view.frame.size.width=%.2f",self.view.frame.size.width);
    
//    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
//    btn.frame = CGRectMake(100, 100, 100, 40);
//    btn.backgroundColor = [UIColor redColor];
//    [btn addTarget:self action:@selector(touchBtn) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:btn];
    
    
    
    [self login];
    // Do any additional setup after loading the view from its nib.
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1001) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_download_url ]];
    }else if (alertView.tag==1000){
        if (buttonIndex==1) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_download_url ]];
        }
    }
}
//检查版本更新
-(void)check{
    //    return;
    AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
//    AppDelegate * delegate=[UIApplication sharedApplication].delegate;
//    delegate.loadURLView.hidden=NO;
//    return;
    if (manager==nil) {
        return;
    }
    //    检查版本更新
    //#define CheakURL  KLocahostID@"/util/app/version/check" //?osType=4&channel_id=00000&app_version=1.0.0&package=com.android.apin //channel_id=00000   app_version 版本  package boundleID
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    //    CFShow(infoDictionary);
    NSString * str=[[NSBundle mainBundle] bundleIdentifier];
    NSString * channel_id=nil;
    if ([str isEqualToString:@"com.apindriver.cn"]) {
        channel_id=@"00000";
    }else{
        channel_id=@"11111";
    }
    
    NSDictionary * dic=@{@"channel_id":channel_id,@"app_version":[infoDictionary objectForKey:@"CFBundleShortVersionString"],@"package":[[NSBundle mainBundle] bundleIdentifier]};
    NSLog(@"[[NSBundle mainBundle] bundleIdentifier]==%@",[[NSBundle mainBundle] bundleIdentifier]);
    [manager POST:CheakURL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"responseObject==%@",responseObject);
        if ([[ NSString stringWithFormat:@"%@",responseObject[@"error_no"]] isEqualToString:@"0"]&&responseObject[@"resultlist"]) {
            _download_url=responseObject[@"resultlist"][@"download_url"];
            if ([responseObject[@"resultlist"][@"force_update"] intValue]==0) {
                UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"有新版本可更新" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定",nil];
                alert.tag=1000;
                [alert show];
            }else if ([responseObject[@"resultlist"][@"force_update"] intValue]==1){
                UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"更新新版本才可继续使用" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
                alert.tag=1001;
                [alert show];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error==%@",error);
        
        if (error.code==3840||error.code==-1011) {
            [self creatAlertWithMessage:@"请求失败"];
        }else if (error.userInfo[@"NSDebugDescription"]&&[[NSString stringWithFormat:@"%@",error.userInfo[@"NSDebugDescription"]] length]<30) {
            
            
            [self creatAlertWithMessage:error.userInfo[@"NSDebugDescription"]];
            
            
            
        }else if(error.userInfo[@"NSLocalizedDescription"]&&[[NSString stringWithFormat:@"%@",error.userInfo[@"NSLocalizedDescription"]] length]>30){
            
            
            [self creatAlertWithMessage:error.userInfo[@"NSLocalizedDescription"]];
            
            
            
        }else{
            [self creatAlertWithMessage:@"请求数据失败,请稍后重试"];
        }
        
    }];
}

-(void)creatAlertWithMessage:(NSString *)message{
    UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:message delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView show];
}
-(void)login{
    
    _loginOK=NO;
   
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.view.frame=CGRectMake(KBgSmallViewX, KBgSmallViewY, KBgSmallViewW, KBgSmallViewH+12*KScaleH);
     
}
-(void)viewDidAppear:(BOOL)animated{
    [self check];
}
-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.view.frame=CGRectMake(KBgSmallViewX, KBgSmallViewY, KBgSmallViewW, KBgSmallViewH);
    
}

-(void)initView{
//    self.navigationController.navigationBarHidden=YES;
    AppDelegate * delegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    UIImageView * bgImageView=[[UIImageView alloc]initWithFrame:self.view.bounds];
    bgImageView.image=[UIImage imageNamed:@"主界面bg.jpg"];
    [self.view addSubview:bgImageView];
    _navCtr=[[UINavigationController alloc]init];
    _navCtr.view.frame=CGRectMake(KBgSmallViewX, KBgSmallViewY, KBgSmallViewW, 308*KScaleH);
    
    NSString* deviceName = [[UIDevice currentDevice] systemName];
    NSLog(@"deviceName==%@",deviceName);
    NSString* phoneModel = [[UIDevice currentDevice] model];
    NSLog(@"phoneModel==%@",phoneModel);
    NSString* phoneVersion = [[UIDevice currentDevice] systemVersion];
    NSLog(@"phoneVersion==%@",phoneVersion);
    
    _navCtr.view.center=self.view.center;
    _navCtr.view.backgroundColor=[UIColor clearColor];
    _navCtr.navigationBarHidden=YES;
    __block ApinDriverMainViewController * dCtr=self;
    __block AppDelegate * del=delegate;
    [delegate setBackBlock:^(UIViewController *ctr) {
        dCtr.centerButton.hidden=NO;
        dCtr.orderButton.hidden=NO;
        [_navCtr willMoveToParentViewController:dCtr];
        [_navCtr.view removeFromSuperview];
        [_navCtr removeFromParentViewController];
        del.loadURLView.hidden=YES;
    }];
}
//个人中心按钮
- (IBAction)centerButtonAction:(id)sender {
//    AppDelegate * delegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
//    __block ApinDriverMainViewController * dCtr=self;
//    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
//    if ([defaults objectForKey:@"phone"]&&[defaults objectForKey:@"password"]) {
//        
//        
//    }
//#warning 记录登录状态
//    if (_loginOK) {
//        DriverCenterViewController * driverCtr=[[DriverCenterViewController alloc]init];
//        _navCtr.viewControllers=@[driverCtr];
//       
//    }
    //判断是否登录
    _centerButton.hidden=YES;
    _orderButton.hidden=YES;
//    _navCtr.view.center=self.view.center;
    _navCtr.view.frame=CGRectMake(KBgSmallViewX, KBgSmallViewY, KBgSmallViewW, KBgSmallViewH);
    if (_loginOK) {
        DriverCenterViewController * driverCtr=[[DriverCenterViewController alloc]init];
        _navCtr.viewControllers=@[driverCtr];
    }else{
        LoginViewController * loginCtr=[[LoginViewController alloc]init];
        _navCtr.viewControllers=@[loginCtr];
    }
    
    [self addChildViewController:_navCtr];
    [self.view addSubview:_navCtr.view];
    
}

- (IBAction)orderInfoButtonAction:(id)sender {
    
//    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
    
    
    
    
    if (_loginOK) {
        
        AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
        if (manager==nil) {
            return;
        }
        NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
        
        [manager POST:GetCentifiedResultURL parameters:@{@"userId":[defaults objectForKey:@"userId"]} success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([responseObject[@"head"][@"msg"] isEqualToString:@"成功"]) {
                switch ([responseObject[@"body"] integerValue]) {
                    case 0:
                    {
                        //在审核当中
                        UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"正在审核当中..." delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
                        [alertView show];
                    }
                        break;
                        
                    case 1:
                    {
                        _centerButton.hidden=YES;
                        _orderButton.hidden=YES;
                        PinChe_DriverOrderDetails * pincheCtr=[[PinChe_DriverOrderDetails alloc]init];
                        //        _navCtr.viewControllers=@[driverCtr];
                        _navCtr.viewControllers =@[pincheCtr];
                        _navCtr.view.frame=CGRectMake(0, 0,KScreenW , KScreenH);
                        [self addChildViewController:_navCtr];
                        [self.view addSubview:_navCtr.view];
                    }
                        break;
                        
                    case 2:
                    {
                        //审核没通过
                        UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"审核没有通过,请重新审核" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
                        [alertView show];
                    }
                        break;
                        
                    case 3:
                    {
                        //未审核
                        UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"你还未进行认证,请进入个人中心前往认证" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
                        [alertView show];
                    }
                        break;
                        
                        
                    default:
                        break;
                }
            }else{
                UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:responseObject[@"head"][@"msg"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
                [alert show];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"连接服务器失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
            [alert show];
            
//            _centerButton.hidden=YES;
//            _orderButton.hidden=YES;
//            PinChe_DriverOrderDetails * pincheCtr=[[PinChe_DriverOrderDetails alloc]init];
//            //        _navCtr.viewControllers=@[driverCtr];
//            _navCtr.viewControllers =@[pincheCtr];
//            _navCtr.view.frame=CGRectMake(0, 0,KScreenW , KScreenH);
//            [self addChildViewController:_navCtr];
//            [self.view addSubview:_navCtr.view];
//            NSLog(@"error==%@",error);
        }];
        
        
        
//        AppDelegate * delegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
//        if ([delegate.personmodel.auditStatus intValue]==0) {
//            
//            return;
//        }else if ([delegate.personmodel.auditStatus intValue]==1){
//            
//        }else if ([delegate.personmodel.auditStatus intValue]==2){
//            
//            return;
//        }else if ([delegate.personmodel.auditStatus intValue]==3){
//            
//            return;
//        }

        
        
        
    }else{
        _centerButton.hidden=YES;
        _orderButton.hidden=YES;
        LoginViewController * loginCtr=[[LoginViewController alloc]init];
        _navCtr.viewControllers=@[loginCtr];
        _navCtr.view.frame=CGRectMake(KBgSmallViewX, KBgSmallViewY, KBgSmallViewW, KBgSmallViewH);
        [self addChildViewController:_navCtr];
        [self.view addSubview:_navCtr.view];
    }

    
//    _centerButton.hidden=YES;
    
    
    
}

- (void)touchBtn
{
    MapViewController *mapsssVC = [[MapViewController alloc] init];
    if (_navCtr) {
        _navCtr.viewControllers=@[mapsssVC];
        _navCtr.view.frame=CGRectMake(0, 0, KScreenW, KScreenH);
        [self.view addSubview:_navCtr.view];
        [self addChildViewController:_navCtr];
        _centerButton.hidden=YES;
        _orderButton.hidden=YES;
    }
}



@end
