//
//  DriverFatherViewController.m
//  Apin_Driver
//
//  Created by Apin on 15/8/5.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "DriverFatherViewController.h"
#import "ApinDriverMainViewController.h"
@interface DriverFatherViewController ()

//@property (weak, nonatomic) IBOutlet UIImageView *centerBgImageView;

@end

@implementation DriverFatherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadSubView];
    // Do any additional setup after loading the view from its nib.
}
-(void)loadSubView{
    _topTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(75*KScaleW, 13*KScaleH, 89*KScaleW, 21*KScaleH)];
    _topTitleLabel.textAlignment=NSTextAlignmentCenter;
    _topTitleLabel.font=[UIFont fontWithName:nil size:18*KScaleH];
    _topTitleLabel.textColor=[UIColor whiteColor];
    [self.view addSubview:_topTitleLabel];
    
    _backButton=[[UIButton alloc]initWithFrame:CGRectMake(15*KScaleW, 9*KScaleH, 30, 35)];
//    [_backButton setBackgroundImage:[UIImage imageNamed:@"返回按钮"] forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_backButton];
    _backImageView=[[UIImageView alloc]initWithFrame:CGRectMake(15*KScaleW, 9*KScaleH, 15, 30)];
    _backImageView.image=[UIImage imageNamed:@"返回按钮"];
    [self.view addSubview:_backImageView];
    
    _topImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 240*KScaleW, 44*KScaleH)];
    _topImageView.image=[UIImage imageNamed:@"头部边框"];
    [self.view addSubview:_topImageView];
    
    _centerBgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(2.5*KScaleW, 45*KScaleH, 235*KScaleW, 218*KScaleH)];
    _centerBgImageView.image=[UIImage imageNamed:@"中间栏大"];
    [self.view addSubview:_centerBgImageView];
    
    _bottomImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 264*KScaleH, 240*KScaleW, 44*KScaleH)];
    _bottomImageView.image=[UIImage imageNamed:@"底部边框"];
    [self.view addSubview:_bottomImageView];
    
    _bottomView=[[UIView alloc]initWithFrame:CGRectMake(0, 264*KScaleH, 240*KScaleW, 44*KScaleH)];
    [self.view addSubview:_bottomView];
}
-(void)backButtonAction:(UIButton*)sender{
    if (self.navigationController.viewControllers.count<2) {
        
        AppDelegate * delegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
        if (delegate.backBlock) {
            delegate.backBlock(self);
        }
        
    
        ApinDriverMainViewController * apinCtr=(ApinDriverMainViewController*)self.navigationController.presentingViewController;
        apinCtr.centerButton.hidden=NO;
    }else{
        [self.navigationController popToViewController:self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] animated:NO];
    }
}

-(void)setTopTitleString:(NSString *)topTitleString{
    _topTitleString=[topTitleString copy];
    _topTitleLabel.text=topTitleString;
}
-(void)setBgImage:(UIImage *)bgImage{
    if (_bgImage!=bgImage) {
        _bgImage=bgImage;
        _centerBgImageView.image=_bgImage;
    }
    
    
}


@end
