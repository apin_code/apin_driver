//
//  DriverFatherViewController.h
//  Apin_Driver
//
//  Created by Apin on 15/8/5.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DriverFatherViewController : UIViewController
//-(void)backButtonAction:(UIButton*)sender;
@property(nonatomic,copy)NSString * topTitleString;
//@property (weak, nonatomic) IBOutlet UIView *buttomView;
@property(nonatomic,strong)UIView * bottomView;
@property(nonatomic,strong)UIImage * bgImage;
@property(nonatomic,strong)UIImageView * topImageView;
@property (strong, nonatomic)  UILabel *topTitleLabel;
@property (strong, nonatomic)  UIButton *backButton;
@property (strong, nonatomic)  UIImageView * centerBgImageView;
//@property(nonatomic,copy)void(^backBlock)(void);
@property (nonatomic, strong) UIImageView * bottomImageView;

@property(nonatomic,strong)UIImageView * backImageView;

@end
