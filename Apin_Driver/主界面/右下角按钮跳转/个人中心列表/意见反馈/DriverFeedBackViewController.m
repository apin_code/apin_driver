//
//  DriverFeedBackViewController.m
//  Apin_Driver
//
//  Created by Apin on 15/8/5.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "DriverFeedBackViewController.h"

@interface DriverFeedBackViewController ()<UITextViewDelegate,UIAlertViewDelegate>
{
    CGRect _keyBoardRect;
 
}
@end

@implementation DriverFeedBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self loadTextView];
    [self loadKeyBoardObserver];
    NSLog(@"高度viewDidLoad == =%f",self.view.frame.size.height);
    self.navigationController.view.frame=CGRectMake(KBgSmallViewX,KBgSmallViewY,KBgSmallViewW,324*KScaleH);
}
-(void)viewWillAppear:(BOOL)animated{
//
    NSLog(@"高度 == =%f",self.view.frame.size.height);
}
-(void)viewWillDisappear:(BOOL)animated{
    
    
}

// 键盘的监听
-(void)loadKeyBoardObserver{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyBoardDidHidden) name:UIKeyboardDidHideNotification object:nil];
}

-(void)loadTextView{
    
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineSpacing = 10;// 字体的行间距
    
    // 设置 暗文
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont systemFontOfSize:13*KScaleH],NSParagraphStyleAttributeName:paragraphStyle,NSForegroundColorAttributeName:[UIColor whiteColor]
                                 };
    _myTextView.attributedText=[[NSAttributedString alloc]initWithString:@"输入你的意见..." attributes:attributes];
    
    _myTextView.returnKeyType = UIReturnKeyDefault;
    
    _myTextView.contentSize=CGSizeMake(240*KScaleW, MAXFLOAT);
    
    
    
    //定义一个toolBar
    UIToolbar * topView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 30)];
    
    //设置style
    [topView setBarStyle:UIBarStyleBlack];
    
    //定义两个flexibleSpace的button，放在toolBar上，这样完成按钮就会在最右边
    UIBarButtonItem * button1 =[[UIBarButtonItem  alloc]initWithBarButtonSystemItem:                                        UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem * button2 = [[UIBarButtonItem  alloc]initWithBarButtonSystemItem:                                        UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    //定义完成按钮
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStyleDone  target:self action:@selector(resignKeyboard)];
    
    //在toolBar上加上这些按钮
    NSArray * buttonsArray = [NSArray arrayWithObjects:button1,button2,doneButton,nil];
    [topView setItems:buttonsArray];
    
    [_myTextView setInputAccessoryView:topView];
    
}

-(void)resignKeyboard{
    
    [_myTextView resignFirstResponder];
}
// 键盘出现
-(void)keyBoardDidShow:(NSNotification *)noti{
    
    NSValue *value= [noti.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    _keyBoardRect=[value CGRectValue];
    
}
// 键盘消失
-(void)KeyBoardDidHidden{
    _myTextView.contentOffset=CGPointZero;
    
}


#pragma mark -UITextViewDelegate
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return  YES;
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@"输入你的意见..."]) {
        _myTextView.text=@"";
    }
    
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    
}
// textView 根据键盘高度 滚动
-(void)textViewDidChange:(UITextView *)textView{
    CGFloat textHeight;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        CGRect textFrame=[[textView layoutManager]usedRectForTextContainer:[textView textContainer]];
        textHeight = textFrame.size.height;
    }else {
        
        textHeight = _myTextView.contentSize.height;
    }
    // 计算差值
    CGFloat tempNum = _keyBoardRect.origin.y-self.navigationController.view.frame.origin.y-_myTextView.frame.origin.y-textHeight-30;
    NSLog(@"我在动了 = = %f",tempNum);
    if (tempNum<0) {
        _myTextView.contentOffset=CGPointMake(0, -tempNum);
    }
}


- (IBAction)backBtn:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:NO];
    
}

- (IBAction)feedBackOpinionBtn:(UIButton *)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userId =[defaults objectForKey:@"userId"];
    
    AFHTTPRequestOperationManager *manager =[AFHTTPRequestOperationManager manager];
    manager.requestSerializer=[AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    // userId=%@&content=%@
    NSLog(@"%@",_myTextView.text);
    NSDictionary *parameter =@{@"userId":userId,@"content":_myTextView.text};
    [manager GET:AdviseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"提交行程 = = %@",responseObject);
        if ([responseObject[@"head"][@"msg"] isEqualToString:@"成功"]) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"提交成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            alert.tag=100;
            [alert show];
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"提交失败!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
        }
#warning 判断返回参数  1  提交成功  0 提交失败
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==100) {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

-(void)showSimpleAlert:(NSString *)str{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:str delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
