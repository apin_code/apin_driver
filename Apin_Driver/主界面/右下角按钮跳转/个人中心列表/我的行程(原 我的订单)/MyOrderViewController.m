//
//  MyOrderViewController.m
//  Apin_Driver
//
//  Created by Apin on 15/8/5.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "MyOrderViewController.h"
#import "OrderTableViewCell.h"
#import "DriverOrderInfoViewController.h"
#import "MyOrderModel.h"
#import "MJRefresh.h"
@interface MyOrderViewController ()<UITableViewDataSource,UITableViewDelegate,MJRefreshBaseViewDelegate>
{
    int _selectItem;
    UITableView * _orderTabelView;
    NSMutableArray * _dataArray;
    NSMutableArray * itemsArray;
    int _finishPageNum;
    int _unfinishPageNum;
    int _invalidPageNum;
    int pageSize;
    
    MJRefreshFooterView * _footView;
    MJRefreshHeaderView * _headView;
    
    UILabel * _messageLabel;
}
@end

@implementation MyOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadMyView];
    [self loadDataWithPage:_finishPageNum AndSelectType:0 AndRemoveArrObjc:YES];
}
-(void)loadMyView{
    _dataArray=[NSMutableArray array];
    //每一页的页号
    _finishPageNum=_unfinishPageNum=_invalidPageNum=1;
    //每一页的个数
    pageSize=10;
    
    //上下拉 刷新加载视图
    _footView=[[MJRefreshFooterView alloc]initWithScrollView:_orderTabelView];
    _headView=[[MJRefreshHeaderView alloc]initWithScrollView:_orderTabelView];
    _footView.delegate=self;
    _headView.delegate=self;
    
    
    for (int i=0; i<3; i++) {
        NSMutableArray * arr=[NSMutableArray array];
        [_dataArray addObject:arr];
    }
    itemsArray=[NSMutableArray arrayWithArray:@[@"未完成\n(0)",@"已完成\n(0)",@"已失败\n(0)"]];
    
    UIImage * image=[self OriginImage:[UIImage imageNamed:@"按钮矩形"] scaleToSize:CGSizeMake(232.0/itemsArray.count, 44*KScaleH)];
    UIImage * butImage=[image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    self.topTitleString=@"我的行程";
    for(int i=0 ; i<itemsArray.count;i++){
        UIButton * but=[[UIButton alloc]initWithFrame:CGRectMake(3*KScaleW+235*KScaleW/itemsArray.count*i, 45*KScaleH, 232*KScaleW/itemsArray.count, 44*KScaleH)];
        [but setTitle:itemsArray[i] forState:UIControlStateNormal];
        but.titleLabel.numberOfLines=0;
        but.titleLabel.textAlignment=NSTextAlignmentCenter;
        but.titleLabel.font=[UIFont fontWithName:nil size:12*KScaleH];
        [but setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [but setTitleColor:BlueColor forState:UIControlStateSelected];
        [self.view addSubview:but];
        but.tag=i+1;
        [but addTarget:self action:@selector(itemButAction:) forControlEvents:UIControlEventTouchUpInside];
        [but setBackgroundImage:butImage forState:UIControlStateNormal];
        [but setBackgroundImage:butImage forState:UIControlStateHighlighted];
//        but.backgroundColor=[UIColor magentaColor];
        if (i==0) {
            _selectItem=but.tag;
            but.selected=YES;
        }
        if (i==2) {
            but.tag=i+2;
        }
    }
    
    
    _messageLabel=[[UILabel alloc]initWithFrame:CGRectMake(8*KScaleW, 90*KScaleH, 235*KScaleW, 30*KScaleH)];
//    _messageLabel.backgroundColor=[UIColor whiteColor];
    _messageLabel.text=@"木有数据,赶快发起拼车吧!";
    _messageLabel.textColor=[UIColor whiteColor];
    [self.view addSubview:_messageLabel];
    _messageLabel.font=[UIFont fontWithName:nil size:12*KScaleH];
    _messageLabel.hidden=YES;
    _orderTabelView=[[UITableView alloc]initWithFrame:CGRectMake(2.5*KScaleW, 90*KScaleH, 235*KScaleW, 174*KScaleH) style:UITableViewStylePlain];
    [_orderTabelView registerNib:[UINib nibWithNibName:@"OrderTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    _orderTabelView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:_orderTabelView];
    _orderTabelView.separatorStyle=UITableViewCellSeparatorStyleNone;
    _orderTabelView.delegate=self;
    _orderTabelView.dataSource=self;
    _orderTabelView.rowHeight=_orderTabelView.frame.size.height/4;
    NSLog(@"%.2f",_orderTabelView.frame.size.height);
}
-(void)loadDataWithPage:(int)page AndSelectType:(int)type AndRemoveArrObjc:(BOOL)removeStyle{
    NSMutableArray * arr=nil;
    if (type==3) {
        arr=_dataArray[2];
    }else{
        arr=_dataArray[type];
    }
    
    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
    
    AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
//    manager.requestSerializer=[AFHTTPRequestSerializer serializer];
//    manager.responseSerializer=[AFJSONResponseSerializer serializer];
    NSDictionary * dic=@{UserIdKey:[defaults objectForKey:@"userId"],RoutePageNum:[NSString stringWithFormat:@"%d",page],RoutePageSize:[NSString stringWithFormat:@"%d",pageSize],RouteType:[NSString stringWithFormat:@"%d",type]};
    
    [manager POST:LOOKRouteURL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@  %@",[self class],responseObject);
        
        if ([responseObject[@"head"][@"msg"] isEqualToString:@"成功"]) {
            if (removeStyle) {
                [arr removeAllObjects];
            }
            
            
            for (NSDictionary * dic in responseObject[@"body"][@"routeList"]) {
                MyOrderModel * model=[[MyOrderModel alloc]init];
                model.setOutTime=dic[@"setOutTime"];
                model.routeStart=dic[@"routeStart"];
                model.routeEnd=dic[@"routeEnd"];
                model.passengerNum=dic[@"passengerNum"];
                model.maxPassengerNum=dic[@"maxPassengerNum"];
                model.routeId=dic[@"routeId"];
                model.orderStyle=[NSString stringWithFormat:@"%d",type];
                model.invalidReason=dic[@"invalidReason"];
                model.status=dic[@"status"];
                [arr addObject:model];
            }
            [itemsArray removeAllObjects];
            
            NSString * str1=[NSString stringWithFormat:@"未完成\n(%@)",responseObject[@"body"][@"tatalNum"][@"unfinished"]];
            if (responseObject[@"body"][@"tatalNum"][@"unfinished"]==nil) {
                str1=@"未完成\n(0)";
            }
            [itemsArray addObject:str1];
            
            NSString * str2=[NSString stringWithFormat:@"已完成\n(%@)",responseObject[@"body"][@"tatalNum"][@"finishedNum"]];
            if (responseObject[@"body"][@"tatalNum"][@"finishedNum"]==nil) {
                str2=@"已完成\n(0)";
            }
            [itemsArray addObject:str2];
            
            NSString * str3=[NSString stringWithFormat:@"已失效\n(%@)",responseObject[@"body"][@"tatalNum"][@"invalid"]];
            if (responseObject[@"body"][@"tatalNum"][@"invalid"]==nil) {
                str3=@"已失效\n(0)";
            }
            [itemsArray addObject:str3];
            
            [self butLoadData];
            [_orderTabelView reloadData];
        }else{
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@  %@",[self class],error);
        
    }];
    
}
-(void)butLoadData{
    NSLog(@"itemsArray==%@",itemsArray);
    [self loadButtonTitleWithTag:1];
    [self loadButtonTitleWithTag:2];
    [self loadButtonTitleWithTag:4];
}
-(void)loadButtonTitleWithTag:(int)tag{
    
    UIButton * but=(UIButton *)[self.view viewWithTag:tag];
    if (but==nil) {
        return;
    }
    if (tag==4) {
        [but setTitle:itemsArray[2] forState:UIControlStateNormal];
    }else
    [but setTitle:itemsArray[tag-1] forState:UIControlStateNormal];
}

-(void)itemButAction:(UIButton *)but{
    for (int i=0; i<itemsArray.count+1; i++) {
        UIButton * button=(UIButton *)[self.view viewWithTag:i+1];
        if (button==nil) {
            continue;
        }
        button.selected=NO;
    }
    but.selected=YES;
    _selectItem=but.tag;
    _finishPageNum=_unfinishPageNum=_invalidPageNum=1;
    [self loadDataWithPage:1 AndSelectType:but.tag-1 AndRemoveArrObjc:YES];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray * arr=nil;
    if (_selectItem==4) {
        arr=_dataArray[2];
//        return [_dataArray[2] count];
    }else{
        arr=_dataArray[_selectItem-1];
        
    }
    if (arr.count==0) {
        _messageLabel.hidden=NO;
    }else{
        _messageLabel.hidden=YES;
    }
    return arr.count;
//    return _dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    NSLog(@"_dataArray==%@",_dataArray);
    int index=0;
    if (_selectItem>_dataArray.count) {
        index=2;
    }else{
        index=_selectItem-1;
    }
//    cell.selectItem=_selectItem;
    
    cell.model=_dataArray[index][indexPath.row];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DriverOrderInfoViewController * orderInfoCtr=[[DriverOrderInfoViewController alloc]init];
    int index=0;
    if (_selectItem>_dataArray.count) {
        index=3;
    }else{
        index=_selectItem-1;
    }
    if (index==3) {
        return;
    }
    if (index==2) {
        return;
    }
    MyOrderModel * model=_dataArray[index][indexPath.row];
    orderInfoCtr.routeId=model.routeId;
    [self.navigationController pushViewController:orderInfoCtr animated:NO];
}
- (void)refreshViewBeginRefreshing:(MJRefreshBaseView *)refreshView{
    BOOL itemStyle=YES;
    int page=1;
    if (refreshView==_footView){
        itemStyle=NO;
        switch (_selectItem) {
            case 1:
                page=++_finishPageNum;
                break;
                
            case 2:
                page=++_unfinishPageNum;
                break;
                
            case 4:
                page=++_invalidPageNum;
                break;
                
            default:
                break;
        }
    }else{
        _finishPageNum=_unfinishPageNum=_invalidPageNum=1;
    }
    [self loadDataWithPage:page AndSelectType:_selectItem-1 AndRemoveArrObjc:itemStyle];
}

//缩放 对应比例
-(UIImage*) OriginImage:(UIImage *)image scaleToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);  //size 为CGSize类型，即你所需要的图片尺寸
    
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return scaledImage;   //返回的就是已经改变的图片
}

-(void)dealloc{
    [_headView free];
    [_footView free];
}
@end
