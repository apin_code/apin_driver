//
//  OrderTableViewCell.h
//  Apin_Driver
//
//  Created by Apin on 15/8/5.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyOrderModel.h"
@interface OrderTableViewCell : UITableViewCell
@property(nonatomic,strong)MyOrderModel * model;
//@property(nonatomic,assign)int selectItem;
@end
