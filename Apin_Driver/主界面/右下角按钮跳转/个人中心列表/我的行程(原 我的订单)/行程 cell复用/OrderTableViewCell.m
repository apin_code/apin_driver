//
//  OrderTableViewCell.m
//  Apin_Driver
//
//  Created by Apin on 15/8/5.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "OrderTableViewCell.h"
@interface OrderTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *cellBgImageView;
@property (weak, nonatomic) IBOutlet UILabel *startPlaceLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UILabel *arrivePlaceLabel;
@property (weak, nonatomic) IBOutlet UILabel *finishLabel;

@end
@implementation OrderTableViewCell

- (void)awakeFromNib {
//    UIImage * image=[self OriginImage:[UIImage imageNamed:@"我的订单cell"] scaleToSize:_cellBgImageView.bounds.size];
//    _cellBgImageView.image=[image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    UITapGestureRecognizer * tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(exchangeCarAction:)];
    [_finishLabel addGestureRecognizer:tap];
    
    // Initialization code
}



-(void)exchangeCarAction:(UITapGestureRecognizer *)tap{
    UILabel * label=(UILabel *)tap.view;
    if ([label.text isEqualToString:@"申请换车"]) {
        
        
    }
    return;
    
}
-(void)setModel:(MyOrderModel *)model{
    _model=model;
    
    _timeLabel.text=[self clipString:model.setOutTime];
    _countLabel.text=[NSString stringWithFormat:@"%@/%@",model.passengerNum,model.maxPassengerNum];
//    _finishLabel.userInteractionEnabled=NO;
    _startPlaceLabel.text=_model.routeStart;
    _arrivePlaceLabel.text=model.routeEnd;
    if ([model.orderStyle isEqualToString:@"0"]) {
        _finishLabel.text=@"未完成";
        if (model.invalidReason.length) {
            _finishLabel.text=@"换车中";
            
        }else{
            _finishLabel.text=@"申请换车";
//            _finishLabel.userInteractionEnabled=YES;
            
        }
        [self setLabelColor:[UIColor whiteColor]];
    }else if ([model.orderStyle isEqualToString:@"1"]){
        _finishLabel.text=@"已完成";
        
        [self setLabelColor:UnfinishedColor];
    }else if ([model.orderStyle isEqualToString:@"3"]){
        _finishLabel.text=@"已失效";
        _finishLabel.text=model.invalidReason;
        [self setLabelColor:InvalidColor];
        
    }
}
-(NSString *)clipString:(NSString *)str{
//    NSMutableString * muStr=[NSMutableString stringWithString:str];
    NSArray * strArr1=[str componentsSeparatedByString:@" "];
    
    NSArray * strYearArr=[strArr1[0] componentsSeparatedByString:@"-"];
    NSArray * strDateArr=[strArr1[1] componentsSeparatedByString:@":"];
    if (strYearArr.count<2||strDateArr.count<2) {
        return @"";
    }
    return [NSString stringWithFormat:@"%@-%@ %@:%@",strYearArr[1],strYearArr[2],strDateArr[0],strDateArr[1]];
}
-(void)setLabelColor:(UIColor *)textColor{
    _timeLabel.textColor=textColor;
    _startPlaceLabel.textColor=textColor;
    _countLabel.textColor=textColor;
    _arrivePlaceLabel.textColor=textColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
