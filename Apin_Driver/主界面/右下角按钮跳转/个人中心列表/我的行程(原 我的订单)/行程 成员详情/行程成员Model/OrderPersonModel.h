//
//  OrderPersonModel.h
//  Apin_Driver
//
//  Created by Apin on 15/8/8.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderPersonModel : NSObject
@property(nonatomic,copy)NSString * filePath;
@property(nonatomic,copy)NSString * passenger;
@property(nonatomic,copy)NSString * setOutTime;
@property(nonatomic,copy)NSString * dest;
@property(nonatomic,copy)NSString * start;
@property(nonatomic,copy)NSString * phone;
@end
