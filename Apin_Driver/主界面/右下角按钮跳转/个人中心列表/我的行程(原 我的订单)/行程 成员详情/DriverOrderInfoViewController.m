//
//  DriverOrderInfoViewController.m
//  Apin_Driver
//
//  Created by Apin on 15/8/5.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "DriverOrderInfoViewController.h"
#import "MyOrderInfoView.h"
#import "OrderPersonModel.h"
@interface DriverOrderInfoViewController ()
{
    MyOrderInfoView*_orderInfoView;
    UILabel * _pageLabel;
    int page;
//    int personCount;
    UILabel * _messageLabel;
    
    NSMutableArray * _dataArray;
}
@end

@implementation DriverOrderInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadMyView];
    [self loadData];
    // Do any additional setup after loading the view.
}
-(void)loadData{
    AFHTTPRequestOperationManager * manger=[AFHTTPRequestOperationManager manager];
    [manger POST:[NSString stringWithFormat:LOOKRouteInfoURL,_routeId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"head"][@"msg"] isEqualToString:@"成功"]) {
            [_dataArray removeAllObjects];
            NSLog(@"行程详情 %@",responseObject);
            for (NSDictionary * dic in responseObject[@"body"]) {
                
                OrderPersonModel * model=[[OrderPersonModel alloc]init];
                model.filePath=dic[@"filePath"];
                model.passenger=dic[@"passenger"];
                model.setOutTime=dic[@"setOutTime"];
                model.dest=dic[@"routeEnd"];
                model.start=dic[@"routeStart"];
                model.phone=dic[@"phone"];
                [_dataArray addObject:model];
            }
            if (_dataArray.count==0) {
                _messageLabel.hidden=NO;
                _orderInfoView.hidden=YES;
            }else{
                _orderInfoView.model=_dataArray[0];
                _messageLabel.hidden=YES;
                _orderInfoView.hidden=NO;
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}
-(void)loadMyView{
    _dataArray=[NSMutableArray array];
    self.topTitleString=@"行程详情";
    self.bgImage=[UIImage imageNamed:@"中间栏中大"];
    page=1;
    _orderInfoView=[[MyOrderInfoView alloc]initWithFrame:CGRectMake(2.5*KScaleW, 46*KScaleH, 235*KScaleW, 218*KScaleH)];
    _messageLabel=[[UILabel alloc]initWithFrame:CGRectMake(2.5*KScaleW, 46*KScaleH, 235*KScaleW, 218*KScaleH)];
    _messageLabel.text=@"没有数据";
    _messageLabel.textColor=[UIColor whiteColor];
    _messageLabel.hidden=YES;
    [self.view addSubview:_orderInfoView];
    [self bottomViewAddSubView];
}
-(void)bottomViewAddSubView{
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(63*KScaleW, 9*KScaleH, 15*KScaleW, 26*KScaleH)];
    [leftButton addTarget:self action:@selector(leftButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setBackgroundImage:[UIImage imageNamed:@"左"] forState:UIControlStateNormal];
    [self.bottomView addSubview:leftButton];
    
    _pageLabel=[[UILabel alloc]initWithFrame:CGRectMake(235.0/2*KScaleW-50*KScaleW, 15*KScaleH, 100*KScaleW, 20*KScaleH)];
    _pageLabel.text=@"1/1";
    _pageLabel.textColor=[UIColor whiteColor];
    _pageLabel.textAlignment=NSTextAlignmentCenter;
    [self.bottomView addSubview:_pageLabel];
    
    UIButton * rightButton=[[UIButton alloc]initWithFrame:CGRectMake(160*KScaleW, 9*KScaleH, 15*KScaleW, 26*KScaleH)];
    [rightButton addTarget:self action:@selector(rightButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [rightButton setBackgroundImage:[UIImage imageNamed:@"右"] forState:UIControlStateNormal];
    [self.bottomView addSubview:rightButton];
}
#warning 需要重写
-(void)rightButtonAction{
    if (page<_dataArray.count) {
        page++;
        _pageLabel.text=[NSString stringWithFormat:@"%d/%ld",page,_dataArray.count];
        
        _orderInfoView.model=_dataArray[page-1];
    }
    
}
-(void)leftButtonAction{

    if (page>1) {
        page--;
        _pageLabel.text=[NSString stringWithFormat:@"%d/%ld",page,_dataArray.count];
        
        _orderInfoView.model=_dataArray[page-1];
//        _orderInfoView
    }
}
//-(void)reloadViewWith:(BOOL)hidden{
//    _messageLabel.hidden=!hidden;
//    _orderInfoView.hidden=hidden;
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
