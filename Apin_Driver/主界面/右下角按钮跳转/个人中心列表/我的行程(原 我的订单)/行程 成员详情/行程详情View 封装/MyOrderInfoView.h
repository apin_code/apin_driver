//
//  MyOrderInfoView.h
//  Apin_Driver
//
//  Created by Apin on 15/8/5.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderPersonModel.h"
@interface MyOrderInfoView : UIView
@property(nonatomic,copy)OrderPersonModel * model;
@end
