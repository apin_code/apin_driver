//
//  MyOrderInfoView.m
//  Apin_Driver
//
//  Created by Apin on 15/8/5.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "MyOrderInfoView.h"
@interface MyOrderInfoView ()
{
    UILabel * _orderNumLabel;
    UIButton * _massageButton;
    UIButton * _exchangCarButton;
    UIImageView * _headImageView;
    UILabel * _nameLabel;
    UIButton * _phoneButton;
    UILabel * _dataLabel;
    UILabel * _startPlaceLabel;
    UILabel * _destinationPlaceLabel;
}
@end
@implementation MyOrderInfoView

-(void)setModel:(OrderPersonModel *)model{
    _model=model;
    [_headImageView setImageWithURL:[NSURL URLWithString:model.filePath] placeholderImage:[UIImage imageNamed:@"头像"]];
    
    _nameLabel.text=model.passenger;
    if ([_nameLabel.text isEqualToString:@"null"]) {
        _nameLabel.text=@"--";
    }
    _dataLabel.text=model.setOutTime;
    _startPlaceLabel.text=[NSString stringWithFormat:@"起:%@",model.start];
    _destinationPlaceLabel.text=[NSString stringWithFormat:@"终:%@",model.dest];
    _dataLabel.text=model.setOutTime;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        self.backgroundColor=[UIColor redColor];
        [self loadView];
    }
    return self;
}
-(void)loadView{
    CGFloat butTextSize=11;
    //订单号
//    _orderNumLabel=[[UILabel alloc]initWithFrame:CGRectMake(18*KScaleW, 9*KScreenH, 128*KScaleW, 17*KScaleH)];
////    [self addSubview:_orderNumLabel];
//    [self setLabel:_orderNumLabel];
//    _orderNumLabel.font=[UIFont fontWithName:nil size:12];
//    _orderNumLabel.text=@"订单编号2015070701";
//    
//    //短信??
//    _massageButton=[[UIButton alloc]initWithFrame:CGRectMake(138*KScaleW,3*KScaleH, 24*KScaleW, 24*KScaleH)];
//    [self addSubview:_massageButton];
//    [_massageButton setBackgroundImage:[UIImage imageNamed:@"短信"] forState:UIControlStateNormal];
//    _massageButton.tag=1;
//    [_massageButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
////    [_massageButton setTitle:@"短" forState:UIControlStateNormal];
////    _massageButton.titleLabel.font=[UIFont fontWithName:nil size:butTextSize];
//    
//    //申请换车
//    _exchangCarButton=[[UIButton alloc]initWithFrame:CGRectMake(182*KScaleW, 7*KScaleH, 46*KScaleW, 18*KScaleH)];
//    _exchangCarButton.tag=2;
//    [self addSubview:_exchangCarButton];
//    [_exchangCarButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
//    [_exchangCarButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
//    [_exchangCarButton setTitle:@"申请换车" forState:UIControlStateNormal];
//    [_exchangCarButton setTitleColor:[UIColor colorWithRed:0 green:198.0/255.0 blue:1 alpha:1] forState:UIControlStateNormal];
//    _exchangCarButton.titleLabel.font=[UIFont fontWithName:nil size:11];
//    
    //电话
    _phoneButton=[[UIButton alloc]initWithFrame:CGRectMake(106*KScaleW, 98*KScaleH, 24*KScaleW, 24*KScaleH)];
    _phoneButton.tag=3;
    [self addSubview:_phoneButton];
    [_phoneButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [_phoneButton setBackgroundImage:[UIImage imageNamed:@"电话"] forState:UIControlStateNormal];
//    [_phoneButton setTitle:@"电" forState:UIControlStateNormal];
    _phoneButton.titleLabel.font=[UIFont fontWithName:nil size:butTextSize];
    
    //头像
    _headImageView=[[UIImageView alloc]initWithFrame:CGRectMake(94*KScaleW, 29*KScaleH, 48*KScaleW, 48*KScaleH)];
    _headImageView.image=[UIImage imageNamed:@"头像"];
//    _headImageView.backgroundColor=[UIColor magentaColor];
    _headImageView.layer.cornerRadius=24*KScaleH;
    _headImageView.layer.masksToBounds=YES;
    [self addSubview:_headImageView];
    
    //名字
    _nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(52*KScaleW, 82*KScaleH, 139*KScaleW, 20*KScaleH)];
//    [self addSubview:_nameLabel];
    [self setLabel:_nameLabel];
    _nameLabel.textAlignment=NSTextAlignmentCenter;
    _nameLabel.font=[UIFont fontWithName:nil size:12*KScaleH];
    _nameLabel.text=@"- -";
    
    //时间
    _dataLabel=[[UILabel alloc]initWithFrame:CGRectMake(40*KScaleW, 138*KScaleH, 176*KScaleW, 18*KScaleH)];
//    [self addSubview:_dataLabel];
    [self setLabel:_dataLabel];
//    _dataLabel.backgroundColor=[UIColor magentaColor];
    _dataLabel.textAlignment=NSTextAlignmentCenter;
    _dataLabel.font=[UIFont fontWithName:nil size:16*KScaleH];
    _dataLabel.text=@"--年--月--日 --:--";
    //起点
    _startPlaceLabel=[[UILabel alloc]initWithFrame:CGRectMake(63*KScaleW, 160*KScaleH, 165*KScaleW, 18*KScaleH)];
//    [self addSubview:_startPlaceLabel];
    [self setLabel:_startPlaceLabel];
    _startPlaceLabel.font=[UIFont fontWithName:nil size:15*KScaleH];
    _startPlaceLabel.text=@"起:--";
    //终点
    _destinationPlaceLabel=[[UILabel alloc]initWithFrame:CGRectMake(63*KScaleW, 186*KScaleH, 165*KScaleW, 15*KScaleH)];
//    [self addSubview:_destinationPlaceLabel];
    [self setLabel:_destinationPlaceLabel];
    _destinationPlaceLabel.font=[UIFont fontWithName:nil size:15*KScaleH];
    _destinationPlaceLabel.text=@"终:--";
}
-(void)buttonAction:(UIButton *)but{
    if (_model.phone.length>9) {
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",_model.phone]]];
    }
    
}
-(void)setLabel:(UILabel *)label{
    label.font=[UIFont fontWithName:nil size:11*KScaleH];
    label.textColor=[UIColor whiteColor];
    label.text=@"内容需更改";
    [self addSubview:label];
}
@end
