//
//  MyOrderModel.h
//  Apin_Driver
//
//  Created by Apin on 15/8/8.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyOrderModel : NSObject
@property(nonatomic,copy)NSString * setOutTime;//出发时间
//@property(nonatomic,copy)NSString * route;//线路
@property(nonatomic,copy)NSString * passengerNum;//乘客数量
@property(nonatomic,copy)NSString * maxPassengerNum;//最大乘客数量
@property(nonatomic,copy)NSString * routeId;//路线Id

@property(nonatomic,copy)NSString * routeStart;//起点
@property(nonatomic,copy)NSString * routeEnd;//终点

@property(nonatomic,copy)NSString * orderStyle;//路线状态

@property(nonatomic,copy)NSString * invalidReason;//原因

@property(nonatomic,copy)NSString * status;
@end
