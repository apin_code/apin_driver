//
//  PersonCenterInfoViewController.m
//  Apin_Driver
//
//  Created by Apin on 15/8/6.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "PersonCenterInfoViewController.h"
#import "RegistTwoViewController.h"
#import "LoginViewController.h"
@interface PersonCenterInfoViewController ()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    UITableView*_tabelView;
    NSMutableArray * _titleArray;
    //    NSMutableArray  *_titleDict;
    UIImageView * _headImageView;
}
@end

@implementation PersonCenterInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadMyView];
    
    [self loadData];
    
}
-(void)loadData{
    AFHTTPRequestOperationManager *manager= [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //    NSString *url =[NSString stringWithFormat:SJPersonCenter,@"13"];
    NSDictionary * dic=@{@"userId":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    
    
    [manager GET:SJPersonCenter parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"responseObject 个人中心 = =%@",responseObject);
        if ([responseObject[@"head"][@"msg"] isEqualToString:@"成功"]) {
            self.centerBgImageView.hidden=YES;
            [_titleArray removeAllObjects];
            [_headImageView setImageWithURL:[NSURL URLWithString:responseObject[@"body"][0][@"filePath"]]];
            NSString * str1 = [NSString stringWithFormat:@"    姓  名      %@",responseObject[@"body"][0][@"user"]];
            NSString * str2 = [NSString stringWithFormat:@"    手机号      %@",responseObject[@"body"][0][@"phone"]];
            NSString * str3 = [NSString stringWithFormat:@"    车牌号      %@",responseObject[@"body"][0][@"plate"]];
            [_titleArray addObject:str1];
            [_titleArray addObject:str2];
            [_titleArray addObject:str3];
            
            [_tabelView reloadData];
        }else{
            // 请求失败
            [self creatAlertViewWithMessage:responseObject[@"head"][@"msg"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        [self creatAlertViewWithMessage:@"网络错误"];
        
        
        if (error.code==3840||error.code==-1011) {
            [self creatAlertViewWithMessage:@"请求失败"];
        }else if (error.userInfo[@"NSDebugDescription"]&&[[NSString stringWithFormat:@"%@",error.userInfo[@"NSDebugDescription"]] length]<30) {
            
            
            [self creatAlertViewWithMessage:error.userInfo[@"NSDebugDescription"]];
            
            
            
        }else if(error.userInfo[@"NSLocalizedDescription"]&&[[NSString stringWithFormat:@"%@",error.userInfo[@"NSLocalizedDescription"]] length]>30){
            
            
            [self creatAlertViewWithMessage:error.userInfo[@"NSLocalizedDescription"]];
            
            
            
        }else{
            [self creatAlertViewWithMessage:@"请求数据失败,请稍后重试"];
        }
        
        NSLog(@"个人中心数据请求失败 = = %@",error.userInfo);
    }];
    
    
}
-(void)creatAlertViewWithMessage:(NSString *)message{
    UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:message delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView show];
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.view.frame=CGRectMake(KBgSmallViewX, KBgSmallViewY, KBgSmallViewW, KBgSmallViewH+12);
    
}
-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.view.frame=CGRectMake(KBgSmallViewX, KBgSmallViewY, KBgSmallViewW, KBgSmallViewH);
}
-(void)loadMyView{
    //    self.topTitleString=@"个人信息";
    _titleArray=[NSMutableArray array];
    //    _titleArray=[[NSMutableArray alloc]initWithObjects:@"   车主姓名   王小贱",@"   手机号      18937181326",@"   车牌号     浙XSCSD", nil];
    _headImageView=[[UIImageView alloc]initWithFrame:CGRectMake(100*KScaleW, 3*KScaleH, 40*KScaleW, 40*KScaleH)];
    //    [imageView setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:@"头像"]];
    [_headImageView setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults]objectForKey:@"headpic"]] placeholderImage:[UIImage imageNamed:@"头像"]];
    _headImageView.layer.cornerRadius=20*KScaleW;
    _headImageView.layer.masksToBounds=YES;
    [self.view addSubview:_headImageView];
    _tabelView=[[UITableView alloc]initWithFrame:CGRectMake(2.5*KScaleW, 44*KScaleH, 235*KScaleW, 218*KScaleH) style:UITableViewStylePlain];
    _tabelView.delegate=self;
    _tabelView.dataSource=self;
    [self.view addSubview:_tabelView];
    _tabelView.rowHeight=_tabelView.frame.size.height/3;
    _tabelView.separatorStyle=UITableViewCellSeparatorStyleNone;
    _tabelView.backgroundColor=[UIColor clearColor];
    _tabelView.scrollEnabled=NO;
    
    self.bottomImageView.image=[UIImage imageNamed:@"底边"];
    
    UIButton * exitButton=[[UIButton alloc]initWithFrame:CGRectMake(120*KScaleW-40*KScaleW, 280*KScaleH, 80*KScaleW, 40*KScaleH)];
    [exitButton setBackgroundImage:[UIImage imageNamed:@"按钮"] forState:UIControlStateNormal];
    exitButton.titleLabel.font=[UIFont fontWithName:nil size:12*KScaleH];
    [exitButton addTarget:self action:@selector(exitButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [exitButton setTitle:@"退出账户" forState:UIControlStateNormal];
    exitButton.titleLabel.textColor=[UIColor whiteColor];
    //    exitButton.backgroundColor=[UIColor magentaColor];
    //    self.bottomView.backgroundColor=[UIColor whiteColor];
    
    [self.view addSubview:exitButton];
}
-(void)exitButtonAction{
    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"退出账户?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.tag=101;
    [alertView show];
    
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==101&&buttonIndex==1) {
        NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
        AFHTTPRequestOperationManager * manager=(AFHTTPRequestOperationManager *)[AFHTTPRequestOperationManager manager];
        if (manager==nil) {
            return;
        }
        [manager POST:LOGINOUTURL parameters:@{@"phone":[defaults objectForKey:@"phone"]} success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([[NSString stringWithFormat:@"%@",responseObject[@"head"][@"code"] ] isEqualToString:@"00000000"]) {
                AppDelegate * delegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
                delegate.userID=nil;
                ApinDriverMainViewController *mainCtr=(ApinDriverMainViewController *)delegate.window.rootViewController;
                mainCtr.loginOK=NO;
                //        [defaults setObject:nil forKey:@"phone"];
                [defaults setObject:nil forKey:@"password"];
                [defaults setObject:nil forKey:@"userId"];
                [defaults setObject:nil forKey:@"headpic"];
                [ApinXGPush stopPush];
                self.navigationController.viewControllers=@[[[LoginViewController alloc] init]];
                
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
        
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _titleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.textLabel.font=[UIFont fontWithName:nil size:15*KScaleH];
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
#warning 代码残缺
    cell.textLabel.text=_titleArray[indexPath.row];
    //    cell.textLabel.text=_titleArray[indexPath.row];
    //    if (_IsOK==NO) {
    //        UIButton * but=[[UIButton alloc]initWithFrame:CGRectMake(84*KScaleW, cell.contentView.frame.size.height/2-14*KScaleH, 35*KScaleW, 14*KScaleH)];
    //        [but setBackgroundImage:[UIImage imageNamed:@"登录输入框"] forState:UIControlStateNormal];
    //        [but setTitle:@"申请认证" forState:UIControlStateNormal];
    //        [but addTarget:self action:@selector(askForButtonAction) forControlEvents:UIControlEventTouchUpInside];
    //        [cell.contentView addSubview:but];
    //    }
    return cell;
}
-(void)askForButtonAction{
    RegistTwoViewController * twoCtr=[[RegistTwoViewController alloc]init];
    NSMutableDictionary * dic=[NSMutableDictionary dictionary];
    twoCtr.imageDic=dic;
    [self.navigationController pushViewController:twoCtr animated:NO];
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    UIImageView * imageView=[[UIImageView alloc]initWithFrame:CGRectMake(cell.contentView.frame.origin.x+1, cell.contentView.frame.origin.y+1, cell.contentView.frame.size.width-1, cell.contentView.frame.size.height-1)];
    imageView.image=[UIImage imageNamed:@"中间栏中大"];
    [cell.contentView addSubview:imageView];
    cell.backgroundColor=[UIColor clearColor];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
