//
//  PersonCenterInfoViewController.h
//  Apin_Driver
//
//  Created by Apin on 15/8/6.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "DriverFatherViewController.h"

@interface PersonCenterInfoViewController : DriverFatherViewController

@property(nonatomic,copy)void(^exitBlock)(void);
@property BOOL IsOK;
@end
