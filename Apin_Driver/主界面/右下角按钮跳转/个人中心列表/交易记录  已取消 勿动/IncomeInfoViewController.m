//
//  IncomeInfoViewController.m
//  Apin_Driver
//
//  Created by Apin on 15/8/5.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "IncomeInfoViewController.h"
#import "IncomeInfoTableViewCell.h"

@interface IncomeInfoViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView * _incomeInfoTabelView;
    NSMutableArray * _dataArray;
}
@end

@implementation IncomeInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.topTitleString=@"收入明细";
    [self loadMyView];
    
    [self loadData];
    // Do any additional setup after loading the view.
}
-(void)loadData{
    
#warning 需要写接口
    return;
//    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
//    AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
//    [manager POST:[NSString stringWithFormat:IncomeURL,[defaults objectForKey:@"userId"]] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"%@  %@",[self class],responseObject);
//        
//        if ([responseObject[@"head"][@"msg"] isEqualToString:@"成功"]) {
//            [_dataArray removeAllObjects];
//            for (NSDictionary * dic in responseObject[@"body"][@"incomeDetail"]) {
//                IncomeModel * model=[[IncomeModel alloc]init];
//                model.remark=responseObject[@"body"][@"desc"][@"remark"];
//                model.settlementRule=responseObject[@"body"][@"desc"][@"settlementRule"];
//                model.period=dic[@"period"];
//                model.routeNum=dic[@"routeNum"];
//                model.totalMileage=dic[@"totalMileage"];
//                model.unFullNum=dic[@"unFullNum"];
//                model.evaluation=dic[@"evaluation"];
//                model.income=dic[@"income"];
//                [_dataArray addObject:model];
//            }
//        }
//        
//        
//        [_incomeInfoTabelView reloadData];
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"%@  %@",[self class],error);
//        
//    }];
}
-(void)loadMyView{
    _dataArray=[NSMutableArray array];
    _incomeInfoTabelView=[[UITableView alloc]initWithFrame:CGRectMake(2.5*KScaleW, 50*KScaleH, 235*KScaleW, 208*KScaleH) style:UITableViewStylePlain];
    [self.view addSubview:_incomeInfoTabelView];
    _incomeInfoTabelView.rowHeight=69*KScaleH;
    _incomeInfoTabelView.delegate=self;
    _incomeInfoTabelView.dataSource=self;
    [_incomeInfoTabelView registerNib:[UINib nibWithNibName:@"IncomeInfoTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    _incomeInfoTabelView.backgroundColor=[UIColor clearColor];
    _incomeInfoTabelView.separatorStyle=UITableViewCellSeparatorStyleNone;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
//    return _dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    IncomeInfoTableViewCell * cell=[_incomeInfoTabelView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
