//
//  IncomeModel.h
//  Apin_Driver
//
//  Created by Apin on 15/8/8.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FeesModel : NSObject
@property(nonatomic,copy)NSString *remark;// 注:
@property(nonatomic,copy)NSString *settlementRule; // 结算规则
@property(nonatomic,copy)NSString *periodStart; // 开始日期
@property(nonatomic,copy)NSString *periodEnd; // 结束日期
@property(nonatomic,copy)NSString *routeNum; // 行程数
@property(nonatomic,copy)NSString *totalMileage; // 单位(公里)
@property(nonatomic,copy)NSString *unFullNum; // 未满座次数
@property(nonatomic,copy)NSString *evaluation; // 评价
@property(nonatomic,copy)NSString *income; // 收入
@end
