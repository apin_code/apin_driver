//
//  DriverFeesView.h
//  Apin_Driver
//
//  Created by Apin on 15/8/6.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeesModel.h"

@interface DriverFeesView : UIView
@property(nonatomic,strong)FeesModel * model;
@end
