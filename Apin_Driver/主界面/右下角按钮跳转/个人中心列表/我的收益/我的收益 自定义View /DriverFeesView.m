//
//  DriverFeesView.m
//  Apin_Driver
//
//  Created by Apin on 15/8/6.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "DriverFeesView.h"
@interface DriverFeesView()
{
    NSArray * strArr;
}
@end
@implementation DriverFeesView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadSubView:self.bounds];
    }
    return self;
}
-(void)loadSubView:(CGRect)rect{
    UIImageView * bgImageView=[[UIImageView alloc]initWithFrame:rect];
    bgImageView.image=[UIImage imageNamed:@"中间栏中大"];
    [self addSubview:bgImageView];
    strArr=@[@"订单里程数:",@"订单总数:",@"未满座位次数:",@"乘客评价:",@"结算费用:",@"注:结算日为每周五12:30"];
    for (int i=0; i<4; i++) {
        UILabel * label=[[UILabel alloc]initWithFrame:CGRectMake(62*KScaleW, 18*KScaleH+22*KScaleH*i, 172*KScaleW, 18*KScaleH)];
        
        label.text=strArr[i];
        
        label.tag=i+1;
        label.font=[UIFont fontWithName:nil size:11*KScaleH];
        if (i==5) {
            label.textColor=[UIColor colorWithRed:109/255.0 green:111/255.0 blue:114/255.0 alpha:1];
            label.frame=CGRectMake(43*KScaleW, 141*KScaleH, 172*KScaleW, 15*KScaleH);
            label.textAlignment=NSTextAlignmentRight;
        }else{
            label.textColor=[UIColor whiteColor];
        }
        [self addSubview:label];
    }
}
-(void)setModel:(FeesModel *)model{
    _model=model;
    //订单里程数
    UILabel * label1=(UILabel *)[self viewWithTag:1];
    label1.text=[NSString stringWithFormat:@"%@%@km",strArr[0],model.totalMileage];
    
    //订单总数
    UILabel * label2=(UILabel *)[self viewWithTag:2];
    label2.text=[NSString stringWithFormat:@"%@%@单",strArr[1],model.routeNum];
    
    //未满座位次数
    UILabel * label3=(UILabel *)[self viewWithTag:3];
    label3.text= [NSString stringWithFormat:@"%@%@次",strArr[2],model.unFullNum];
    
    //乘客评价
    UILabel * label4=(UILabel *)[self viewWithTag:4];
    label4.text= [NSString stringWithFormat:@"%@%@",strArr[3],model.evaluation];
    
    //费用
//    UILabel * label5=(UILabel *)[self viewWithTag:5];
//    label5.text=  [NSString stringWithFormat:@"%@%@",strArr[4],model.income];
}


@end
