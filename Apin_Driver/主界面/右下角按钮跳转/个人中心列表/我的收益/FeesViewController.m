//
//  FeesViewController.m
//  Apin_Driver
//
//  Created by Apin on 15/8/5.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "FeesViewController.h"
#import "DriverFeesView.h"
//#import "DriverFeesModel.h"
#import "FeesModel.h"
@interface FeesViewController ()
{
    int _selectItem;
    DriverFeesView*_feesview;
    FeesModel * _feesModel;
    NSMutableArray * _dataArray;
}
@end

@implementation FeesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadMyView];
    [self loadData];
    // Do any additional setup after loading the view.
}
-(void)loadData{
    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
    AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
//    NSLog(@"%@",[NSString stringWithFormat:IncomeURL,[defaults objectForKey:@"userId"]]);
    NSLog(@"%@",[defaults objectForKey:@"userId"]);
    NSDictionary * dic=@{@"userId":@"1"};
    [manager POST:IncomeURL parameters:dic
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@  %@",[self class],responseObject);
        
        if ([responseObject[@"head"][@"msg"] isEqualToString:@"成功"]) {
            [_dataArray removeAllObjects];
            
            NSLog(@"shuzu%@",responseObject[@"body"][@"list"]);
            for (NSDictionary * dic in responseObject[@"body"][@"list"]) {
                FeesModel * model=[[FeesModel alloc]init];
                model.remark=responseObject[@"body"][@"desc"][@"remark"];
                model.settlementRule=responseObject[@"body"][@"desc"][@"settlementRule"];
                NSLog(@"%@",dic);
                model.periodStart=[NSString stringWithFormat:@"%@",dic[@"periodStart"]];
                model.periodEnd=[NSString stringWithFormat:@"%@",dic[@"periodEnd"]];
                model.routeNum=[NSString stringWithFormat:@"%@",dic[@"routeNum"]];
                model.totalMileage=[NSString stringWithFormat:@"%@",dic[@"totalMileage"]];
                model.unFullNum=[NSString stringWithFormat:@"%@",dic[@"unFullNum"]];
                model.evaluation=[NSString stringWithFormat:@"%@",dic[@"evaluation"]];
                model.income=[NSString stringWithFormat:@"%@",dic[@"income"]];
                [_dataArray addObject:model];
            }
            [self reloadView];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@  %@",[self class],error);
        
    }];
}
-(void)reloadView{
    FeesModel * model;
    
    for (int i=0; i<4; i++) {
        UIButton * button=(UIButton *)[self.view viewWithTag:i+1];
        if (_dataArray.count>i) {
            model=_dataArray[i];
            [button setTitle:[NSString stringWithFormat:@"%@\n~\n%@",model.periodStart,model.periodEnd] forState:UIControlStateNormal];
        }else{
            [button setTitle:@"无" forState:UIControlStateNormal];
            
        }
    }
    if (_dataArray.count>_selectItem-1) {
        model=_dataArray[_selectItem-1];
    }else{
        model=[[FeesModel alloc]init];
        model.remark=@"注:结算日为每周五 12:30";
        model.settlementRule=@"无";
        model.periodStart=@"无";
        model.periodEnd=@"无";
        model.routeNum=@"0";
        model.totalMileage=@"0";
        model.unFullNum=@"0";
        model.evaluation=@"0";
        model.income=@"0";
    }
    _feesview.model=model;
}
-(void)loadMyView{
    _dataArray=[NSMutableArray array];
    _feesModel=[[FeesModel alloc]init];
    self.topTitleString=@"费用结算";
    NSArray * itemsArray=@[@"无",@"无",@"无",@"无"];
    //    self.topTitleString=@"我的订单";
    //    self.bgImage=[UIImage imageNamed:@"边框2"];
    UIImage * image=[self OriginImage:[UIImage imageNamed:@"按钮矩形"] scaleToSize:CGSizeMake(232.0/itemsArray.count, 44*KScaleH)];
    UIImage * butImage=[image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
//    UIImage * butImage=image;
    
    for(int i=0 ; i<itemsArray.count;i++){
        
        UIButton * but=[[UIButton alloc]initWithFrame:CGRectMake(2.5*KScaleW+235*KScaleW/4*i, 44*KScaleH, 235*KScaleW/itemsArray.count-1, 48*KScaleH)];
        
        [but setTitle:itemsArray[i] forState:UIControlStateNormal];
//        [but setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        //        but setTitleColor:<#(UIColor *)#> forState:<#(UIControlState)#>
        but.titleLabel.numberOfLines=0;
        but.titleLabel.textAlignment=NSTextAlignmentCenter;
        but.titleLabel.font=[UIFont fontWithName:nil size:11*KScaleH];
        [but setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [but setTitleColor:BlueColor forState:UIControlStateSelected];
        [self.view addSubview:but];
        but.tag=i+1;
        [but addTarget:self action:@selector(itemButAction:) forControlEvents:UIControlEventTouchUpInside];
        [but setBackgroundImage:butImage forState:UIControlStateNormal];
        [but setBackgroundImage:butImage forState:UIControlStateHighlighted];
        if (i==0) {
            _selectItem=(int)but.tag;
            but.selected=YES;
        }
    }
    _feesview=[[DriverFeesView alloc]initWithFrame:CGRectMake(2.5*KScaleW, 92*KScaleH, 235*KScaleW, 172*KScaleH)];
//    _feesview.frame=CGRectMake(2.5*KScaleW, 94*KScaleH, 235*KScaleW, 164*KScaleH);
    [self.view addSubview:_feesview];

}
-(void)itemButAction:(UIButton *)but{
    for (int i=0; i<4; i++) {
        UIButton * button=(UIButton *)[self.view viewWithTag:i+1];
        button.selected=NO;
    }
    if (but.tag-1<_dataArray.count) {
        _feesview.model=_dataArray[but.tag-1];
    }else{
        FeesModel * model=[[FeesModel alloc]init];
        model=[[FeesModel alloc]init];
        model.remark=@"注:结算日为每周五 12:30";
        model.settlementRule=@"无";
        model.periodStart=@"无";
        model.periodEnd=@"无";
        model.routeNum=@"0";
        model.totalMileage=@"0";
        model.unFullNum=@"0";
        model.evaluation=@"0";
        model.income=@"0";
        _feesview.model=model;
    }
//    _feesview.model=_dataArray[but.tag-1];
    but.selected=YES;
    _selectItem=(int)but.tag;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
}
//缩放 对应比例
-(UIImage*) OriginImage:(UIImage *)image scaleToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);  //size 为CGSize类型，即你所需要的图片尺寸
    
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return scaledImage;   //返回的就是已经改变的图片
}
@end
