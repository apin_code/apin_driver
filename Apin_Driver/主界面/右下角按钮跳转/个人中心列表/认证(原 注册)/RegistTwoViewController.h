//
//  RegistTwoViewController.h
//  Apin_Driver
//
//  Created by Apin on 15/8/1.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistTwoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *topLabel;

@property(nonatomic,strong)NSMutableDictionary * imageDic;

@property(nonatomic,copy)NSString * carCodeStr;

@end
