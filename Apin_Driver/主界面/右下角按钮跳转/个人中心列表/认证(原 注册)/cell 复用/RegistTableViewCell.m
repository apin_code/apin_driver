//
//  RegistTableViewCell.m
//  Apin_Driver
//
//  Created by Apin on 15/8/4.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "RegistTableViewCell.h"


#define Kscale 1
@implementation RegistTableViewCell

- (void)awakeFromNib {
    // Initialization code
    UIImage * bgimage=[self OriginImage:[UIImage imageNamed:@"中间栏中小"] scaleToSize:CGSizeMake(235*KScaleW, 120*KScaleH)];
//    _bgImageView=[[UIImageView alloc]initWithFrame:self.contentView.bounds];
    self.cellBgImageView.image=[bgimage stretchableImageWithLeftCapWidth:bgimage.size.width/2 topCapHeight:bgimage.size.height/2];
    
//    [self loadCellView];
}
//-(void)getCellTitleLabel:(<#object-type#> **)buffer range:(NSRange)inRange
//缩放 对应比例
-(UIImage*) OriginImage:(UIImage *)image scaleToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);  //size 为CGSize类型，即你所需要的图片尺寸
    
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return scaledImage;   //返回的就是已经改变的图片
}
-(void)loadCellView{
    UIImage * bgimage=[self OriginImage:[UIImage imageNamed:@"中间栏大"] scaleToSize:self.contentView.bounds.size];
    _bgImageView=[[UIImageView alloc]initWithFrame:self.contentView.bounds];
    _bgImageView.image=[bgimage stretchableImageWithLeftCapWidth:bgimage.size.width/2 topCapHeight:bgimage.size.height/2];
//    [self.contentView addSubview:_bgImageView];
//    
//    _titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(38*Kscale, 32*Kscale, 158*Kscale, 21*Kscale)];
//    _titleLabel.textColor=[UIColor whiteColor];
//    _titleLabel.text=@"车牌号";
//    _titleLabel.textAlignment=NSTextAlignmentCenter;
//    [self.contentView addSubview:_titleLabel];
//    
////    _cellTextField=[[UITextField alloc]initWithFrame:CGRectMake(70*Kscale, 43*Kscale-15, 146*Kscale, 30)];
////    _cellTextField.placeholder=@"请输入车牌号";
////    [self.contentView addSubview:_cellTextField];
////    _cellTextField.borderStyle=UITextBorderStyleNone;
//    
//    _photoImageView=[[UIImageView alloc]initWithFrame:CGRectMake(31*Kscale, 14*Kscale, 173*Kscale, 90*Kscale)];
//    _photoImageView.backgroundColor=[UIColor magentaColor];
//    [self.contentView addSubview:_photoImageView];
//    _photoImageView.hidden=YES;
//    
//    _changeButton=[[UIButton alloc]initWithFrame:CGRectMake(204*Kscale, 2*Kscale, 28*Kscale, 16*Kscale)];
//    [_changeButton setBackgroundImage:[UIImage imageNamed:@"更改"] forState:UIControlStateNormal];
//    _changeButton.adjustsImageWhenHighlighted=NO;
//    [self.contentView addSubview:_changeButton];
//    _changeButton.hidden=YES;
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
