//
//  RegistTableViewCell.h
//  Apin_Driver
//
//  Created by Apin on 15/8/4.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cellBgImageView;
@property (weak, nonatomic) IBOutlet UIImageView *cellImageView;
@property (weak, nonatomic) IBOutlet UILabel *cellTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *cellChangeButton;
@property (strong, nonatomic)UILabel     *titleLabel;
@property (strong, nonatomic)UIImageView *bgImageView;
@property (strong, nonatomic)UIImageView *photoImageView;
@property (strong, nonatomic)UIButton    *changeButton;
@property (strong, nonatomic)UITextField *cellTextField;



@end
