//
//  RegistTextTableViewCell.h
//  Apin_Driver
//
//  Created by Apin on 15/8/4.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistTextTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *cellTextField;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (weak, nonatomic) IBOutlet UILabel *styleLabel;
@property (weak, nonatomic) IBOutlet UILabel *wanLabel;

@end
