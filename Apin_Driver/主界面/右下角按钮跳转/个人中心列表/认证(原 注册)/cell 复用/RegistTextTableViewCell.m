//
//  RegistTextTableViewCell.m
//  Apin_Driver
//
//  Created by Apin on 15/8/4.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "RegistTextTableViewCell.h"

@implementation RegistTextTableViewCell

- (void)awakeFromNib {
    // Initialization code
    UIImage * bgimage=[self OriginImage:[UIImage imageNamed:@"中间栏中小"] scaleToSize:CGSizeMake(235*KScaleW, 120*KScaleH)];
    //    _bgImageView=[[UIImageView alloc]initWithFrame:self.contentView.bounds];
    self.bgImageView.image=[bgimage stretchableImageWithLeftCapWidth:bgimage.size.width/2 topCapHeight:bgimage.size.height/2];
}
//缩放 对应比例
-(UIImage*) OriginImage:(UIImage *)image scaleToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);  //size 为CGSize类型，即你所需要的图片尺寸
    
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return scaledImage;   //返回的就是已经改变的图片
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
