//
//  RegistTwoViewController.m
//  Apin_Driver
//
//  Created by Apin on 15/8/1.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "RegistTwoViewController.h"
//#import "RegistThreeViewController.h"
#import "RegestFourViewController.h"

#import "RegistTableViewCell.h"
#import "RegistTextTableViewCell.h"


@interface RegistTwoViewController ()<UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate,UIAlertViewDelegate,UIScrollViewDelegate,UIActionSheetDelegate,UINavigationControllerDelegate>
{
    NSMutableArray * _titleArray;
//    NSMutableArray * _imageArray;
    int _selectedIndex;
//    RegistTextTableViewCell * _textCell;
    UITextField * _carCodeTextField;
    NSMutableArray * _keyFauseArray;
    UITextField * _nameTextField;
    UITextField * _carTypeTextField;//品牌
    UITextField * _carPriceTextField;//裸车价
    UITextField * _carModelTextField;//型号
    int tag;//记录没有照片点击的哪一行?
    
    CGRect messageLabelRect;
    CGRect TabelViewRect;
    CGRect nextPageButtonRect;
    CGRect topImageViewRect;
    CGRect bottomImageViewRect;
    CGRect backButtonRect;
    CGRect centerImageViewRect;
    CGRect topLabelRect;
}
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *nextPageImageView;
@property (weak, nonatomic) IBOutlet UITableView *TabelView;
@property (weak, nonatomic) IBOutlet UIButton *nextPageButton;
@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bottomImageView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIImageView *centerImageView;
//@property (weak, nonatomic) IBOutlet UILabel *topLabel;

@end

@implementation RegistTwoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _imageDic=[NSMutableDictionary dictionary];
    self.topLabel.text = @"司机认证";
    _keyFauseArray=[NSMutableArray array];
    _selectedIndex=-1;
    _titleArray=[[NSMutableArray alloc]initWithArray:@[@"车牌号",@"姓名",@"品牌",@"车型",@"裸车价",@"行驶证",@"驾驶证",@"车险",@"身份证",@"车辆照"]];
//    NSArray * paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSFileManager * manager=[NSFileManager defaultManager];
    [self tabelLoadData];
    [_TabelView registerNib:[UINib nibWithNibName:@"RegistTextTableViewCell" bundle:nil] forCellReuseIdentifier:@"textcell"];
    [_TabelView registerNib:[UINib nibWithNibName:@"RegistTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    _TabelView.separatorStyle=UITableViewCellSeparatorStyleNone;
    
//    UITapGestureRecognizer * tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction)];
//    [self.view addGestureRecognizer:tap];
}
//-(void)tapAction{
//    self.navigationController.view.frame=CGRectMake(KBgSmallViewX, KBgSmallViewY, KBgSmallViewW, KBgSmallViewH+12*KScaleH);
//    [self.view endEditing:YES];
//}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.view.frame=CGRectMake(KBgSmallViewX, KBgSmallViewY, KBgSmallViewW, KBgSmallViewH+12*KScaleH);
//    _TabelView.backgroundColor=[UIColor magentaColor];
//    self.view.backgroundColor=[UIColor blueColor];
//    _topImageView.frame=topImageViewRect;
//    _bottomImageView.frame=bottomImageViewRect;
//    _backButton.frame=backButtonRect;
//    _centerImageView.frame=centerImageViewRect;
//    _topLabel.frame=topLabelRect;
//    _nextPageButton.frame=nextPageButtonRect;
//    _TabelView.frame=TabelViewRect;
//    _messageLabel.frame=messageLabelRect;
}
-(void)tabelLoadData{
//    topImageViewRect=_topImageView.frame;
//    bottomImageViewRect=_bottomImageView.frame;
//    backButtonRect=_backButton.frame;
//    centerImageViewRect=_centerImageView.frame;
//    nextPageButtonRect=_nextPageButton.frame;
//    TabelViewRect=_TabelView.frame;
//    messageLabelRect=_messageLabel.frame;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _titleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
        if (indexPath.row==0) {
            
            RegistTextTableViewCell * _textCell = [[[NSBundle mainBundle] loadNibNamed:@"RegistTextTableViewCell" owner:self options:nil] lastObject];
            _textCell.cellTextField.delegate=self;
            
            _textCell.selectionStyle=UITableViewCellSelectionStyleNone;
            if (_carCodeTextField!=nil) {
                _textCell.cellTextField.text=_carCodeTextField.text;
            }
            _carCodeTextField=_textCell.cellTextField;
            _textCell.styleLabel.text=@"车牌号:";
            _carCodeTextField.keyboardType=UIKeyboardTypeDefault;
            _textCell.wanLabel.hidden=YES;
            
            _textCell.cellTextField.placeholder=[NSString stringWithFormat:@"请输入%@",_titleArray[indexPath.row]];
            [_textCell.cellTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
            
            return _textCell;
        }else if(indexPath.row==1){
            RegistTextTableViewCell * _textCell = [[[NSBundle mainBundle] loadNibNamed:@"RegistTextTableViewCell" owner:self options:nil] lastObject];
            _textCell.cellTextField.delegate=self;
            _textCell.selectionStyle=UITableViewCellSelectionStyleNone;
            if (_nameTextField!=nil) {
                _textCell.cellTextField.text=_nameTextField.text;
            }
            
            _nameTextField=_textCell.cellTextField;
            _textCell.styleLabel.text=@"名   字:";
            _nameTextField.keyboardType=UIKeyboardAppearanceDefault;
            _textCell.wanLabel.hidden=YES;
            _textCell.cellTextField.placeholder=[NSString stringWithFormat:@"请输入%@",_titleArray[indexPath.row]];
            [_textCell.cellTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
            return _textCell;
        }else if (indexPath.row==2){
            RegistTextTableViewCell * _textCell = [[[NSBundle mainBundle] loadNibNamed:@"RegistTextTableViewCell" owner:self options:nil] lastObject];
            _textCell.cellTextField.delegate=self;
            _textCell.selectionStyle=UITableViewCellSelectionStyleNone;
            if (_carTypeTextField!=nil) {
                _textCell.cellTextField.text=_carTypeTextField.text;
            }
            _carTypeTextField=_textCell.cellTextField;
            _textCell.styleLabel.text=@"品   牌:";
            _carTypeTextField.keyboardType=UIKeyboardAppearanceDefault;
            _textCell.wanLabel.hidden=YES;
            _textCell.cellTextField.placeholder=[NSString stringWithFormat:@"请输入%@",_titleArray[indexPath.row]];
            [_textCell.cellTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
            return _textCell;
        }else if (indexPath.row==3){
            RegistTextTableViewCell * _textCell = [[[NSBundle mainBundle] loadNibNamed:@"RegistTextTableViewCell" owner:self options:nil] lastObject];
            _textCell.cellTextField.delegate=self;
            _textCell.selectionStyle=UITableViewCellSelectionStyleNone;
            if (_carModelTextField!=nil) {
                _textCell.cellTextField.text=_carModelTextField.text;
            }
            _carModelTextField=_textCell.cellTextField;
            _textCell.styleLabel.text=@"型   号:";
            _textCell.wanLabel.hidden=YES;
            _carModelTextField.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
            
            _textCell.cellTextField.placeholder=[NSString stringWithFormat:@"请输入%@",_titleArray[indexPath.row]];
            [_textCell.cellTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
            return _textCell;
        }else if (indexPath.row==4){
            RegistTextTableViewCell * _textCell = [[[NSBundle mainBundle] loadNibNamed:@"RegistTextTableViewCell" owner:self options:nil] lastObject];
            _textCell.cellTextField.delegate=self;
            _textCell.selectionStyle=UITableViewCellSelectionStyleNone;
            if (_carPriceTextField!=nil) {
                _textCell.cellTextField.text=_carPriceTextField.text;
            }
            _carPriceTextField=_textCell.cellTextField;
            _textCell.styleLabel.text=@"裸车价:";
            _carPriceTextField.keyboardType=UIKeyboardAppearanceDefault;
            _textCell.wanLabel.hidden=NO;
            _textCell.cellTextField.placeholder=[NSString stringWithFormat:@"请输入%@",_titleArray[indexPath.row]];
            [_textCell.cellTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
            return _textCell;
        }

    
    RegistTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
//    NSDictionary * dic=_imageArray[indexPath.row];
    UIImage * image=_imageDic[_titleArray[indexPath.row]];
    [cell.cellChangeButton addTarget:self action:@selector(useCamera:) forControlEvents:UIControlEventTouchUpInside];
    if (_selectedIndex==indexPath.row&&image!=nil) {
        cell.cellTitleLabel.hidden=YES;
//        cell.cellTitleLabel.text=[NSString stringWithFormat:@"请选择%@",_titleArray[indexPath.row]];
        
        cell.cellChangeButton.hidden=NO;
        cell.cellImageView.hidden=NO;
        cell.cellImageView.image=image;
        
    }else{
        cell.cellTitleLabel.hidden=NO;
        cell.cellChangeButton.hidden=YES;
        cell.cellImageView.hidden=YES;
        if (image!=nil) {
            cell.cellTitleLabel.text=[NSString stringWithFormat:@"%@: 已选择",_titleArray[indexPath.row]];
//            cell.cellTitleLabel.textColor=[UIColor whiteColor];
        }else if(image==nil){
            cell.cellTitleLabel.text=[NSString stringWithFormat:@"%@: 请选择",_titleArray[indexPath.row]];
//            cell.cellTitleLabel.textColor=[UIColor lightGrayColor];
        }
        NSLog(@"_titleArray[%ld]==%@",indexPath.row,cell.cellTitleLabel.text);
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_selectedIndex==indexPath.row){
        return 146*KScaleH;
    }
    if (_selectedIndex!=-1) {
        return 35*KScaleH;
    }
    return 72*KScaleH;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewY+KBgSmallViewH/2+6);
    }];
    [self.view endEditing:YES];
    
    if (indexPath.row==0||indexPath.row==1||indexPath.row==2||indexPath.row==3||indexPath.row==4||_selectedIndex==indexPath.row) {
        if (_selectedIndex==-1) {
            return;
        }
        NSIndexPath * selectIndexPath=[NSIndexPath indexPathForItem:_selectedIndex inSection:0];
        RegistTableViewCell * cell=(RegistTableViewCell *)[tableView cellForRowAtIndexPath:selectIndexPath];
        cell.cellTitleLabel.hidden=NO;
        cell.cellChangeButton.hidden=YES;
        cell.cellImageView.hidden=YES;
        _selectedIndex=-1;
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
        return;
    }
    
    
    UIImage * image=_imageDic[_titleArray[indexPath.row]];
    
    if (_selectedIndex!=-1) {
        NSIndexPath * selectIndexPath=[NSIndexPath indexPathForItem:_selectedIndex inSection:0];
        RegistTableViewCell * cell=(RegistTableViewCell *)[tableView cellForRowAtIndexPath:selectIndexPath];
        cell.cellTitleLabel.hidden=NO;
        cell.cellChangeButton.hidden=YES;
        cell.cellImageView.hidden=YES;
    }
    
    _selectedIndex=(int)indexPath.row;
    if (image==nil) {
        [self useCamera:nil];
        tag=_selectedIndex;
        _selectedIndex=-1;
        [self reloadCellTabel];
        NSIndexPath * indexPath=[NSIndexPath indexPathForItem:1 inSection:0];
        [_TabelView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        return;
    }
    [self reloadCellTabel];
    [_TabelView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    
}
-(void)reloadCellTabel{
    for (int i=5; i<_titleArray.count; i++) {
        NSIndexPath * indexp=[NSIndexPath indexPathForItem:i inSection:0];
        RegistTableViewCell * cell1=(RegistTableViewCell *)[_TabelView cellForRowAtIndexPath:indexp];
        UIImage * image=_imageDic[_titleArray[indexp.row]];
        if (image!=nil) {
            cell1.cellTitleLabel.text=[NSString stringWithFormat:@"%@: 已选择",_titleArray[indexp.row]];
        }else if(image==nil){
            cell1.cellTitleLabel.text=[NSString stringWithFormat:@"%@: ",_titleArray[indexp.row]];
        }
    }
}
-(void)useCamera:(UIButton *)but{
    
    
    UIActionSheet *choiceSheet=[[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"从相册中选取", nil];
    [choiceSheet showInView:[UIApplication sharedApplication].keyWindow];
    
    
    NSLog(@"调用相机");
    if (but) {
        tag=_selectedIndex;
        _selectedIndex=-1;
    }
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:
(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            UIImagePickerController *picker=[[UIImagePickerController alloc]init];
            picker.delegate=self;
            picker.sourceType=UIImagePickerControllerSourceTypeCamera;
            [self presentModalViewController:picker animated:NO];
            
        }
        
        
} else if (buttonIndex ==1 ){
        self.navigationController.view.frame=CGRectMake(0, 0, KScaleW, KScaleH);
        // 从相册中选取
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            UIImagePickerController *controller=[[UIImagePickerController alloc]init];
            controller.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
            NSMutableArray *mediaTypes=[[NSMutableArray alloc]init];
            [mediaTypes addObject:(NSString *)kUTTypeImage];
            controller.mediaTypes=mediaTypes;
            controller.delegate=self;
            
            
            [self presentViewController:controller animated:YES completion:^{
                
            }];
        }
    }
}

#pragma mark UIImagePickerControllerDelegate
//UIImagePickerController
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    NSLog(@"image OK");
    UIImage *image=[info objectForKey:UIImagePickerControllerOriginalImage];
    _selectedIndex=tag;
    [_imageDic setObject:image forKey:_titleArray[_selectedIndex]];
    [self reloadCellTabel];
    [_TabelView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:_selectedIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    
//    NSArray * paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString * filePath=[paths[0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",_titleArray[_selectedIndex]]];
//    
//    NSData * imageData=UIImageJPEGRepresentation(image, 1);
//    NSFileManager * manager=[NSFileManager defaultManager];
//    
//    if([manager createFileAtPath:filePath contents:imageData attributes:nil]){
//        NSLog(@",保存成功 %@",filePath);
//    }
    
    
//    [_TabelView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:_selectedIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    [self dismissModalViewControllerAnimated:YES];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    NSLog(@"image cancel");
    [self reloadCellTabel];
    [_TabelView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:_selectedIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)backButtonAction:(id)sender {
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewY+KBgSmallViewH/2);
    }];
    [self.navigationController popToViewController:self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] animated:NO];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
}

#pragma mark textFieldDelegate代理方法
//屏幕上移
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    //    [self.view endEditing:NO];
    
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewH/2+30);
    }];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewY+KBgSmallViewH/2);
    }];
    return YES;
}

- (IBAction)nextPageButtonAction:(id)sender {
    
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewY+KBgSmallViewH/2+6);
    }];
    [self.view endEditing:YES];
    if (_carCodeTextField.text.length==0) {
        [self creatAlertViewWithMassage:@"请输入车牌号"];
        return;
    }
    if (_nameTextField.text.length==0) {
        [self creatAlertViewWithMassage:@"请输入姓名"];
        return;
    }
    if (_carTypeTextField.text.length==0) {
        [self creatAlertViewWithMassage:@"请输入车品牌"];
        return;
    }
    if (_carPriceTextField.text.length==0) {
        [self creatAlertViewWithMassage:@"请输入裸车价"];
        return;
    }
    if (_carModelTextField.text.length==0) {
        [self creatAlertViewWithMassage:@"请输入车型号"];
        return;
    }
    for (NSString * str in _imageDic.allValues) {
        if (str==nil) {
            [self creatAlertViewWithMassage:@"请完善证件信息"];
            return;
        }
    }
//    NSLog(@"_imageDic==%@",_imageDic);
    
#warning 需要接口 上传图片   上传成功再跳
    
    for (int i=5;i<_titleArray.count;i++) {
        if (_imageDic[_titleArray[i]]==nil) {
            [self creatAlertViewWithMassage:[NSString stringWithFormat:@"请选择%@图片",_titleArray[i]]];
            return;
        }
    }
    _nextPageButton.enabled=!_nextPageButton.enabled;
        AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
    NSString * price =[NSString stringWithFormat:@"%.2f",[_carPriceTextField.text floatValue]];
    NSDictionary * dic=@{@"userId":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],
                         @"carPlate":_carCodeTextField.text,
                         @"driverName":_nameTextField.text,
                         @"nakePrice":price,
                         @"brand":_carTypeTextField.text,
                         @"model":_carModelTextField.text};
    _messageLabel.hidden=NO;
    _messageLabel.text=@"正在上传姓名,车牌号,车型号和裸车价";
    [manager POST:CarPlateURL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"上传车牌号和司机姓名 结果:%@",responseObject);
//        _nextPageButton.enabled=!_nextPageButton.enabled;
        if ([responseObject[@"head"][@"msg"] isEqualToString:@"成功"]) {
            _messageLabel.text=@"上传姓名车牌号等成功";
            [self importImageWithIndex:5 Add:YES];
        }else{
            _messageLabel.hidden=YES;
            _nextPageButton.enabled=!_nextPageButton.enabled;
            [self creatAlertViewWithMassage:@"上传车牌号等失败"];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self creatAlertViewWithMassage:@"上传车牌号等失败"];
        _messageLabel.hidden=YES;
        _nextPageButton.enabled=!_nextPageButton.enabled;
    }];
}

-(void)importImageWithIndex:(int)index Add:(BOOL)added{
    if (index==_titleArray.count) {
        _messageLabel.hidden=YES;
        return;
    }
    
    
    
    UIImage * image=_imageDic[_titleArray[index]];
    NSLog(@"图片大小: %.2fx%.2f",image.size.width,image.size.height);
    
    NSData * imageData=UIImageJPEGRepresentation(image, 1);
    if (300000.0/imageData.length<1) {
        imageData=UIImageJPEGRepresentation(image, 300000.0/imageData.length);
    }
    
    NSLog(@"%f",300000.0/imageData.length);
//    NSFileManager * manager=[NSFileManager defaultManager];
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
//    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"sms.gif"]];   // 保存文件的名称
//    NSLog(@"filePath==%@",filePath);
//    BOOL result = [UIImagePNGRepresentation([UIImage imageWithData:imageData])writeToFile: filePath    atomically:YES];
    
    NSLog(@"imageData.length==%d",imageData.length);
    
    NSString * imageStr=[imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    AFHTTPRequestOperationManager * manager1=[AFHTTPRequestOperationManager manager];
    if (manager1==nil) {
        return;
    }
    NSString * codeStr=_titleArray[index];
    
    
    manager1.requestSerializer.timeoutInterval=60;
    _messageLabel.text=[NSString stringWithFormat:@"正在上传  %@...",codeStr];
    NSNotificationCenter * notiCenter=[NSNotificationCenter defaultCenter];
    [notiCenter addObserver:self selector:@selector(progessAction:) name:@"上传进度" object:_messageLabel];
    _messageLabel.hidden=NO;
    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
    NSDictionary * dic =@{@"fdata":imageStr, @"fileType":[NSString stringWithFormat:@"%d",index-3],@"userId":[defaults objectForKey:@"userId"]};
    [manager1 POST:UPLOADURL parameters:dic constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"responseObject==%@",responseObject);
        [notiCenter removeObserver:self];
        if ([responseObject[@"head"][@"msg"] isEqualToString:@"成功"]) {
            _messageLabel.text=[NSString  stringWithFormat:@"上传 %@ 成功",_titleArray[index]];
            if (added) {
                
                [self importImageWithIndex:index+1 Add:YES];
            }
            
            _messageLabel.hidden=NO;
            if (index==_titleArray.count-1) {
                
                [manager1 POST:[NSString stringWithFormat:CentifiedURL,[defaults objectForKey:@"userId"]] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    if ([[NSString stringWithFormat:@"%@",responseObject[@"head"][@"msg"]]isEqualToString:@"成功"]) {
                        [self creatAlertViewWithMassage:@"上传成功"];
                        _nextPageButton.enabled=!_nextPageButton.enabled;
                        
                    }else{
                        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"提交认证失败" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"重试", nil];
                        [alert show];
                        
                    }
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                    UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"提交认证失败" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"重试", nil];
                    [alert show];
                    
                }];
                
                
                
            }
            
        }else{
            NSMutableString * str=[NSMutableString stringWithString:_titleArray[index]];
            [str replaceCharactersInRange:[str rangeOfString:@"   "] withString:@""];
            [self creatAlertViewWithMassage:[NSString stringWithFormat:@"上传%@失败",str]];
            _nextPageButton.enabled=!_nextPageButton.enabled;
            _messageLabel.hidden=YES;
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [notiCenter removeObserver:self];
        NSLog(@"errorerror==%@",error);
        NSMutableString * str=[NSMutableString stringWithString:_titleArray[index]];
        if ([str rangeOfString:@"   "].length>0) {
            [str replaceCharactersInRange:[str rangeOfString:@"   "] withString:@""];
        }
        _nextPageButton.enabled=!_nextPageButton.enabled;
        _messageLabel.hidden=YES;
        
//        [self creatAlertViewWithMassage:[NSString stringWithFormat:@"%@",error.userInfo[@"NSLocalizedDescription"]]];
        [self creatAlertViewWithMassage:@"连接失败,请检查网络连接"];
    }];
    
}

-(void)progessAction:(NSNotification *)noti{
    NSLog(@"%@",noti);
    if ([noti.userInfo[@"上传进度"] intValue]<=100&&[noti.userInfo[@"上传进度"] intValue]>=0) {
        _messageLabel.text=[NSString stringWithFormat:@"%@%@%%",_messageLabel.text,noti.userInfo[@"上传进度"]];
    }
    
}

-(void)creatAlertViewWithMassage:(NSString *)massage{
    UIAlertView*alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:massage delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ([alertView.message isEqualToString:@"上传成功"]) {
        
        
        RegestFourViewController * threeCtr=[[RegestFourViewController alloc]init];
        [self.navigationController pushViewController:threeCtr animated:NO];
    }else if([alertView.message isEqualToString:@"提交认证失败"]){
        if (buttonIndex) {
            
            AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
            if (manager==nil) {
                return;
            }
            NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
            
            [manager POST:[NSString stringWithFormat:CentifiedURL,[defaults objectForKey:@"userId"]] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                if ([[NSString stringWithFormat:@"%@",responseObject[@"head"][@"msg"]]isEqualToString:@"成功"]) {
                    [self creatAlertViewWithMassage:@"上传成功"];
                    _nextPageButton.enabled=!_nextPageButton.enabled;
                    
                    
                    
                }else{
                    UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"提交认证失败" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"重试", nil];
                    [alert show];
                    
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"提交认证失败" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"重试", nil];
                [alert show];
                
            }];
        }
    }
}
@end
