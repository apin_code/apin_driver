//
//  SelectPayStyleTableViewCell.h
//  Apin_Driver
//
//  Created by Apin on 15/8/8.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectPayStyleTableViewCell : UITableViewCell
//@property (weak, nonatomic) IBOutlet UIImageView *statusImageView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *accountType;
@property (weak, nonatomic) IBOutlet UILabel *accountTypeLabel;

@end
