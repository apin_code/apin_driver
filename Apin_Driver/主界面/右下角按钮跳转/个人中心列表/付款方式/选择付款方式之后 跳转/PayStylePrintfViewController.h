//
//  PayStylePrintfViewController.h
//  Apin_Driver
//
//  Created by Apin on 15/8/7.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "DriverFatherViewController.h"

@interface PayStylePrintfViewController : DriverFatherViewController
@property(nonatomic,copy)NSString * payStyleString;
@property(nonatomic,copy)NSString * accountType;

@property(copy,nonatomic)NSString * phone;
@property(copy,nonatomic)NSString * password;

@property(nonatomic,strong)NSDictionary * dic;



@end
