//
//  PayStylePrintfViewController.m
//  Apin_Driver
//
//  Created by Apin on 15/8/7.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "PayStylePrintfViewController.h"
#import "DriverCenterViewController.h"
@interface PayStylePrintfViewController ()<UITextFieldDelegate>
{
    UITextField * _textField;
}
@end

@implementation PayStylePrintfViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.topTitleString=@"收款账号";
    UIImage * image=[UIImage imageNamed:@"中间栏大"];
    self.bgImage=[image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    self.bottomImageView.image=[UIImage imageNamed:@"底边"];
    
    UIImageView * textImageView=[[UIImageView alloc]initWithFrame:CGRectMake(20*KScaleW, 138*KScaleH, 195*KScaleW, 30*KScaleH)];
    textImageView.image=[UIImage imageNamed:@"输入账号"];
    [self.view addSubview:textImageView];
    
    NSString *str=[NSString stringWithFormat:@"%@",_dic[@"accountType"]];
    
    _textField=[[UITextField alloc]initWithFrame:textImageView.frame];
    _textField.text=[NSString stringWithFormat:@"请输入您的%@账号",str];
    _textField.delegate=self;
    _textField.textColor=[UIColor whiteColor];
    _textField.borderStyle=UITextBorderStyleNone;
    [self.view addSubview:_textField];
    
    UIButton * OKButton=[[UIButton alloc]initWithFrame:CGRectMake(120*KScaleW-40*KScaleW, 16*KScaleH, 80*KScaleW, 40*KScaleH)];
    [self.bottomView addSubview:OKButton];
    [OKButton setBackgroundImage:[UIImage imageNamed:@"确定按钮"] forState:UIControlStateNormal];
    [OKButton addTarget:self action:@selector(OKButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
//    UITapGestureRecognizer * tap=[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(<#selector#>)
    
    // Do any additional setup after loading the view.
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewY+KBgSmallViewH/2);
    }];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    NSString *str=[NSString stringWithFormat:@"%@",_dic[@"filename"]];
//    if ([str isEqualToString:@"VISA"]) {
//        str=@"银行卡";
//    }
    
    if ([_textField.text isEqualToString:[NSString stringWithFormat:@"请输入您的%@账号",str]]) {
        _textField.text=@"";
    }
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewH/2+30);
    }];
}
-(void)OKButtonAction{
    
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewY+KBgSmallViewH/2);
    }];
    //个人中心列表
//    DriverCenterViewController * driverCenterCtr=[[DriverCenterViewController alloc]init];
//    self.navigationController.viewControllers=@[driverCenterCtr];
#warning 需要写接口   绑定付款方式  绑定成功才能跳转
    AFHTTPRequestOperationManager* manager=[AFHTTPRequestOperationManager manager];
//    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
//    ?accountType=%@&accountNum=%@&userId=%@
    NSDictionary * dic=@{@"accountType":_dic[@"accountType"],@"accountNum":_textField.text,@"userId":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    [manager POST:SJBoundCount parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"绑定账号 %@",responseObject);
        NSString * str=[NSString stringWithFormat:@"%@",responseObject[@"head"][@"msg"]];
        if (![str isEqualToString:@"<null>"]) {
            if ([responseObject[@"head"][@"msg"] isEqualToString:@"成功"]) {
                DriverCenterViewController * driverCenterCtr=[[DriverCenterViewController alloc]init];
                self.navigationController.viewControllers=@[driverCenterCtr];
            }else {
                
            }
        }else{
            
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
//    DriverCenterViewController * driverCenterCtr=[[DriverCenterViewController alloc]init];
//    self.navigationController.viewControllers=@[driverCenterCtr];
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.view.frame=CGRectMake(KBgSmallViewX, KBgSmallViewY, KBgSmallViewW, KBgSmallViewH+12*KScaleH);
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"textFieldDidEndEditing");
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewH/2);
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
