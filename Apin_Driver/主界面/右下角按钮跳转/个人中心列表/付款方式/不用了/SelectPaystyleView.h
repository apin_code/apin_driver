//
//  SelectPaystyleView.h
//  Apin_Driver
//
//  Created by Apin on 15/8/7.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectPaystyleView : UIView
@property(nonatomic,assign)int payStyle;
@property(nonatomic,strong)NSArray * arr;

@end
