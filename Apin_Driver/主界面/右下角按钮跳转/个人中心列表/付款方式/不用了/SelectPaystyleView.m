//
//  SelectPaystyleView.m
//  Apin_Driver
//
//  Created by Apin on 15/8/7.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "SelectPaystyleView.h"
@interface SelectPaystyleView()
{
    UIImageView *payImageView;
    UILabel *payLabel;
    UIImageView *payOKImageView;
}
//@property (weak, nonatomic) IBOutlet UIImageView *payImageView;
//@property (weak, nonatomic) IBOutlet UILabel *payLabel;
//@property (weak, nonatomic) IBOutlet UIImageView *payOKImage;

@end
@implementation SelectPaystyleView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadMyView];
    }
    return self;
}
-(void)loadMyView{
    _arr=@[@"微信",@"支付宝",@"银行卡"];
    self.backgroundColor=[UIColor clearColor];
    UIImageView * bgImageView=[[UIImageView alloc]initWithFrame:self.bounds];
    bgImageView.image=[UIImage imageNamed:@"中间栏大"];
    [self addSubview:bgImageView];
    
    payImageView=[[UIImageView alloc]initWithFrame:CGRectMake(50*KScaleW, 21*KScaleH, 29*KScaleW, 29*KScaleH)];
    [self addSubview:payImageView];
    
    payLabel=[[UILabel alloc]initWithFrame:CGRectMake(104*KScaleW, 25*KScaleH, 65*KScaleW, 21*KScaleH)];
    payLabel.font=[UIFont fontWithName:nil size:16*KScaleH];
    payLabel.textColor=[UIColor whiteColor];
    [self addSubview:payLabel];
    
    payOKImageView=[[UIImageView alloc]initWithFrame:CGRectMake(181*KScaleW, 31*KScaleH, 40*KScaleW, 10*KScaleH)];
    
}
-(void)setPayStyle:(int)payStyle{
    _payStyle=payStyle;
    payImageView.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",_arr[_payStyle-1]]];
    payLabel.text=_arr[_payStyle-1];
    if (_payStyle==3) {
        [self addSubview:payOKImageView];
    }
}
@end
