//
//  PayStyleViewController.m
//  Apin_Driver
//
//  Created by Apin on 15/8/7.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "PayStyleViewController.h"
#import "SelectPaystyleView.h"
#import "PayStylePrintfViewController.h"
#import "SelectPayStyleTableViewCell.h"
@interface PayStyleViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView * _tabelView;
    NSMutableArray * _dataArray;
    NSArray * _sbArray;
}
@end

@implementation PayStyleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.topTitleLabel.font=[UIFont fontWithName:@"" size:18*KScaleH];
    self.topTitleString=@"收款方式";
    _sbArray=@[@"微信支付",@"支付宝",@"银行卡"];
    [self loadData];
    [self loadMyView];
}

-(void)loadMyPayView{
    for (int i=0; i<3; i++) {
        SelectPaystyleView * payView=[[SelectPaystyleView alloc]initWithFrame:CGRectMake(2.5*KScaleW, 45*KScaleH+73*KScaleH*i, 235*KScaleW, 72*KScaleH)];
        payView.payStyle=i+1;
        [self.view addSubview:payView];
        UITapGestureRecognizer * tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectPayStyle:)];
        [payView addGestureRecognizer:tap];
    }
}
-(void)loadData{
    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
    
    AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:SJWayOfPay,[defaults objectForKey:@"userId"]] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject[@"head"][@"msg"] isEqualToString:@"成功"]) {
            [_dataArray removeAllObjects];
            [_dataArray addObjectsFromArray:responseObject[@"body"]];
            if (_dataArray.count==0) {
                [self loadMyView];
            }else{
                [_tabelView reloadData];
            }
            self.centerBgImageView.hidden=YES;
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}


-(void)loadMyView{
    
    _tabelView=[[UITableView alloc]initWithFrame:CGRectMake(2.5*KScaleW, 44*KScaleH, 235*KScaleW, 220*KScaleH) style:UITableViewStylePlain];
    UIImageView * imageView=[[UIImageView alloc]initWithFrame:CGRectMake(2.5*KScaleW, 44*KScaleH, 235*KScaleW, 220*KScaleH)];
    imageView.image=[UIImage imageNamed:@"中间栏大"];
    [self.view addSubview:imageView];
    _tabelView.scrollEnabled=NO;
    _tabelView.delegate=self;
    _tabelView.dataSource=self;
    [self.view addSubview:_tabelView];
    _tabelView.rowHeight=_tabelView.frame.size.height/3;
    _tabelView.separatorStyle=UITableViewCellSeparatorStyleNone;
    _tabelView.backgroundColor=[UIColor clearColor];
    _dataArray=[NSMutableArray array];
    [_tabelView registerNib:[UINib nibWithNibName:@"SelectPayStyleTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    _tabelView.backgroundColor=[UIColor clearColor];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectPayStyleTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    NSDictionary * dic=_dataArray[indexPath.row];
    [cell.accountType setImageWithURL:[NSURL URLWithString:dic[@"filepath"]]];
    [cell.accountType setImageWithURL:[NSURL URLWithString:dic[@"filepath"]] placeholderImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",dic[@"filename"]]]];
    cell.accountTypeLabel.text=dic[@"filename"];
    
    
//    cell.accountTypeLabel.text=dic[@"accountType"];
//    if ([cell.accountTypeLabel.text isEqualToString:@"VISA"]) {
//        cell.accountTypeLabel.text=@"银行卡";
//    }
    [cell.accountType setImageWithURL:[NSURL URLWithString:dic[@"filepath"]] placeholderImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",cell.accountTypeLabel.text]]];
    if (cell.accountTypeLabel.text.length==0) {
        
        cell.accountTypeLabel.text=_sbArray[indexPath.row];
        cell.accountType.image=[UIImage imageNamed:_sbArray[indexPath.row]];
        
        
        
    }
    if ([dic[@"status"] intValue]==0) {
        cell.statusLabel.text=@"暂未开通";
    }else {
        if ([dic[@"isSet"] isEqualToString:@"Y"]) {
            cell.statusLabel.text=@"已绑定";
        }else{
            cell.statusLabel.text=@"";
        }
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary * dic=_dataArray[indexPath.row];
    SelectPayStyleTableViewCell * cell=(SelectPayStyleTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    if ([cell.statusLabel.text isEqualToString:@"暂未开通"]||[cell.statusLabel.text isEqualToString:@"已绑定"]) {
        return;
    }
    PayStylePrintfViewController*payPritfCtr=[[PayStylePrintfViewController alloc]init];
    payPritfCtr.dic=dic;
    [self.navigationController pushViewController:payPritfCtr animated:NO];

}
//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//    UIImageView * imageView=[[UIImageView alloc]initWithFrame:cell.contentView.bounds];
//    imageView.image=[UIImage imageNamed:@"边框3"];
//    [cell.contentView addSubview:imageView];
//    cell.backgroundColor=[UIColor clearColor];
//}



-(void)selectPayStyle:(UITapGestureRecognizer *)tap{
    SelectPaystyleView * payView=(SelectPaystyleView *)tap.view;
    if (payView.payStyle==3) {
        return;
    }else{
        PayStylePrintfViewController * printfCtr=[[PayStylePrintfViewController alloc]init];
        printfCtr.payStyleString=payView.arr[payView.payStyle-1];
        
        printfCtr.phone=_phone;
        printfCtr.password=_password;
        [self.navigationController pushViewController:printfCtr animated:NO];
    }
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.view.frame=CGRectMake(KBgSmallViewX, KBgSmallViewY, KBgSmallViewW, KBgSmallViewH);
}
@end
