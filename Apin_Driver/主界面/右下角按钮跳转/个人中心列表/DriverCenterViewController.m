//
//  DriverCenterViewController.m
//  Apin_Driver
//
//  Created by Apin on 15/8/5.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "DriverCenterViewController.h"
//#import "DriverFatherViewController.h"//父视图
#import "IncomeInfoViewController.h"//收入明细
#import "MyOrderViewController.h"//我的订单
#import "FeesViewController.h"//费用结算

#import "PayStyleViewController.h"
#import "PersonCenterInfoViewController.h"//个人中心 资料详情
#import "RegistTwoViewController.h"
#import "DriverFeedBackViewController.h"
@interface DriverCenterViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSArray * _dataArray;
    UITableView * _driverCenterTableView;
    
    UIImageView * _headImageView;
    UILabel * _nameLabel;
    
    
}
@end

@implementation DriverCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadMyView];
//    [self loadData];
    // Do any additional setup after loading the view.
}

-(void)loadMyView{
    //    UIView
    AppDelegate * delegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.backImageView.frame=CGRectMake(15*KScaleW, 19*KScaleH, 15, 30);
    self.topTitleLabel.frame=CGRectMake(120*KScaleW-100*KScaleW, 49*KScaleH, 200*KScaleW, 14*KScaleH);
    self.topTitleLabel.font=[UIFont fontWithName:nil size:11*KScaleH];
    self.topTitleString=delegate.personmodel.pdriver;
    
    self.topImageView.frame=CGRectMake(0, 0, 240*KScaleW, 68*KScaleH);
    UIImage * image=[UIImage imageNamed:@"上边框"];
    self.topImageView.image=[image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:80];
    self.centerBgImageView.hidden=YES;
    
    _headImageView=[[UIImageView alloc]initWithFrame:CGRectMake(120*KScaleW-20*KScaleW, 7*KScaleH, 40*KScaleH, 40*KScaleH)];
    
    NSLog(@"头像路径 %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"headpic"]);
    [_headImageView setImageWithURL:[[NSUserDefaults standardUserDefaults]objectForKey:@"headpic"] placeholderImage:[UIImage imageNamed:@"头像"]];
    
    _headImageView.contentScaleFactor=_headImageView.frame.size.height/2;
    _headImageView.layer.cornerRadius=_headImageView.frame.size.height/2;
    _headImageView.layer.masksToBounds=YES;
    [self.view addSubview:_headImageView];
    _headImageView.userInteractionEnabled=YES;
    UITapGestureRecognizer * tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction)];
    [_headImageView addGestureRecognizer:tap];
    
//    _nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(20*KScaleW, 47*KScaleH, 200*KScaleW, 21*KScaleH)];
//    _nameLabel.text=delegate.personmodel.pdriver;
//    _nameLabel.textAlignment=NSTextAlignmentCenter;
//    _nameLabel.textColor=[UIColor whiteColor];
//    _nameLabel.font=[UIFont fontWithName:nil size:11*KScaleH];
//    [self.view addSubview:_nameLabel];
    
//    _dataArray=@[@"收入明细",@"收款方式",@"我的订单",@"费用结算",@"意见反馈"];
    _dataArray=@[@"我的行程",@"我的收益",@"司机认证",@"收款方式",@"意见反馈"];
    _driverCenterTableView=[[UITableView alloc]initWithFrame:CGRectMake(2.5*KScaleW, 68*KScaleH, 235*KScaleW, 196*KScaleH) style:UITableViewStylePlain];
    _driverCenterTableView.rowHeight=_driverCenterTableView.frame.size.height/_dataArray.count;
    _driverCenterTableView.delegate=self;
    _driverCenterTableView.dataSource=self;
    _driverCenterTableView.backgroundColor=[UIColor clearColor];
    _driverCenterTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_driverCenterTableView];
    _driverCenterTableView.scrollEnabled=NO;
}
//点击头像 进入个人中心
-(void)tapAction{
    PersonCenterInfoViewController * perctr=[[PersonCenterInfoViewController alloc]init];
    [self.navigationController pushViewController:perctr animated:NO];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell.backgroundColor=[UIColor clearColor];
        cell.contentView.backgroundColor=[UIColor clearColor];
        
        
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.textLabel.textAlignment=NSTextAlignmentCenter;
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.textLabel.font=[UIFont fontWithName:nil size:15*KScaleH];
        
        if (indexPath.row==2) {
            AppDelegate * delegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
            
            UILabel * lab=[[UILabel alloc]initWithFrame:CGRectMake(174*KScaleW, 12*KScaleH, 46*KScaleW, 14*KScaleH)];
            lab.textColor=[UIColor colorWithRed:1 green:1 blue:1 alpha:204/255.0];
            lab.font=[UIFont systemFontOfSize:10*KScaleH];
            lab.tag=10;
            [cell.contentView addSubview:lab];
//            lab.backgroundColor=[UIColor magentaColor];
//            lab.backgroundColor=[UIColor magentaColor];
            NSArray * array=@[@"审核中",@"审核通过",@"审核未通过",@"待审核"];
//            lab.textColor=[UIColor whiteColor];
            lab.text=array[[delegate.personmodel.auditStatus intValue]];
            
        }
        
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.textLabel.text=_dataArray[indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([cell.contentView viewWithTag:100]==nil) {
        UIImageView * imageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, tableView.frame.size.height/5)];
        imageView.tag=100;
        imageView.image=[UIImage imageNamed:@"中间栏小小"];
        [cell.contentView addSubview:imageView];
        cell.backgroundColor=[UIColor clearColor];
    }
    
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIViewController * fatherCtr=nil;
    switch (indexPath.row) {
        case 0:{
            fatherCtr=[[MyOrderViewController alloc]init];
        }
            break;
            
        case 1:
        {
            fatherCtr=[[FeesViewController alloc]init];
        }
            break;
            
        case 2:
        {
            AppDelegate * delegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
            
            if ([delegate.personmodel.auditStatus intValue]==0) {
                //在审核当中
                UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"正在审核当中..." delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alertView show];
                return;
            }else if ([delegate.personmodel.auditStatus intValue]==1){
                //审核通过
                UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"已审核通过" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alertView show];
                return;
            }else if ([delegate.personmodel.auditStatus intValue]==2){
                //审核没通过
                
            }else if ([delegate.personmodel.auditStatus intValue]==3){
                //未审核
                
            }
            fatherCtr=[[RegistTwoViewController alloc]init];
        
        }
            break;
            
        case 3:
        {
            
            fatherCtr=[[PayStyleViewController alloc]init];
            
        }
            break;
            
        case 4:
        {
            fatherCtr=[[DriverFeedBackViewController alloc]init];
            
        }
            break;
            
        default:
            break;
    }
    
    [self.navigationController pushViewController:fatherCtr animated:NO];
}
-(void)viewWillAppear:(BOOL)animated{
    AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
    if (manager==nil) {
        return;
    }
    AppDelegate * delegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
    
    NSNotificationCenter * notiCenter=[NSNotificationCenter defaultCenter];
    [notiCenter addObserver:self selector:@selector(progessAction:) name:@"上传进度" object:nil];
    
    [manager POST:GetCentifiedResultURL parameters:@{@"userId":[defaults objectForKey:@"userId"]} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"head"][@"msg"] isEqualToString:@"成功"]) {
            delegate.personmodel.auditStatus=[NSString stringWithFormat:@"%@",responseObject[@"body"]];
            [_driverCenterTableView reloadData];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}
-(void)progessAction:(NSNotification *)noti{
    NSLog(@"%@",noti);
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
