//
//  LoginViewController.m
//  Apin_Driver
//
//  Created by Apin on 15/8/1.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "LoginViewController.h"
#import "RegistViewController.h"
#import "DriverCenterViewController.h"
#import "PayStyleViewController.h"
#import "BackPasswordViewController.h"

@interface LoginViewController ()<UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
}
-(void)viewWillAppear:(BOOL)animated{
    _bgImageView.center=self.view.center;
    UIImage * image=[UIImage imageNamed:@"中间栏大"];
    _bgImageView.image=[image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    UITextField * passwordTextField=(UITextField*)[self.view viewWithTag:9];
    passwordTextField.secureTextEntry=YES;
    self.navigationController.view.frame=CGRectMake(KBgSmallViewX, KBgSmallViewY, KBgSmallViewW, KBgSmallViewH);
    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"phone"]) {
        UITextField * phoneTextField=(UITextField*)[self.view viewWithTag:8];
        phoneTextField.text=[defaults objectForKey:@"phone"];
        if ([defaults objectForKey:@"password"]) {
            UITextField * passwordTextField=(UITextField*)[self.view viewWithTag:9];
            passwordTextField.text=[defaults objectForKey:@"password"];
        }
    }
}

//去注册
- (IBAction)gotoRegistAction:(id)sender {
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewY+KBgSmallViewH/2);
    }];
    RegistViewController * registCtr=[[RegistViewController alloc]init];
    [self.navigationController pushViewController:registCtr animated:NO];
}

//忘记密码
- (IBAction)forgetPassword:(id)sender {
#warning 忘记密码
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewY+KBgSmallViewH/2);
    }];
    BackPasswordViewController * backCtr=[[BackPasswordViewController alloc]init];
    [self.navigationController pushViewController:backCtr animated:NO];
}

//微信登陆
- (IBAction)WXLoginAction:(id)sender {
    
#warning 微信登陆
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewY+KBgSmallViewH/2);
    }];
}

//登录
- (IBAction)loginButtonAction:(id)sender {
    
//    [XGPush unRegisterDevice];
//    [XGPush initForReregister:^{
//        NSLog(@"初始化成功");
//    }];
//    
//    [XGPush setAccount:@"18937181326"];
//    [XGPush initForReregister:^{
//        NSLog(@"注销 重新注册");
//    }];
    
    UITextField * phoneTextField=(UITextField*)[self.view viewWithTag:8];
    UITextField * passwordTextField=(UITextField*)[self.view viewWithTag:9];
    if (phoneTextField.text!=nil&&phoneTextField.text.length!=11) {
        [self creatAlertViewWithMassage:@"手机位数不正确"];
        return;
    }else if(passwordTextField.text.length<8||passwordTextField.text.length>20){
        [self creatAlertViewWithMassage:@"密码位数应为8~20位"];
        return;
    }
    
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewY+KBgSmallViewH/2);
    }];
    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
    AppDelegate * delegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
//    delegate.userID=phoneTextField.text;
    

    AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
    if (manager==nil) {
        return;
    }
    UIButton * but=(UIButton *)sender;
    but.enabled=NO;
    NSString * bundelID=[[NSBundle mainBundle] bundleIdentifier];
    NSDictionary * dic=@{@"phone":phoneTextField.text,@"password":passwordTextField.text};
    delegate.loadURLView.hidden=NO;
//    [manager.requestSerializer setTimeoutInterval:5];
    manager.requestSerializer=[AFHTTPRequestSerializer serializer];
    [manager POST:LoginURL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"登录responseObject%@",responseObject);
        but.enabled=YES;
        delegate.loadURLView.hidden=YES;
        delegate.isLogin=YES;
        if ([[NSString stringWithFormat:@"%@",responseObject[@"head"][@"msg"]
              ] isEqualToString:@"成功"]) {
            
            
            
            
            
            
            //登陆成功 15821814221
            [defaults  setObject:responseObject[@"body"][@"userId"] forKey:@"userId"];
//            [self XGPushReloadDataWith:phoneTextField.text SetDelegatePersonModelWith:responseObject];
            [self SetDelegatePersonmodelWith:responseObject];
            
            //添加跳转
            [defaults setObject:phoneTextField.text forKey:@"phone"];
            [defaults setObject:passwordTextField.text forKey:@"password"];
            [defaults setObject:responseObject[@"body"][@"headPic"] forKey:@"headpic"];
            
            NSString * urlStr=[NSString stringWithFormat:@"%@?identifier=%@&type=driver&userId=%@",SENDBUNDELIDURL,bundelID,responseObject[@"body"][@"userId"]];
            //@{@"identifier":bundelID,@"type":@"driver"}
            [manager POST:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
            }];
            
            
            [ApinXGPush startPushWithAccount:phoneTextField.text];
            ApinDriverMainViewController * mainCtr=(ApinDriverMainViewController  * )delegate.window.rootViewController;
            mainCtr.loginOK=YES;
            DriverCenterViewController * driverCtr=[[DriverCenterViewController alloc]init];
                self.navigationController.viewControllers=@[driverCtr];
            
            
        }else{
            //登录失败
            [self creatAlertViewWithMassage:responseObject[@"head"][@"msg"]];
            but.enabled=YES;
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        delegate.loadURLView.hidden=YES;
        NSLog(@"%@  %@",[self class],error.userInfo);
        but.enabled=YES;
        NSLog(@"NSDebugDescription=%@",error.userInfo[@"NSLocalizedDescription"]);
//        [self creatAlertViewWithMassage:@"连接失败,请检查网络连接"];
        
        
        if (error.code==3840||error.code==-1011) {
            [self creatAlertViewWithMassage:@"请求失败"];
        }else if (error.userInfo[@"NSDebugDescription"]&&[[NSString stringWithFormat:@"%@",error.userInfo[@"NSDebugDescription"]] length]<30) {
            
            
            [self creatAlertViewWithMassage:error.userInfo[@"NSDebugDescription"]];
            
            
            
        }else if(error.userInfo[@"NSLocalizedDescription"]&&[[NSString stringWithFormat:@"%@",error.userInfo[@"NSLocalizedDescription"]] length]>30){
            
            
            [self creatAlertViewWithMassage:error.userInfo[@"NSLocalizedDescription"]];
            
            
            
        }else{
            [self creatAlertViewWithMassage:@"请求数据失败,请稍后重试"];
        }
        
        
        
//        [self creatAlertViewWithMassage:[NSString stringWithFormat:@"%@",error]];
//        if (error.userInfo[@"NSLocalizedDescription"]==nil) {
//            [self creatAlertViewWithMassage:error.userInfo[@"NSDebugDescription"]];
//        }else{
//            [self creatAlertViewWithMassage:error.userInfo[@"NSLocalizedDescription"]];
//        }
    }];
    
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewY+KBgSmallViewH/2);
    }];
}
-(void)SetDelegatePersonmodelWith:(id)responseObject{
    AppDelegate * delegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
    if (delegate.personmodel==nil) {
        delegate.personmodel=[[PersonModel alloc]init];
    }
    delegate.personmodel.puserId=responseObject[@"body"][@"userId"];
    delegate.personmodel.pisBindAccount=responseObject[@"body"][@"isBindAccount"];
    delegate.personmodel.pauditStatus=responseObject[@"body"][@"auditStatus"];
    delegate.personmodel.pheadPic=responseObject[@"body"][@"headPic"];
    delegate.personmodel.pdriver=responseObject[@"body"][@"driver"];
    delegate.personmodel.pphone=responseObject[@"body"][@"phone"];
    delegate.personmodel.pplate=responseObject[@"body"][@"plate"];
    delegate.personmodel.auditStatus=responseObject[@"body"][@"auditStatus"];
}
-(void)XGPushReloadData{
    
}
//返回
- (IBAction)backButtonAction:(id)sender {
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewY+KBgSmallViewH/2);
    }];
    AppDelegate * delegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
    if (delegate.backBlock) {
        delegate.backBlock(self);
    }
}
//取消点击
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewY+KBgSmallViewH/2);
    }];
    
}

//屏幕上移
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    //    [self.view endEditing:NO];
    
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewH/2+30);
    }];
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"textFieldDidEndEditing");
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewH/2);
    }];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewY+KBgSmallViewH/2);
    }];
    return YES;
}
-(void)creatAlertViewWithMassage:(NSString *)massage{
    UIAlertView*alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:massage delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
}

@end
