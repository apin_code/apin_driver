//
//  ForgetPasswordTableViewCell.m
//  Apin_Driver
//
//  Created by Apin on 15/8/8.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "ForgetPasswordTableViewCell.h"

@interface ForgetPasswordTableViewCell ()
{
    
    
}
@end
@implementation ForgetPasswordTableViewCell


- (void)awakeFromNib {
    // Initialization code
    [_cellTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
