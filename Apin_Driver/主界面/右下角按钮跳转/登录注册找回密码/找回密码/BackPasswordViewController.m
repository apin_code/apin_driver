//
//  BackPasswordViewController.m
//  Apin_Driver
//
//  Created by Apin on 15/8/8.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "BackPasswordViewController.h"
#import "ForgetPasswordTableViewCell.h"
#import "DriverCenterViewController.h"
//#import "LoginViewController.h"
@interface BackPasswordViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIAlertViewDelegate>
{
    UITextField *_phoneTextField;
    UITextField *_passwordTextField;
    UITextField * _codeTextField;
    NSArray * _labelTextArray;
    
    
    UILabel * _codelabel;
    UIButton * _sendButton;
    NSInteger _time;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation BackPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _time=60;
    [self loadMyView];
}

-(void)loadMyView{
//    _titleArray=[[NSMutableArray alloc]initWithObjects:@"   车主姓名   王小贱",@"   手机号      18937181326",@"   车牌号     浙XSCSD", nil];
    
    _labelTextArray=@[@"手机号",@"验证码",@"密码"];
    UIImageView * imageView=[[UIImageView alloc]initWithFrame:CGRectMake(100*KScaleW, 3*KScaleH, 40*KScaleW, 40*KScaleH)];
    //    [imageView setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:@"头像"]];
    [self.view addSubview:imageView];
//    _tableView.delegate=self;
//    _tableView.dataSource=self;
    
}

-(void)viewWillAppear:(BOOL)animated{
    _tableView.rowHeight=_tableView.frame.size.height/3;
    _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor=[UIColor clearColor];
    
    [_tableView registerNib:[UINib nibWithNibName:@"ForgetPasswordTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ForgetPasswordTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.cellLabel.text=_labelTextArray[indexPath.row];
    cell.cellTextField.placeholder=[NSString stringWithFormat:@"请输入%@",_labelTextArray[indexPath.row]];
    if (indexPath.row==1) {
        cell.cellButton.hidden=NO;
        cell.codeLabel.hidden=NO;
        [cell.cellButton addTarget:self action:@selector(sendCodeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        _codeTextField=cell.cellTextField;
        _sendButton=cell.cellButton;
        _codelabel=cell.codeLabel;
        _codeTextField.keyboardType=UIKeyboardTypeNumberPad;
    }else if (indexPath.row==0){
        _phoneTextField=cell.cellTextField;
        _phoneTextField.keyboardType=UIKeyboardTypeNumberPad;
    }else if (indexPath.row==2){
        _passwordTextField=cell.cellTextField;
        _passwordTextField.secureTextEntry=YES;
        _passwordTextField.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
        
    }
    cell.cellTextField.delegate=self;
    return cell;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    UIImageView * imageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 1, cell.contentView.bounds.size.width, cell.contentView.bounds.size.height-1)];
    imageView.image=[UIImage imageNamed:@"中间栏中小"];
    [cell.contentView addSubview:imageView];
    cell.backgroundColor=[UIColor clearColor];
}
//改密码 发送验证码
-(void)sendCodeButtonAction:(UIButton *)but{
    
    if (_phoneTextField.text.length!=11) {
        UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"请输入正确的手机号" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
        return;
    }
    but.enabled=NO;
    AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
    [manager POST:SendCodeURL parameters:@{@"phone":_phoneTextField.text,@"type":@"2"} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"head"][@"msg"] isEqualToString:@"成功"]) {
            [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
            [_sendButton setTitle:@"" forState:UIControlStateNormal];
        }else{
            but.enabled=YES;
            [self creatAlertViewWithMassage:responseObject[@"head"][@"msg"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        but.enabled=YES;
//        [self creatAlertViewWithMassage:@"请求数据失败"];
        
        
        if (error.code==3840||error.code==-1011) {
            [self creatAleartViewWithMassage:@"请求失败"];
        }else if (error.userInfo[@"NSDebugDescription"]&&[[NSString stringWithFormat:@"%@",error.userInfo[@"NSDebugDescription"]] length]<30) {
            
            
            [self creatAleartViewWithMassage:error.userInfo[@"NSDebugDescription"]];
            
            
            
        }else if(error.userInfo[@"NSLocalizedDescription"]&&[[NSString stringWithFormat:@"%@",error.userInfo[@"NSLocalizedDescription"]] length]>30){
            
            
            [self creatAleartViewWithMassage:error.userInfo[@"NSLocalizedDescription"]];
            
            
            
        }else{
            [self creatAleartViewWithMassage:@"请求数据失败,请稍后重试"];
        }
        
    }];
}
-(void)timerAction:(NSTimer *)timer{
//    [_sendButton setTitle:[NSString stringWithFormat:@"剩余%ld秒",--_time] forState:UIControlStateDisabled];
    _codelabel.text=[NSString stringWithFormat:@"剩余%ld秒",--_time];
    if (_time<0) {
        _sendButton.enabled=YES;
        [_sendButton setTitle:@"发送验证码" forState:UIControlStateNormal];
        _codelabel.text=@"获取验证码";
        [timer invalidate];
        _time=60;
    }
}
-(void)creatAlertViewWithMassage:(NSString *)massage{
    UIAlertView*alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:massage delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView show];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewH/2+30);
    }];
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewH/2);
    }];
}
- (IBAction)backButtonAction:(id)sender {
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewH/2);
    }];
    [self.navigationController popToViewController:self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] animated:NO];
}
- (IBAction)OKButtonAction:(id)sender {
    
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewH/2);
    }];
    if (_phoneTextField.text.length!=11) {
        [self creatAlertViewWithMassage:@"请输入正确的手机号"];
        return;
    }
    if (_codeTextField.text.length!=4) {
        [self creatAlertViewWithMassage:@"请输入正确的验证码"];
        return;
    }
    if (_passwordTextField.text.length<8||_passwordTextField.text.length>20) {
        [self creatAlertViewWithMassage:@"请保持密码位数在8~20内"];
        return;
    }
    
    //phone=%@&newpwd=%@&verificationCode=%@&userId=%@
    //忘记密码
    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
#warning 忘记密码 接口 点击确认
    AFHTTPRequestOperationManager* manager=[AFHTTPRequestOperationManager manager];
    NSDictionary * dic=@{@"phone":_phoneTextField.text,@"newpwd":_passwordTextField.text,@"verificationCode":_codeTextField.text};
    [manager POST:ForgetOKButURL parameters:dic  success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"找回密码responseObject==%@",responseObject);
        if ([responseObject[@"head"][@"msg"] isEqualToString:@"成功"]) {
            [self creatAleartViewWithMassage:@"找回密码成功"];
            [defaults setObject:_phoneTextField.text forKey:@"phone"];
            [defaults setObject:_passwordTextField.text forKey:@"password"];
            [self.navigationController popToRootViewControllerAnimated:NO];
        }else{
            [self creatAleartViewWithMassage:responseObject[@"head"][@"msg"]];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        [self creatAleartViewWithMassage:@"网络错误"];
        if (error.code==3840||error.code==-1011) {
            [self creatAleartViewWithMassage:@"请求失败"];
        }else if (error.userInfo[@"NSDebugDescription"]&&[[NSString stringWithFormat:@"%@",error.userInfo[@"NSDebugDescription"]] length]<30) {
            
            
            [self creatAleartViewWithMassage:error.userInfo[@"NSDebugDescription"]];
            
            
            
        }else if(error.userInfo[@"NSLocalizedDescription"]&&[[NSString stringWithFormat:@"%@",error.userInfo[@"NSLocalizedDescription"]] length]>30){
            
            
            [self creatAleartViewWithMassage:error.userInfo[@"NSLocalizedDescription"]];
            
            
            
        }else{
            [self creatAleartViewWithMassage:@"请求数据失败,请稍后重试"];
        }

    }];
    
    
}
-(void)creatAleartViewWithMassage:(NSString *)massage{
    UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:massage delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ([alertView.message isEqualToString:@"找回密码成功"]) {
        DriverCenterViewController * viewctr=[[DriverCenterViewController alloc]init];
        self.navigationController.viewControllers=@[viewctr];
    }
}
@end
