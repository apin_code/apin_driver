//
//  RegistViewController.h
//  Apin_Driver
//
//  Created by Apin on 15/8/1.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *headImgBtn;

@property(nonatomic,strong)UIImageView *headImgView;
// base 64 字符串
@property(nonatomic,copy)NSString *encodeImgStr;

@end
