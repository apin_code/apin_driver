//
//  RegistViewController.m
//  Apin_Driver
//
//  Created by Apin on 15/8/1.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import "RegistViewController.h"
#import "RegistTwoViewController.h"
#import "Apin_changeHeadImgViewController.h"

#import "LoginViewController.h"

#import "DriverCenterViewController.h"
#define ORIGINAL_MAX_WIDTH 640.0f

@interface RegistViewController ()<UIActionSheetDelegate,UIImagePickerControllerDelegate,Apin_chaImgViewDelegate,UINavigationControllerDelegate,UIAlertViewDelegate>
{
    int _time;
}
@property (weak, nonatomic) IBOutlet UITextField *phoneTextfield;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextfield;
@property (weak, nonatomic) IBOutlet UILabel *codeLabel;
@property (weak, nonatomic) IBOutlet UITextField *codeTextfield;
@property (weak, nonatomic) IBOutlet UIButton *agreeButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
@end

@implementation RegistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
//    _topImageView.image=[[UIImage imageNamed:@"头部边框"]stretchableImageWithLeftCapWidth:20 topCapHeight:93];
    self.navigationController.view.frame=CGRectMake(KBgSmallViewX, KBgSmallViewY, KBgSmallViewW, KBgSmallViewH+12*KScaleH);
    _time=60;
//    _topImageView.layer.cornerRadius=_topImageView.frame.size.width/2;
//    _topImageView.layer.masksToBounds=YES;
    _headImgBtn.layer.cornerRadius=28*KScaleW;
    _headImgBtn.layer.masksToBounds=YES;
    _headImgBtn.layer.cornerRadius=28.5*KScaleH;
    _headImgBtn.layer.masksToBounds=YES;
    [_phoneTextfield setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [_passwordTextfield setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [_codeTextfield setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    
}

//发送验证码
- (IBAction)sendCodeButAction:(id)sender {
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewY+KBgSmallViewH/2);
    }];
    
//    return;
    if (_phoneTextfield.text.length!=11) {
        [self creatAlertViewWithMassage:@"请输入正确的手机号"];
        return;
    }
    _sendButton.enabled=NO;
    AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
    
    [manager POST:SendCodeURL parameters:@{@"phone":_phoneTextfield.text,@"type":@"1"}  success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"head"][@"msg"] isEqualToString:@"成功"]) {
            [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
            
        }else{
            _sendButton.enabled=YES;
            [self creatAlertViewWithMassage:responseObject[@"head"][@"msg"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        _sendButton.enabled=YES;
        NSLog(@"error==%@",error);
//        [self creatAlertViewWithMassage:@"请求数据失败"];
    }];
}
-(void)timerAction:(NSTimer *)timer{
//    [_sendButton setTitle:[NSString stringWithFormat:@"剩余%d秒",--_time] forState:UIControlStateDisabled];
    _codeLabel.text=[NSString stringWithFormat:@"剩余%d秒",--_time];
    if (_time<=0) {
        _sendButton.enabled=YES;
        _codeLabel.text=@"获取验证码";
        [timer invalidate];
        _time=60;
    }
}

//返回按钮
- (IBAction)backButtonAction:(id)sender {
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewY+KBgSmallViewH/2);
    }];
    [self.navigationController popToRootViewControllerAnimated:NO];
    
}
#pragma mark textFieldDelegate代理方法
//屏幕上移
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    //    [self.view endEditing:NO];
    
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewH/2);
    }];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewY+KBgSmallViewH/2);
    }];
    return YES;
}

- (IBAction)nextPageAction:(id)sender {
    
    if (_phoneTextfield.text.length!=11) {
        [self creatAlertViewWithMassage:@"手机号错误"];
        return;
    }else if (_passwordTextfield.text.length<8||_passwordTextfield.text.length>20){
        [self creatAlertViewWithMassage:@"密码位数错误"];
        return;
    }else if (_codeTextfield.text.length!=4){
        [self creatAlertViewWithMassage:@"验证码输入有误"];
        return;
    }
    
//    RegistTwoViewController * twoCtr=[[RegistTwoViewController alloc]init];
//    twoCtr.imageDic=[[NSMutableDictionary alloc]init];
//    [self.navigationController pushViewController:twoCtr animated:NO];
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewY+KBgSmallViewH/2);
    }];
    if (self.encodeImgStr==nil) {
        [self creatAlertViewWithMassage:@"请上传头像"];
        return;
    }
    
    
    AFHTTPRequestOperationManager * manager=[AFHTTPRequestOperationManager manager];
    if (manager==nil) {
        return;
    }
    UIButton * but=(UIButton *)sender;
    but.enabled=NO;
    NSDictionary * dic=@{@"phone":_phoneTextfield.text,  @"password":_passwordTextfield.text,  @"verificationCode":_codeTextfield.text,  @"fdata":self.encodeImgStr,  @"fileType":@"1"};
//    NSLog(@"RegistURL %@",[NSString stringWithFormat:RegistURL,_phoneTextfield.text,_passwordTextfield.text,_codeTextfield.text,self.encodeImgStr]);
    [manager POST:RegistURL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@  %@",[self class],responseObject);
        if ([responseObject[@"head"][@"msg"] isEqualToString:@"成功"]) {
            [self creatAlertViewWithMassage:@"注册成功"];
            but.enabled=YES;
            AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            delegate.userID=responseObject[@"body"][@"userId"];
            [[NSUserDefaults standardUserDefaults] setObject:delegate.userID forKey:@"userId"];
            [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"body"][@"filePath"] forKey:@"headpic"];
            
//            ApinDriverMainViewController * mainCtr=(ApinDriverMainViewController  * )delegate.window.rootViewController;
//            mainCtr.loginOK=YES;
            
        }else{
            [self creatAlertViewWithMassage:responseObject[@"head"][@"msg"]];
            but.enabled=YES;
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@  %@",[self class],error);
        but.enabled=YES;
        if (error.userInfo[@"NSLocalizedDescription"]) {
//            [self creatAlertViewWithMassage:error.userInfo[@"NSLocalizedDescription"]];
            [self creatAlertViewWithMassage:@"连接失败,请检查网络连接"];
        }else{
            [self creatAlertViewWithMassage:@"上传失败"];
        }
        
    }];
}
-(void)creatAlertViewWithMassage:(NSString *)massage{
    UIAlertView*alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:massage delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView show];
}


- (IBAction)upHeadImageButtonAction:(id)sender {
    [UIView animateWithDuration:0.2 animations:^{
        self.navigationController.view.center=CGPointMake(KBgSmallViewX+KBgSmallViewW/2, KBgSmallViewY+KBgSmallViewH/2);
    }];
    
    UIActionSheet *choiceSheet=[[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"从相册中选取", nil];
    [choiceSheet showInView:[UIApplication sharedApplication].keyWindow];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        self.navigationController.view.frame=CGRectMake(0, 0, KScaleW, KScaleH);
        // 拍照
        if ([self isCameraAvailable] && [self doesCameraSupportTakingPhotos] ) {
            UIImagePickerController *controller=[[UIImagePickerController alloc]init];
            controller.sourceType=UIImagePickerControllerSourceTypeCamera;
            NSMutableArray *mediaTypes=[[NSMutableArray alloc]init];
            [mediaTypes addObject:(NSString *)kUTTypeImage];
            controller.mediaTypes=mediaTypes;
            controller.delegate=self;
            [self presentViewController:controller animated:YES completion:^{
                
            }];
        }
    } else if (buttonIndex ==1 ){
        self.navigationController.view.frame=CGRectMake(0, 0, KScaleW, KScaleH);
        // 从相册中选取
        if ([self isPhotoLibraryAvailable]) {
            UIImagePickerController *controller=[[UIImagePickerController alloc]init];
            controller.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
            NSMutableArray *mediaTypes=[[NSMutableArray alloc]init];
            [mediaTypes addObject:(NSString *)kUTTypeImage];
            controller.mediaTypes=mediaTypes;
            controller.delegate=self;
            
            
            [self presentViewController:controller animated:YES completion:^{
                
            }];
        }
    }
}

#pragma mark - 摄像头的一些设置 ,这个是仿大神写的
-(BOOL) isCameraAvailable{
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}
-(BOOL)doesCameraSupportTakingPhotos{
    return [self cameraSupportsMedia:(NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypeCamera];
}
/*
 获取用户拍摄的是照片 还是视频
 NSString *mediaType=[info objectForKey:UIImagePickerControllerMediaType];
 判断 获取类型: 照片,并且是刚拍摄的照片
 if ([mediaType isEqualToString:(NSString *)kUTTypeImage]&&picker.sourceType == UIImagePickerControllerSourceTypeCamera)
 */
-(BOOL) cameraSupportsMedia:(NSString *)paramMediaType sourceType:(UIImagePickerControllerSourceType)paramSourceType{
    __block BOOL result=NO;
    if ([paramMediaType length]==0) {
        return NO;
    }
    NSArray *availableMediaTypes=[UIImagePickerController availableMediaTypesForSourceType:paramSourceType];
    [availableMediaTypes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *mediaType=(NSString *)obj;
        if ([mediaType isEqualToString:paramMediaType]) {
            result =YES;
            *stop=YES;
        }
    }];
    return  result;
}
-(BOOL) isPhotoLibraryAvailable{
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
}
#pragma mark- 图片尺寸
-(UIImage *)imageByScalingToMaxSize:(UIImage *)sourceImage{
    if (sourceImage.size.width<ORIGINAL_MAX_WIDTH) {
        return sourceImage;
    }
    CGFloat btWidth=0.0f;
    CGFloat btHeight=0.0f;
    if (sourceImage.size.width>sourceImage.size.height) {
        btHeight=ORIGINAL_MAX_WIDTH;
        btWidth=sourceImage.size.width *(ORIGINAL_MAX_WIDTH/sourceImage.size.height);
    }else {
        btWidth=ORIGINAL_MAX_WIDTH;
        btHeight=sourceImage.size.height * (ORIGINAL_MAX_WIDTH / sourceImage.size.width);
    }
    CGSize targetSize=CGSizeMake(btWidth, btHeight);
    return [self imageByScalingAndCroppingForSourceImage:sourceImage targetSize:targetSize];
}

-(UIImage *)imageByScalingAndCroppingForSourceImage:(UIImage *)sourceImage targetSize:(CGSize)targetSize{
    UIImage *newImage=nil;
    CGSize imageSize=sourceImage.size;
    CGFloat width=imageSize.width;
    CGFloat height=imageSize.height;
    CGFloat targetWidth=targetSize.width;
    CGFloat targetHeight=targetSize.height;
    CGFloat scaleFactor=0.0;
    CGFloat scaledWidth=targetWidth;
    CGFloat scaledHeight=targetHeight;
    CGPoint thumbnailPoint=CGPointMake(0.0, 0.0);
    if (CGSizeEqualToSize(imageSize, targetSize)==NO) {
        CGFloat widthFactor=targetWidth / width;
        CGFloat heightFactor= targetHeight /height;
        
        if (widthFactor > heightFactor) {
            scaleFactor = widthFactor;
        }else{
            scaleFactor = heightFactor;
            scaledWidth = width * scaleFactor;
            scaledHeight = height * scaleFactor;
        }
        // center the image
        if (widthFactor > heightFactor) {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }else if ( widthFactor < heightFactor){
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    UIGraphicsBeginImageContext(targetSize);  // this will crop
    CGRect thumbnailRect= CGRectZero ;
    thumbnailRect.origin=thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil) NSLog(@"could not scale image");
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
    
    
}
#pragma  mark - ZLPImageCropperDelegate
-(void)imgTaByPh:(Apin_changeHeadImgViewController *)phViewController didFinished:(UIImage *)editdImage{
    [_headImgBtn setBackgroundImage:editdImage forState:UIControlStateNormal];
//    [_headImgBtn setBackgroundImage:editdImage forState:UIControlStateNormal];
    // 转换成 base64 编码
    NSData * _data=UIImageJPEGRepresentation(_headImgBtn.currentBackgroundImage, 1.0f);
    NSString *encodedImagStr=[_data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
//    NSLog(@"encodedImagStrencodedImagStr%@",encodedImagStr);
    
    
    self.encodeImgStr=encodedImagStr;
    [phViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
}
-(void)imgTaByPhDidCancel:(Apin_changeHeadImgViewController *)TaByPhViewController{
    [TaByPhViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
}


#pragma mark- UIImagePickerController

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    NSLog(@"成功 : %@",info);
    
    [picker dismissViewControllerAnimated:YES completion:^{
        self.view.frame=CGRectMake(0, 0, KScreenW, KScreenH);
        UIImage *portraitImg=[info objectForKey:UIImagePickerControllerOriginalImage];
        portraitImg=[self imageByScalingToMaxSize:portraitImg];
        
        // 剪裁
        Apin_changeHeadImgViewController * imgEditoVC=[[Apin_changeHeadImgViewController alloc]initWithImage:portraitImg cropFrame:CGRectMake(0, 100.0f, self.view.frame.size.width, self.view.frame.size.width) limitScaleRatio:3.0];
        imgEditoVC.delegate=self;
        [self presentViewController:imgEditoVC animated:YES completion:^{
        }];
    }];
    
    //    [picker dismissViewControllerAnimated:YES completion:^{
    //        // 获取用户拍摄的是照片 还是视频
    //        NSString *mediaType=[info objectForKey:UIImagePickerControllerMediaType];
    //        // 判断 获取类型: 照片,并且是刚拍摄的照片
    //        if ([mediaType isEqualToString:(NSString *)kUTTypeImage]&&picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    //        {
    //            // 判断是否允许修改
    //          _theImage=nil;
    //            if ([picker allowsEditing]) {
    //                // 获取用户编辑之后的照片
    //                _theImage=[info objectForKey:UIImagePickerControllerEditedImage];
    //            }else {
    //                // 获取原始照片
    //                _theImage =[info objectForKey:UIImagePickerControllerOriginalImage];
    //            }
    //            //保存照片到相册中
    //            UIImageWriteToSavedPhotosAlbum(_theImage, self, nil, nil);
    //        }
    //        ZLPImgTaByPhoneViewController *imgEditoVC=[[ZLPImgTaByPhoneViewController alloc]initWithImage:_theImage cropFrame:CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.width) limitScaleRatio:3.0];
    //        imgEditoVC.delegate=self;
    //    [self presentViewController:imgEditoVC animated:YES completion:^{
    //
    //    }];
    //
    //    }];
    
    
}

//
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    //用户取消拍摄
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ([alertView.message isEqualToString:@"注册成功"]) {
        LoginViewController * loginCtr=[[LoginViewController alloc]init];
        
        self.navigationController.viewControllers=@[loginCtr];
    }
}
@end
