//
//  ApinDriverHeaderPaths.h
//  Apin_Driver
//
//  Created by Apin on 15/8/1.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#ifndef Apin_Driver_ApinDriverHeaderPaths_h
#define Apin_Driver_ApinDriverHeaderPaths_h

#import "AppDelegate.h"
#import "AFNetworking.h"
#import "MJRefresh.h"
#import "UIImageView+WebCache.h"
#import "Reachability.h"
#import "ApinDriverMainViewController.h"
#import "ApinXGPush.h"
//信鸽
#import "XGPush.h"
#import "XGSetting.h"
//支付宝
/*
 *商户的唯一的parnter和seller。
 *签约后，支付宝会为每个商户分配一个唯一的 parnter 和 seller。
 */
#define PartnerID @"2088911723363342"
#define SellerID  @"ali@hzapin.com"

// 商户私钥
#define PartnerPrivKey @"MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAKjZ3VFRHA+yjrzHzt4shdMIbN/hJMfpwUUHG0JeB5M7hnUKkZNbaaosHe7BASD7KKqo0tu+/oel3RSG2HvAdJP8hlf7A3mrejlcdDyJ1MfjSk522iNgqdeVRC8hmz2CJTmGhI1Uf24ul2Ik2B8omzC5etDxE7fHk8N7T/siVQwhAgMBAAECgYEAmBVrj3Oj98uXXxgFDv5BJzw+fEQY0euMxfTJq7Rv1EBYMhFr37MFcF4+M68pqiAul9Bp472bGmjdwUBXutc06CLmwgNLOfM2D4UtKuBOHcOkWovxjD72oEzZ5fjTvnchE6Z6eLFdF4MnnZA/QjDY3iNZVKDqZbgKiq05v1Ytd5UCQQDcvPAuwvNg6S4ASQhXtYzEk/gYFSzoto2Bgxv5IdoIX621oGBEBkEz5zrioAH5ZIoCvSvoyDhVabigQPKWTxHfAkEAw9MDD64tLP5Y4nKdP+eQFHa5P8YEC4Z454fMfqCAWdmfVXAfA3+YQ2HAwxo+hFqy/q7LncXnGETS3RYm7Uuh/wJBAL6q0QiDSri+EgcbDoK8hjBS4+zhdZ45+XcSafpDXFL5WlHVivGXOYD8zadtsObcCj4day0yrT5SCLEcCK7+858CQHKq1VgRPhLivVmck8gf6JaTDtbxNN0q2RF6y5w6j+4NKOf2x7JcvOR4EVAtxf3qvnW0FTdF85SaeH9NwW2ipDECQFmXDKoZchBJ23h/KpUgaq5ccLWTmC58KglXZNejKXJvdyh/VhxlvmLGWaGvS27gcd79axOjdJRW8AqfitA0Ufw="
////客户端  信鸽
//#define ACCESSID 2200139074
//#define ACCESSKEY @"I86X4XN14WMN"
//#define SECRETKEY @"71dc091e7e9cd5c71bf2300d4301e95b"

//司机端 信鸽
#define ACCESSID 2200148332//2200139183
#define ACCESSKEY @"I5MCX6Q66Z6N"//@"IA5Z96KA3U9Q"
//#define SECRETKEY @"a8ef3a37a49c21a9ef9ef31b38927198"
//司机端 在线安装 
#define ACCESSIDDISCT 2200139183//2200148332
#define ACCESSKEYDISCT @"IA5Z96KA3U9Q"//@"I5MCX6Q66Z6N"

#define KScreenH  ([[UIScreen mainScreen] bounds].size.height)
#define KScreenW  ([[UIScreen mainScreen] bounds].size.width)
#define KScreen   [[UIScreen mainScreen] bounds]

#define KScaleH  (KScreenH/568)
#define KScaleW  (KScreenW/320)

#define KMapH (KScreenH/667)
#define KMapW (KScreenW/375)

#define KM

#define KBgSmallViewX (KScreenW/8)
#define KBgSmallViewY  130*KScaleH
#define KBgSmallViewW (KScreenW/8*6)
#define KBgSmallViewH  308*KScaleH //

#define KArrowAndLabelX 0
#define KArrowAndLabelY 0
#define KArrowAndLabelW KBgSmallViewW
#define KArrowAndLabelH  (KBgSmallViewH/5)

#define KButtonColor   [UIColor colorWithRed:116/255.0 green:163/255.0 blue:196/255.0 alpha:0.8];

#define KUnusableColor [UIColor colorWithRed:163/255.0 green:162/255.0 blue:162/255.0 alpha:0.8];

#define KCancelButtonColor [UIColor colorWithRed:95/255.0 green:129/255.0 blue:153/255.0 alpha:0.8];

#define RGBA(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define PLACEHOLDERCOLOR RGBA(170,170,170,1.0)
#define PLACEHOLDERFONT  [UIFont boldSystemFontOfSize:12*(KScaleH)]
#define COLOROFKEYPATH  @"_placeholderLabel.textColor"
#define FONTOFKEYPATH @"_placeholderLabel.font"

#define UnfinishedColor [UIColor colorWithRed:0/255.0f green:214/255.0f blue:252/255.0f alpha:1]
#define InvalidColor [UIColor colorWithRed:82/255.0f green:85/255.0f blue:87/255.0f alpha:1]
#define BlueColor [UIColor colorWithRed:23/255.0 green:177/255.0 blue:243/255.0 alpha:1]
//地址
//#define MainURL @"http://192.168.1.51:8083/apin-back-interface-driver/"   

//#define MainURL @"http:121.40.19.75:8083/apin-back-interface-driver/"
#define MainURL @"http://120.26.85.57:8083/apin-back-interface-driver/"

//检查版本更新
#define CheakURL  @"http:121.40.19.75:8080/util/app/version/check" //?osType=4&channel_id=00000&app_version=1.0.0&package=com.android.apin //channel_id=00000   app_version 版本  package boundleID


//登录?phone=%@&password=%@
#define LoginURL  MainURL@"api/apin/user/login"
//注册
#define RegistURL MainURL@"api/apin/user/register"//手机号  密码 验证码 base64字符串?phone=%@&password=%@&verificationCode=%@&fdata=%@&fileType=1

//忘记密码 ?phone=%@&newpwd=%@&verificationCode=%@&userId=%@
#define ForgetOKButURL MainURL@"api/apin/user/pwd/forget"

//更新车牌号?userId=%@&carPlate=%@&driverName=%@
#define CarPlateURL MainURL@"api/apin/user/carPlate/update"
//#define CarPlate2URL MainURL@"api/apin/user/carPlate/update"
//上传图片
#define UPLOADURL MainURL@"api/apin/user/file/upload"//fdata file?data    filetype:string   ?fdata=%@&fileType=%d&userId=%@

//图片预览
#define ImageLookURL MainURL@"api/apin/user/file/upload?filePath=%@"

//判断认证是否成功  提交认证??
#define CentifiedURL MainURL@"api/apin/user/identity/certified?userId=%@"


//获取认证结果
#define GetCentifiedResultURL MainURL@"api/apin/user/identification/get" //driverId=%@  0：待审核, 1:审核通过，2：审核不通过

//获取乘车路线
#define GetCarLinesURL MainURL@"api/apin/user/rideRoute/get"
//#define GetCarLinesURL MainURL@"api/apin/user/rideRoute/get?userId=%@&street=%@&longitude=%@&latitude=%@&pageNum=%@&pageSize=%@"
// 停止接单
#define StopService MainURL@"api/apin/user/service/stop?userId=%@"
//开始接单
#define StartService MainURL@"api/apin/user/service/start?userId=%@"

//抢单
#define RideRouteURL MainURL@"api/apin/user/rideRoute/rob"
//#define RideRouteURL MainURL@"api/apin/user/rideRoute/rob?userId=%@&routeId=%@"//司机id  路线id

//开启行程
#define StartRouteURL MainURL@"api/apin/user/rideRoute/start"

//查看行程
#define LOOKRouteURL MainURL@"api/apin/user/allRideRoute/view"
#define UserIdKey @"userId"
#define RoutePageNum @"pageNum"
#define RoutePageSize @"pageSize"
#define RouteType @"routeType"


//我的行程
#define MyRoute MainURL@"api/apin/user/currentRideRoute/get?"//userId=%@

//结束行程
#define EndTheRoute MainURL@"api/apin/user/route/end"


//查看行程详情
#define LOOKRouteInfoURL MainURL@"api/apin/user/rideRoute/detail/view?routeId=%@"//

//我的收入
#define IncomeURL MainURL@"api/apin/user/income/view"//?userId=%@
//

//意见反馈
#define AdviseURL MainURL@"api/apin/user/advise"
//#define AdviseURL MainURL@"api/apin/user/advise?userId=%@&content=%@"


// 申请换车
#define  SJApplyChangeCar (MainURL@"api/apin/user/changeCar/apply?userId=%@&routeId=%@") // 司机id  行程id

// 收款方式
#define SJWayOfPay  (MainURL@"api/apin/user/payment/get?userId=%@")

// 绑定账号
#define SJBoundCount (MainURL@"api/apin/user/account/bind") // 账号类别  账号 司机Id  ?accountType=%@&accountNum=%@&userId=%@

// 个人中心
#define SJPersonCenter (MainURL@"api/apin/user/basicInfo/view") // 司机Id ?userId=%@

//发送验证码
#define SendCodeURL MainURL@"api/apin/user/verifyCode/get?"//phone=%@



#define SENDBUNDELIDURL MainURL@"/user/redis/ios/setIdentifier"//?identifier=%@  type=(client driver)    



#define LOGINOUTURL MainURL@"api/apin/user/loginOut"//phone token
////忘记密码
//#define ForgetPasswordURL MainURL@"api/apin/user/pwd/forget?phone=%@&newpwd=%@&verificationCode=%@&userId=%@"//电话  新密码 验证码 司机Id


////传递boundleID  type --类型(client/driver)  userId 用户ID identifier 标识符
//#define SetIdentifierURL (MainURL@"/user/redis/ios/setIdentifier")

#define UpdataAddressURL  @"http:121.40.19.75:8080/user/location/set"//type  clientLocation,//乘客位置
//driverLocation,//司机位置
//id    longitude   --经度  latitude    --纬度    


#endif
