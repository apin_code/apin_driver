//
//  AppDelegate.h
//  Apin_Driver
//
//  Created by Apin on 15/8/1.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
//#import <BaiduMapAPI/BMapKit.h>
//#import <BaiduMapAPI/BMKMapView.h>
#import "BMapKit.h"
#import "PersonModel.h"

#import <CoreLocation/CoreLocation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>
{
    BMKMapManager *_mapManager;
    CLLocationManager *_locationManager;
}
// 请求路线的 3个参数
@property (nonatomic,copy) NSString *locaStreetStr;
@property (nonatomic,copy) NSString *locaLonStr;
@property (nonatomic,copy) NSString *localLatStr;


@property (copy,nonatomic) NSString * tokenStr;
@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,strong)PersonModel*personmodel;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property(nonatomic,strong)NSString * userID;

@property(nonatomic,copy)void(^backBlock)(UIViewController*ctr);

@property(nonatomic,assign)BOOL isLogin;
@property(nonatomic,strong)UIView * loadURLView;
@property(nonatomic,strong)UIView *middleURLView;
@property(nonatomic,strong)UIView *bigLoadURLView;

+ (void)creatMessageLabelWithMessage:(NSString *)message;
- (void)startMessageLabel:(NSString *)message;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (void)registerPushForIOS8;
- (void)registerPush;
@end

