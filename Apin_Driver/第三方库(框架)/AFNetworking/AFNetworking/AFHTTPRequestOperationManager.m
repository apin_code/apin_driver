// AFHTTPRequestOperationManager.m
//
// Copyright (c) 2013-2014 AFNetworking (http://afnetworking.com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <Foundation/Foundation.h>

#import "NSData+Encryption.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFHTTPRequestOperation.h"

#import <Availability.h>
#import <Security/Security.h>

#if defined(__IPHONE_OS_VERSION_MIN_REQUIRED)
#import <UIKit/UIKit.h>
#endif

@interface AFHTTPRequestOperationManager ()
@property (readwrite, nonatomic, strong) NSURL *baseURL;
@end

@implementation AFHTTPRequestOperationManager

+ (instancetype)manager {
    AFHTTPRequestOperationManager * manager=[[self alloc] initWithBaseURL:nil];
    //    manager.requestSerializer=[AFHTTPRequestSerializer serializer];
    //    manager.responseSerializer=[AFHTTPResponseSerializer serializer];
    
    //    UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"无网络连接!请检查网络" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
    //    [alertView show];
    
    __block BOOL isConnected=YES;
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        if (status<=0) {
            NSLog(@"无连接");
            UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"温馨提示" message:@"无网络连接" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
            [alertView show];
            isConnected=NO;
            [AppDelegate creatMessageLabelWithMessage:@"当前网络连接异常"];
        }else if (status>0){
            NSLog(@"连接");
        }
        NSLog(@"网络状态%@",AFStringFromNetworkReachabilityStatus(status));
        
    }];
    
    if (isConnected==NO) {
        return nil;
    }
    manager.requestSerializer.timeoutInterval=60;
    AppDelegate * delegate=(AppDelegate *)[UIApplication sharedApplication].delegate;
    if (delegate.loadURLView!=nil) {
        delegate.loadURLView.hidden=NO;
        //        delegate.window.userInteractionEnabled=NO;
    }
    return manager;
}

- (instancetype)init {
    return [self initWithBaseURL:nil];
}

- (instancetype)initWithBaseURL:(NSURL *)url {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    // Ensure terminal slash for baseURL path, so that NSURL +URLWithString:relativeToURL: works as expected
    if ([[url path] length] > 0 && ![[url absoluteString] hasSuffix:@"/"]) {
        url = [url URLByAppendingPathComponent:@""];
    }
    
    self.baseURL = url;
    
    self.requestSerializer = [AFHTTPRequestSerializer serializer];
    self.responseSerializer = [AFJSONResponseSerializer serializer];
    
    self.securityPolicy = [AFSecurityPolicy defaultPolicy];
    
    self.reachabilityManager = [AFNetworkReachabilityManager sharedManager];
    
    self.operationQueue = [[NSOperationQueue alloc] init];
    
    self.shouldUseCredentialStorage = YES;
    
    return self;
}

#pragma mark -

#ifdef _SYSTEMCONFIGURATION_H
#endif

- (void)setRequestSerializer:(AFHTTPRequestSerializer <AFURLRequestSerialization> *)requestSerializer {
    NSParameterAssert(requestSerializer);
    
    _requestSerializer = requestSerializer;
}

- (void)setResponseSerializer:(AFHTTPResponseSerializer <AFURLResponseSerialization> *)responseSerializer {
    NSParameterAssert(responseSerializer);
    
    _responseSerializer = responseSerializer;
}

#pragma mark -

- (AFHTTPRequestOperation *)HTTPRequestOperationWithRequest:(NSURLRequest *)request
                                                    success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                                    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = self.responseSerializer;
    operation.shouldUseCredentialStorage = self.shouldUseCredentialStorage;
    operation.credential = self.credential;
    operation.securityPolicy = self.securityPolicy;
    
    [operation setCompletionBlockWithSuccess:success failure:failure];
    operation.completionQueue = self.completionQueue;
    operation.completionGroup = self.completionGroup;
    
    return operation;
}

#pragma mark -

- (AFHTTPRequestOperation *)GET:(NSString *)URLString
                     parameters:(id)parameters
                        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    //get 请求改成post请求
    //    return [self POST:URLString parameters:parameters success:success failure:failure];
    
    //第三方库 改动过
    NSMutableDictionary * dic=[NSMutableDictionary dictionaryWithDictionary:parameters];
    NSMutableString * url=[NSMutableString stringWithString:URLString];
    AppDelegate * delegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
    
    NSString * str=nil;
    if (delegate.tokenStr==nil) {
        str=@"123456";
    }else{
        str=delegate.tokenStr;
    }
    if (parameters==nil) {
        [url appendString:[NSString stringWithFormat:@"&osType=4&token=%@",str]];
        if ([defaults objectForKey:@"userId"]) {
            NSData * data=[[defaults objectForKey:@"userId"] dataUsingEncoding:NSUTF8StringEncoding];
            
            [url appendString:[NSString stringWithFormat:@"&auth_driver_id=%@",[data AES256ParmEncryptWithKey]]];
        }
        //        NSLog(@"我在这里在这里 == %@",url);
        
    }else{
        // NSString *str =[dic objectForKey:@""];
        //        NSLog(@"wo zai zhe li  zai zheli  =%@",dic);
        [dic  setObject:@"4" forKey:@"osType"];
        [dic  setObject:str forKey:@"token"];
        if ([defaults objectForKey:@"userId"]) {
            NSData * data=[[defaults objectForKey:@"userId"] dataUsingEncoding:NSUTF8StringEncoding];
            [dic setObject:[data AES256ParmEncryptWithKey] forKey:@"auth_driver_id"];
        }
    }
    NSLog(@"%@ url%@",[self class],url);
    NSLog(@"%@  dic%@",[self class],dic);
    
    //    AppDelegate * delegate=[UIApplication sharedApplication].delegate;
    
    delegate.loadURLView.hidden=NO;
    
    
    NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"GET" URLString:[[NSURL URLWithString:url relativeToURL:self.baseURL] absoluteString] parameters:dic error:nil];
    AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:success failure:failure];
    
    [self.operationQueue addOperation:operation];
    
    return operation;
}

- (AFHTTPRequestOperation *)HEAD:(NSString *)URLString
                      parameters:(id)parameters
                         success:(void (^)(AFHTTPRequestOperation *operation))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"HEAD" URLString:[[NSURL URLWithString:URLString relativeToURL:self.baseURL] absoluteString] parameters:parameters error:nil];
    AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *requestOperation, __unused id responseObject) {
        if (success) {
            success(requestOperation);
        }
    } failure:failure];
    
    [self.operationQueue addOperation:operation];
    
    return operation;
}

- (AFHTTPRequestOperation *)POST:(NSString *)URLString
                      parameters:(id)parameters
                         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    //第三方库 改动过
    NSMutableDictionary * dic=[NSMutableDictionary dictionaryWithDictionary:parameters];
    NSMutableString * url=[NSMutableString stringWithString:URLString];
    AppDelegate * delegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
    
    NSString * str=nil;
    if (delegate.tokenStr==nil) {
        str=@"123456";
    }else{
        str=delegate.tokenStr;
    }
    if (parameters==nil) {
        [url appendString:[NSString stringWithFormat:@"&osType=4&token=%@",str]];
        if ([defaults objectForKey:@"userId"]) {
            NSData * data=[[defaults objectForKey:@"userId"] dataUsingEncoding:NSUTF8StringEncoding];
            
            [url appendString:[NSString stringWithFormat:@"&auth_driver_id=%@",[data AES256ParmEncryptWithKey]]];
        }
        //        NSLog(@"我在这里在这里 == %@",url);
        
    }else{
        // NSString *str =[dic objectForKey:@""];
        //        NSLog(@"wo zai zhe li  zai zheli  =%@",dic);
        [dic  setObject:@"4" forKey:@"osType"];
        [dic  setObject:str forKey:@"token"];
        if ([defaults objectForKey:@"userId"]) {
            NSLog(@"%@",[defaults objectForKey:@"userId"]);
            NSData * data=[[NSString stringWithFormat:@"%@",[defaults objectForKey:@"userId"]] dataUsingEncoding:NSUTF8StringEncoding];
            [dic setObject:[data AES256ParmEncryptWithKey] forKey:@"auth_driver_id"];
        }
    }
    NSLog(@"url====%@",url);
    NSLog(@"请求的dic==%@",dic);
    delegate.loadURLView.hidden=NO;
    NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"POST" URLString:[[NSURL URLWithString:url relativeToURL:self.baseURL] absoluteString] parameters:dic error:nil];
    AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:success failure:failure];
    
    [self.operationQueue addOperation:operation];
    
    return operation;
}

- (AFHTTPRequestOperation *)POST:(NSString *)URLString
                      parameters:(id)parameters
       constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    
    //第三方库 改动过
    NSMutableDictionary * dic=[NSMutableDictionary dictionaryWithDictionary:parameters];
    NSMutableString * url=[NSMutableString stringWithString:URLString];
    AppDelegate * delegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
    
    NSString * str=nil;
    if (delegate.tokenStr==nil) {
        str=@"123456";
    }else{
        str=delegate.tokenStr;
    }
    if (parameters==nil) {
        [url appendString:[NSString stringWithFormat:@"&osType=4&token=%@",str]];
        if ([defaults objectForKey:@"userId"]) {
            NSData * data=[[defaults objectForKey:@"userId"] dataUsingEncoding:NSUTF8StringEncoding];
            
            [url appendString:[NSString stringWithFormat:@"&auth_driver_id=%@",[data AES256ParmEncryptWithKey]]];
        }
        //        NSLog(@"我在这里在这里 == %@",url);
        
    }else{
        // NSString *str =[dic objectForKey:@""];
        //        NSLog(@"wo zai zhe li  zai zheli  =%@",dic);
        [dic  setObject:@"4" forKey:@"osType"];
        [dic  setObject:str forKey:@"token"];
        
        if ([defaults objectForKey:@"userId"]) {
            NSData * data=[[defaults objectForKey:@"userId"] dataUsingEncoding:NSUTF8StringEncoding];
            [dic setObject:[data AES256ParmEncryptWithKey] forKey:@"auth_driver_id"];
        }
    }
//    NSLog(@"请求头 %@",url);
//    NSLog(@"请求字段 %@",dic);
    delegate.loadURLView.hidden=NO;
    NSMutableURLRequest *request = [self.requestSerializer multipartFormRequestWithMethod:@"POST" URLString:[[NSURL URLWithString:url relativeToURL:self.baseURL] absoluteString] parameters:dic constructingBodyWithBlock:block error:nil];
    AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:success failure:failure];
    
    [self.operationQueue addOperation:operation];
    
    return operation;
}

- (AFHTTPRequestOperation *)PUT:(NSString *)URLString
                     parameters:(id)parameters
                        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"PUT" URLString:[[NSURL URLWithString:URLString relativeToURL:self.baseURL] absoluteString] parameters:parameters error:nil];
    AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:success failure:failure];
    
    [self.operationQueue addOperation:operation];
    
    return operation;
}

- (AFHTTPRequestOperation *)PATCH:(NSString *)URLString
                       parameters:(id)parameters
                          success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                          failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"PATCH" URLString:[[NSURL URLWithString:URLString relativeToURL:self.baseURL] absoluteString] parameters:parameters error:nil];
    AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:success failure:failure];
    
    [self.operationQueue addOperation:operation];
    
    return operation;
}

- (AFHTTPRequestOperation *)DELETE:(NSString *)URLString
                        parameters:(id)parameters
                           success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                           failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"DELETE" URLString:[[NSURL URLWithString:URLString relativeToURL:self.baseURL] absoluteString] parameters:parameters error:nil];
    AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:success failure:failure];
    
    [self.operationQueue addOperation:operation];
    
    return operation;
}

#pragma mark - NSObject

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@: %p, baseURL: %@, operationQueue: %@>", NSStringFromClass([self class]), self, [self.baseURL absoluteString], self.operationQueue];
}

#pragma mark - NSSecureCoding

+ (BOOL)supportsSecureCoding {
    return YES;
}

- (id)initWithCoder:(NSCoder *)decoder {
    NSURL *baseURL = [decoder decodeObjectForKey:NSStringFromSelector(@selector(baseURL))];
    
    self = [self initWithBaseURL:baseURL];
    if (!self) {
        return nil;
    }
    
    self.requestSerializer = [decoder decodeObjectOfClass:[AFHTTPRequestSerializer class] forKey:NSStringFromSelector(@selector(requestSerializer))];
    self.responseSerializer = [decoder decodeObjectOfClass:[AFHTTPResponseSerializer class] forKey:NSStringFromSelector(@selector(responseSerializer))];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.baseURL forKey:NSStringFromSelector(@selector(baseURL))];
    [coder encodeObject:self.requestSerializer forKey:NSStringFromSelector(@selector(requestSerializer))];
    [coder encodeObject:self.responseSerializer forKey:NSStringFromSelector(@selector(responseSerializer))];
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    AFHTTPRequestOperationManager *HTTPClient = [[[self class] allocWithZone:zone] initWithBaseURL:self.baseURL];
    
    HTTPClient.requestSerializer = [self.requestSerializer copyWithZone:zone];
    HTTPClient.responseSerializer = [self.responseSerializer copyWithZone:zone];
    
    return HTTPClient;
}

@end
