//
//  NSData+Encryption.m
//  AESCodeTest
//
//  Created by Apin on 15/9/2.
//  Copyright (c) 2015年 ios. All rights reserved.
//

#import "NSData+Encryption.h"
#import <CommonCrypto/CommonCryptor.h>

static char base64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

@implementation NSData (Encryption)

-(NSString *)AES256ParmEncryptWithKey{
    NSString * key=@"[u\\8a1w4=ag;'\"\\i";
    char keyPtr[kCCKeySizeAES256+1];
    bzero(keyPtr, sizeof(keyPtr));
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    NSUInteger dataLength = [self length];
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);
    size_t numBytesEncrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmAES128,
                                          kCCOptionPKCS7Padding | kCCOptionECBMode,
                                          keyPtr, kCCBlockSizeAES128,
                                          NULL,
                                          [self bytes], dataLength,
                                          buffer, bufferSize,
                                          &numBytesEncrypted);
    if (cryptStatus == kCCSuccess) {
        return [self changeWithData:[NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted]];
    }
    free(buffer);
    return nil;
    
}

-(NSString *)changeWithData:(NSData *)cipher{
     NSString * str=[NSString stringWithFormat:@"%@",cipher];
//    NSLog(@"str = %@",str);
    NSMutableString *mutalStr = [[NSMutableString alloc]initWithString:str];
//    NSLog(@"可变字符的length == %ld",mutalStr.length);
    NSRange range =NSMakeRange(1, mutalStr.length-2);
    NSString *str3 = [mutalStr substringWithRange:range];
//    NSLog(@"去掉后 %@",str3);
    
    NSArray *array = [str3 componentsSeparatedByString:@" "];
//    NSLog(@"array == %@",array);
    
    NSMutableString *str4 = [NSMutableString string];
    for (NSMutableString *muStr in array) {
        
        
//        NSLog(@"muStr == %@",muStr.uppercaseString);
        [str4 appendString:muStr.uppercaseString];
    }
//    NSLog(@"大写的str4 == %@",str4);
    return str4;
}


- (NSData *)AES256ParmDecryptWithKey:(NSString *)key   //解密
{
    char keyPtr[kCCKeySizeAES256+1];
    bzero(keyPtr, sizeof(keyPtr));
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    NSUInteger dataLength = [self length];
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);
    size_t numBytesDecrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt, kCCAlgorithmAES128,
                                          kCCOptionPKCS7Padding | kCCOptionECBMode,
                                          keyPtr, kCCBlockSizeAES128,
                                          NULL,
                                          [self bytes], dataLength,
                                          buffer, bufferSize,
                                          &numBytesDecrypted);
    if (cryptStatus == kCCSuccess) {
        return [NSData dataWithBytesNoCopy:buffer length:numBytesDecrypted];
    }
    free(buffer);
    return nil;
}




@end
