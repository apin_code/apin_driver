//
//  Apin_changeHeadImgViewController.m
//  Apin
//
//  Created by Apin on 15/7/11.
//  Copyright (c) 2015年 Apin. All rights reserved.

#import "Apin_changeHeadImgViewController.h"

#define SCALE_FRAME_Y 100.0f
#define BOUNDCE_DURATION 0.3f

@interface Apin_changeHeadImgViewController ()

@property (nonatomic, retain) UIImage *originalImage;
@property (nonatomic, retain) UIImage *editedImage;
@property (nonatomic, retain) UIImageView *showImgView;
@property (nonatomic, retain) UIView *overlayView;
@property (nonatomic, retain) UIView *ratioView;
@property (nonatomic, assign) CGRect oldFrame;
@property (nonatomic, assign) CGRect largeFrame;
@property (nonatomic, assign) CGFloat limitRatio;
@property (nonatomic, assign) CGRect latestFrame;
@end

@implementation Apin_changeHeadImgViewController

-(id)initWithImage:(UIImage *)originalImage cropFrame:(CGRect)cropFrame limitScaleRatio:(NSInteger)limitRatio{
    
    if (self=[super init]) {
        self.cropFrame=cropFrame;
        self.limitRatio=limitRatio;
        self.originalImage=originalImage;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [super viewDidLoad];
    [self initView];
    [self initControllBtn];
}

//
-(void)initView{
    self.showImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    [self.showImgView setMultipleTouchEnabled:YES];
    [self.showImgView setUserInteractionEnabled:YES];
    [self.showImgView setImage:self.originalImage];
    [self.showImgView setUserInteractionEnabled:YES];
    [self.showImgView setMultipleTouchEnabled:YES];
    
    
    //scale to fit the screen
    CGFloat oriWidth=self.cropFrame.size.width;
    CGFloat oriHeight=self.originalImage.size.height*(oriWidth/self.originalImage.size.width);
    CGFloat oriX=self.cropFrame.origin.x+(self.cropFrame.size.width-oriWidth)/2;
    CGFloat oriY=self.cropFrame.origin.y+(self.cropFrame.size.height-oriHeight)/2;
    self.oldFrame=CGRectMake(oriX, oriY, oriWidth, oriHeight);
    self.latestFrame=self.oldFrame;
    self.showImgView.frame=self.oldFrame;
    
    self.largeFrame=CGRectMake(0, 0, self.limitRatio*self.oldFrame.size.width, self.limitRatio*self.oldFrame.size.height);
    [self addGestureRecognizer];
    [self.view addSubview:self.showImgView];
    
    self.overlayView=[[UIView alloc]initWithFrame:self.view.bounds];
    self.overlayView.alpha=.5f;
    self.overlayView.backgroundColor=[UIColor blackColor];
    self.overlayView.userInteractionEnabled=NO;
    self.overlayView.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.overlayView];
    
    self.ratioView=[[UIView alloc]initWithFrame:self.cropFrame];
    self.ratioView.layer.borderColor=[UIColor blackColor].CGColor;
    self.ratioView.layer.cornerRadius=self.cropFrame.size.width/2;
    self.ratioView.layer.masksToBounds=YES;
    self.ratioView.layer.borderWidth=1.0f;
    self.ratioView.clipsToBounds=YES;
    self.ratioView.autoresizingMask=UIViewAutoresizingNone;
    [self.view addSubview:self.ratioView];
    
    [self overlayClipping];
}
-(void)initControllBtn{
    UIButton *cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 50.0f, 100, 50)];
    cancelBtn.backgroundColor = [UIColor blackColor];
    cancelBtn.titleLabel.textColor = [UIColor whiteColor];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0*KScaleH]];
    [cancelBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [cancelBtn.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [cancelBtn.titleLabel setNumberOfLines:0];
    [cancelBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 5.0f, 5.0f, 5.0f)];
    [cancelBtn addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelBtn];
    
    UIButton *confirmBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 100.0f, self.view.frame.size.height - 50.0f, 100, 50)];
    confirmBtn.backgroundColor = [UIColor blackColor];
    confirmBtn.titleLabel.textColor = [UIColor whiteColor];
    [confirmBtn setTitle:@"确定" forState:UIControlStateNormal];
    [confirmBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0*KScaleH]];
    [confirmBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
    confirmBtn.titleLabel.textColor = [UIColor whiteColor];
    [confirmBtn.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [confirmBtn.titleLabel setNumberOfLines:0];
    [confirmBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 5.0f, 5.0f, 5.0f)];
    [confirmBtn addTarget:self action:@selector(confirm:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:confirmBtn];
}

- (void)cancel:(id)sender {
    if (self.delegate && [self.delegate conformsToProtocol:@protocol(Apin_chaImgViewDelegate)]) {
        [self.delegate imgTaByPhDidCancel:self];
    }
}

- (void)confirm:(id)sender {
    if (self.delegate && [self.delegate conformsToProtocol:@protocol(Apin_chaImgViewDelegate)]) {
        [self.delegate imgTaByPh:self didFinished:[self getSubImage]];
    }
}

-(UIImage *)getSubImage{
    CGRect squareFrame = self.cropFrame;
    CGFloat scaleRatio = self.latestFrame.size.width / self.originalImage.size.width;
    CGFloat x = (squareFrame.origin.x - self.latestFrame.origin.x) / scaleRatio;
    CGFloat y = (squareFrame.origin.y - self.latestFrame.origin.y) / scaleRatio;
    CGFloat w = squareFrame.size.width / scaleRatio;
    CGFloat h = squareFrame.size.width / scaleRatio;
    if (self.latestFrame.size.width < self.cropFrame.size.width) {
        CGFloat newW = self.originalImage.size.width;
        CGFloat newH = newW * (self.cropFrame.size.height / self.cropFrame.size.width);
        x = 0; y = y + (h - newH) / 2;
        w = newH; h = newH;
    }
    if (self.latestFrame.size.height < self.cropFrame.size.height) {
        CGFloat newH = self.originalImage.size.height;
        CGFloat newW = newH * (self.cropFrame.size.width / self.cropFrame.size.height);
        x = x + (w - newW) / 2; y = 0;
        w = newH; h = newH;
    }
    CGRect myImageRect = CGRectMake(x, y, w, h);
    CGImageRef imageRef = self.originalImage.CGImage;
    CGImageRef subImageRef = CGImageCreateWithImageInRect(imageRef, myImageRect);
    CGSize size;
    size.width = myImageRect.size.width;
    size.height = myImageRect.size.height;
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextDrawImage(context, myImageRect, subImageRef);
    UIImage* smallImage = [UIImage imageWithCGImage:subImageRef];
    UIGraphicsEndImageContext();
    return smallImage;
    
}




//  加移动手势
-(void)addGestureRecognizer
{
    /*
     、拍击UITapGestureRecognizer (任意次数的拍击)
     2、向里或向外捏UIPinchGestureRecognizer (用于缩放)
     3、摇动或者拖拽UIPanGestureRecognizer
     4、滑动UISwipeGestureRecognizer (以任意方向)
     5、旋转UIRotationGestureRecognizer (手指朝相反方向移动)
     6、长按UILongPressGestureRecognizer
     */
    UIPinchGestureRecognizer *pinchGestureRecoginzer=[[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(pinchView:)];
    [self.view addGestureRecognizer:pinchGestureRecoginzer];
    
    UIPanGestureRecognizer *panGestureRecognizer=[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panView:)];
    [self.view addGestureRecognizer:panGestureRecognizer];
}
// 缩放 method
-(void)pinchView:(UIPinchGestureRecognizer *)pinchGestureRecognizer
{
    /*
     Possible: 识别器在未识别出它的手势，但可能会接收到触摸时处于这个状态。这是默认状态。
     Began: 识别器接收到触摸并识别出是它的手势时处于这个状态。响应方法将在下个循环步骤中被调用。
     Changed:the recognizer has received touches recognized as a change to the gesture. （不懂怎么翻译，理解上就是识别器识别出一个变化为它的手势的触摸），响应方法将在下个循环步骤中被调用。
     Ended:识别器在识别到作为当前手势结束信号的触摸时处于这个状态。响应方法将在下个循环步骤中被调用 并且 识别器将重置为possible状态。
     Cancelled:识别器处于取消状态.响应方法将在下个循环步骤中被调用 并且 识别器将重置为possible状态。
     Failed: 识别器接收到不能识别为它的手势的一系列触摸。响应方法不会被调用 并且 识别器将重置为possible状态。
     Recognized: 识别器已识别到它的手势。响应方法将在下个循环步骤中被调用 并且 识别器将重置为possible状态。
     */
    UIView * view=self.showImgView;
    if (pinchGestureRecognizer.state==UIGestureRecognizerStateBegan||pinchGestureRecognizer.state==UIGestureRecognizerStateChanged) {
        view.transform=CGAffineTransformScale(view.transform, pinchGestureRecognizer.scale, pinchGestureRecognizer.scale);
        pinchGestureRecognizer.scale=1;
    }else if (pinchGestureRecognizer.state==UIGestureRecognizerStateEnded)
    {
        CGRect newFrame=self.showImgView.frame;
        newFrame=[self handleBorderOverflow:newFrame];
        newFrame=[self handleScaleOverflow:newFrame];
        [UIView animateWithDuration:BOUNDCE_DURATION animations:^{
            self.showImgView.frame=newFrame;
            self.latestFrame=newFrame;
        }];
    }
}
-(CGRect)handleScaleOverflow:(CGRect)newFrame{
    
    CGPoint oriCenter=CGPointMake(newFrame.origin.x+newFrame.size.width/2  ,newFrame.origin.y+newFrame.size.height/2);
    if (newFrame.size.width<self.oldFrame.size.width) {
        newFrame=self.oldFrame;
    }
    if (newFrame.size.width>self.largeFrame.size.width) {
        newFrame=self.largeFrame;
    }
    newFrame.origin.x=oriCenter.x-newFrame.size.width/2;
    newFrame.origin.y=oriCenter.y-newFrame.size.height/2;
    return newFrame;
}
-(CGRect)handleBorderOverflow:(CGRect)newFrame{
    if (newFrame.origin.x > self.cropFrame.origin.x) newFrame.origin.x = self.cropFrame.origin.x;
    if (CGRectGetMaxX(newFrame) < self.cropFrame.size.width) newFrame.origin.x = self.cropFrame.size.width - newFrame.size.width;
    // vertically
    if (newFrame.origin.y > self.cropFrame.origin.y) newFrame.origin.y = self.cropFrame.origin.y;
    if (CGRectGetMaxY(newFrame) < self.cropFrame.origin.y + self.cropFrame.size.height) {
        newFrame.origin.y = self.cropFrame.origin.y + self.cropFrame.size.height - newFrame.size.height;
    }
    // adapt horizontally rectangle
    if (self.showImgView.frame.size.width > self.showImgView.frame.size.height && newFrame.size.height <= self.cropFrame.size.height) {
        newFrame.origin.y = self.cropFrame.origin.y + (self.cropFrame.size.height - newFrame.size.height) / 2;
    }
    return newFrame;
}
// 拖拽 method
-(void)panView:(UIPanGestureRecognizer *)panGestureRecognizer{
    
    UIView *view=self.showImgView;
    
    if (panGestureRecognizer.state==UIGestureRecognizerStateBegan||panGestureRecognizer.state==UIGestureRecognizerStateChanged) {
        CGFloat absCenterX = self.cropFrame.origin.x + self.cropFrame.size.width / 2;
        CGFloat absCenterY = self.cropFrame.origin.y + self.cropFrame.size.height / 2;
        CGFloat scaleRatio = self.showImgView.frame.size.width / self.cropFrame.size.width;
        CGFloat acceleratorX = 1 - ABS(absCenterX - view.center.x) / (scaleRatio * absCenterX);
        CGFloat acceleratorY = 1 - ABS(absCenterY - view.center.y) / (scaleRatio * absCenterY);
        CGPoint translation = [panGestureRecognizer translationInView:view.superview];
        [view setCenter:(CGPoint){view.center.x + translation.x * acceleratorX, view.center.y + translation.y * acceleratorY}];
        [panGestureRecognizer setTranslation:CGPointZero inView:view.superview];
    }else if (panGestureRecognizer.state == UIGestureRecognizerStateEnded) {
        // bounce to original frame
        CGRect newFrame = self.showImgView.frame;
        newFrame = [self handleBorderOverflow:newFrame];
        [UIView animateWithDuration:BOUNDCE_DURATION animations:^{
            self.showImgView.frame = newFrame;
            self.latestFrame = newFrame;
        }];
    }
}

//
-(void)overlayClipping
{
    CAShapeLayer *maskLayer=[[CAShapeLayer alloc]init];
    CGMutablePathRef path=CGPathCreateMutable();
    // ratioView 圆形   overlayView 大的背景图
    CGPathAddRect(path, nil, CGRectMake(0, 0, self.ratioView.frame.origin.x, self.overlayView.frame.size.height));
    
    CGPathAddRect(path, nil, CGRectMake(
                                        self.ratioView.frame.origin.x + self.ratioView.frame.size.width,
                                        0,
                                        self.overlayView.frame.size.width - self.ratioView.frame.origin.x - self.ratioView.frame.size.width,
                                        self.overlayView.frame.size.height));
    // Top side of the ratio view
    CGPathAddRect(path, nil, CGRectMake(0, 0,
                                        self.overlayView.frame.size.width,
                                        self.ratioView.frame.origin.y));
    // Bottom side of the ratio view
    CGPathAddRect(path, nil, CGRectMake(0,
                                        self.ratioView.frame.origin.y + self.ratioView.frame.size.height,
                                        self.overlayView.frame.size.width,
                                        self.overlayView.frame.size.height - self.ratioView.frame.origin.y + self.ratioView.frame.size.height));
    
    maskLayer.path=path;
    self.overlayView.layer.mask=maskLayer;
    CGPathRelease(path);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
