//
//  Apin_changeHeadImgViewController.h
//  Apin
//
//  Created by Apin on 15/7/11.
//  Copyright (c) 2015年 Apin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Apin_changeHeadImgViewController;

@protocol Apin_chaImgViewDelegate <NSObject>

-(void)imgTaByPh:(Apin_changeHeadImgViewController *)phViewController didFinished:(UIImage *)editdImage;
-(void)imgTaByPhDidCancel:(Apin_changeHeadImgViewController *)TaByPhViewController;

@end


@interface Apin_changeHeadImgViewController : UIViewController

@property(nonatomic,assign) NSInteger tag;
@property(nonatomic,assign)id <Apin_chaImgViewDelegate>delegate;
@property(nonatomic,assign)CGRect cropFrame;

-(id)initWithImage:(UIImage *)originalImage cropFrame:(CGRect)cropFrame limitScaleRatio:(NSInteger)limitRatio;






@end
