//
//  NSData+Encryption.h
//  AESCodeTest
//
//  Created by Apin on 15/9/2.
//  Copyright (c) 2015年 ios. All rights reserved.
//

#import <Foundation/Foundation.h>


@class NSString;

@interface NSData (Encryption)

- (NSString *)AES256ParmEncryptWithKey;   //加密
- (NSData *)AES256ParmDecryptWithKey:(NSString *)key;   //解密
          //同上64编码


@end
